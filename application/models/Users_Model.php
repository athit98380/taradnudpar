<?php
/**
 *
 */
class Users_Model extends CI_Model
{

  private $table = "users";

  function __construct()
  {
    $this->load->database();
  }

  public function record_count($email,$password = false)
  {
    if ($password != false) {
      $salt = $this->get_salt($email);
      $this->db->where('password',md5($password));
    }
    $this->db->where('email',$email);
    $numrow = $this->db->count_all_results($this->table);
    // echo $this->db->last_query();
    if ($numrow > 0) {
      return true;
    }else {
      return false;
    }
  }

  public function fetch_user($email,$password)
  {
    $salt = $this->get_salt($email);
    $this->db->where('email',$email);
    $this->db->where('password',md5($password));
    return $this->db->get($this->table)->row();
  }

  public function get_all()
  {
    $this->db->where('admin_active','1');
    return $this->db->get($this->table)->result();
  }

  public function get_new_all()
  {
    $this->db->where('admin_active','0');
    return $this->db->get($this->table)->result();
  }

  public function get_user($uid)
  {
    $this->db->where('id',$uid);
    return $this->db->get($this->table)->row();
  }

  public function get_profile($uid)
  {
    $this->db->where('id',$uid);
    return $this->db->get($this->table)->row();
  }

  public function set_log_login($email)
  {
    $this->db->where('email',$email);
    $this->db->set('last_login','now()',false);
    $this->db->update($this->table);
  }

  public function create_user($value)
  {
    $this->db->set('created_on','now()',false);
    $this->db->insert($this->table,$value);
    return $this->db->insert_id();
  }

  public function create_salt()
  {
    return hex2bin(rand(10,100).time());
  }

  public function get_salt($email)
  {
    $this->db->where('email',$email);
    return $this->db->get($this->table)->row('salt');
  }

  public function set_imgageid($value)
  {
    return $this->db->update($this->table,$value);
  }

  public function del_user($uid)
  {
    return $this->db->delete($this->table,['id' => $uid]);
  }

  public function admin_active($data,$uid)
  {
    return $this->db->update($this->table,$data,['id' => $uid]);
  }

  public function change_image_profile($data,$uid)
  {
    return $this->db->update($this->table,$data,['id' => $uid]);
  }
}

 ?>
