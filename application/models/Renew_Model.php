<?php
/**
 *
 */
class Renew_Model extends CI_Model
{
  private $table = "renew";

  function __construct()
  {
    $this->load->database();
  }

  public function add($data)
  {
    $this->db->where('users_id',$this->session->uid);
    $this->db->where('renew_status',0);
    $result = $this->db->get($this->table);
    if ($result->num_rows() == 0) {
      $this->db->set('renew_create_on',date('Y-m-d H:i:s'));
      $this->db->set('renew_update_on',date('Y-m-d H:i:s'));
      $this->db->set('renew_status', '0');
      return $this->db->insert($this->table,$data);
    }else {
      return false;
    }
  }

  public function get()
  {
    $this->db->where('renew_status','0');
    return $this->db->get($this->table)->result();
  }

  public function get_shopid($rid)
  {
    $this->db->select('shop_id');
    $this->db->where('renew_id',$rid);
    return $this->db->get($this->table)->row('shop_id');
  }

  public function confirm($data,$rid)
  {
    return $this->db->update($this->table,array('renew_status' => $data),['renew_id' => $rid]);
  }

}

 ?>
