<?php
/**
 *
 */
class Product_Model extends CI_Model
{
  private $table = 'product';

  function __construct()
  {
    $this->load->database();
  }

  /*
   * select
   */

  // ส่วนของแอดมิน ยืนยันการแก้ไขต่างๆของ user
  public function get_confirm_product()
  {
    $this->db->where('product_status =','1');
    return $this->db->get($this->table)->result();
  }

  public function get_all_product()
  {
    $this->db->where('product_status','0');
    return $this->db->get($this->table)->result();
  }

  public function get_limit_product($limit)
  {
    $this->db->limit($limit);
    $this->db->where('product_status','0');
    $this->db->group_by('shop_id');
    $this->db->order_by('product_id', 'RANDOM');
    return $this->db->get($this->table)->result();
  }

  public function get_limit_product_by_admin($limit)
  {
    $this->db->limit($limit);
    $this->db->where('product_status','0');
    $this->db->where('users_id','1');
    $this->db->order_by('product_id', 'RANDOM');
    return $this->db->get($this->table)->result();
  }

  public function get_product($id = false,$uid = false,$update = false)
  {
    if ($id != false) {
      $this->db->where('product_id',$id);
      return $this->db->get($this->table)->row();
    }elseif ($uid != false) {
      $this->db->where('users_id',$uid);
      return $this->db->get($this->table)->result();
    }elseif ($update != false) {
      $this->db->where('product_status','0');
      $this->db->order_by('shop_update_on','desc');
      return $this->db->get($this->table)->result();
    }else {
      $this->db->where('product_status','0');
      return $this->db->get($this->table)->result();
    }
  }

  public function get_product_by_shop_id($id)
  {
    $this->db->where('product_status','0');
    $this->db->where('shop_id',$id);
    return $this->db->get($this->table)->result();
  }

  public function get_last_product($uid)
  {
    $this->db->where('users_id',$uid);
    $this->db->order_by('product_id','DESC');
    $this->db->limit(1);
    return $this->db->get($this->table)->result();
  }

  public function get_product_join($pid)
  {
    $sql = 'update '.$this->table.' set product_view=product_view+1 where product_id='.$pid;
    $this->db->query($sql);

    $this->db->join('shop',"shop.shop_id = {$this->table}.shop_id",'inner');
    $this->db->join('users',"users.id = shop.shop_id",'inner');
    $this->db->where('product_id',$pid);
    return $this->db->get($this->table)->row();
  }

  public function get_count_product($id = null)
  {
    if ($id != null) {
      $this->db->where('product_status','0');
      $this->db->where('shop_id',$id);
    }
    return $this->db->count_all_results($this->table);
  }

  public function get_count_product_amulettop()
  {
    $this->db->where('product_status','0');
    return $this->db->count_all_results($this->table);
  }
  public function get_count_product_search($text)
  {
    $this->db->where('product_status','0');
    $this->db->like('product_name',$text);
    $this->db->or_like('product_detail',$text);
    return $this->db->count_all_results($this->table);
  }

  public function get_pagination_product_orderby($limit,$start)
  {
    $this->db->where('product_status','0');
    $this->db->order_by('product_view','DESC');
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }

  public function get_pagination_product_idby($limit,$start,$sid)
  {
    $this->db->where('product_status','0');
    $this->db->where('shop_id',$sid);
    $this->db->order_by('product_id','DESC');
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }

  public function get_pagination_product_idby_all_0($limit,$start,$sid)
  {
    $this->db->where('product_status','0');
    $this->db->where('shop_id',$sid);
    $this->db->order_by('product_id','DESC');
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }


  public function get_pagination_product_idby_all_1($limit,$start,$sid)
  {
    $this->db->where('product_status','1');
    $this->db->where('shop_id',$sid);
    $this->db->order_by('product_id','DESC');
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }

  public function get_pagination_product_group($limit,$start,$id)
  {
    $this->db->where('product_status','0');
    $this->db->where('product_type_id',$id);
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }


  // shop
  public function get_product_by_shop($sid)
  {
    $this->db->where('shop_id',$sid);
    $this->db->order_by('product_id','DESC');
    return $this->db->get($this->table)->result();
  }

  // shop
  public function get_userid($pno,$status = TRUE)
  {
    if ($status == true) {
      $this->db->where('product_no',$pno);
    }else {
      $this->db->where('product_id',$pno);
    }
    return $this->db->get($this->table)->row('users_id');
  }

  /* end select */

  /*
   * update
   */

  public function update_product($data,$id)
  {
   $this->db->where('product_id',$id);
   $this->db->set('product_update_on',date('Y-m-d H:i:s'));
   return $this->db->update($this->table,$data);
 }

 /* end update */


 public function get_product_by_status($product_status)
 {
  $this->db->where('product_status =',$product_status);
  return $this->db->get($this->table)->result();
}

public function getCountAll()
{
  return $this->db->count_all_results($this->product_type);
}
public function getCountAllProduct()
{
  $this->db->where('product_status','0');
  return $this->db->count_all_results($this->table);
}


public function get_pagination($limit,$start)
{
  $this->db->limit($limit,$start);
  return $this->db->get($this->product_type)->result();
    // echo $this->db->last_query();
}

public function get_pagination_product_amulet($limit,$start)
{
  $this->db->where('product_status','0');
  $this->db->order_by('product_update_on','DESC');
  $this->db->limit($limit,$start);
  return $this->db->get($this->table)->result();
}

public function get_pagination_product($limit,$start,$status = false)
{
  if ($status != false) {
    $this->db->where('product_status !=','0');
  }
  $this->db->limit($limit,$start);
  return $this->db->get($this->table)->result();
    // echo $this->db->last_query();
}

public function create_product($data)
{
  $this->db->set('product_create_on',date('Y-m-d H:i:s'));
  $this->db->set('product_update_on',date('Y-m-d H:i:s'));
  return $this->db->insert($this->table,$data);
}




public function record_product($uid)
{
  $this->db->where('users_id',$uid);
    // echo $this->db->get($this->table)->num_rows();
    // echo $this->db->last_query();
  if ($this->db->get($this->table)->num_rows() > 0) {
    return true;
  }else {
    return false;
  }
}


public function get_product_last($uid)
{
  $this->db->select('product_create_on');
  $this->db->where('users_id',$uid);
  return $this->db->get($this->table)->row();
}



public function delete_product($id)
{
  $this->db->where('product_id',$id);
  $this->db->set('product_status','2');
  return $this->db->update($this->table);
}

  /*
   * delete
   */

  public function delete_product_confirm($id)
  {
    return $this->db->delete($this->table,['product_id' => $id]);
  }

  public function del_product($uid)
  {
    return $this->db->delete($this->table,['users_id' => $uid]);
  }

  /* end delete */
  public function get_pagination_product_search($limit,$start,$text)
  {
    $this->db->where('product_status','0');
    $this->db->like('product_name',$text);
    $this->db->or_like('product_detail',$text);
    $this->db->order_by('product_update_on','DESC');
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }

  public function search_product($text)
  {
    $this->db->having('product_status','0');
    $this->db->like('product_name',$text);
    $this->db->or_like('product_detail',$text);
    return $this->db->get($this->table)->result();
  }
  public function product_view($text)
  {
    $this->db->where('product_view');
    $this->db->having('product_status','0');
    $this->db->like('product_name',$text);
    $this->db->or_like('product_detail',$text);
    return $this->db->get($this->table)->result();
  }

}

?>
