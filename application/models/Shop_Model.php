<?php
/**
 *
 */
class Shop_Model extends CI_Model
{

  private $table = "shop";

  function __construct()
  {
    $this->load->database();
  }

  public function create_shop($data)
  {
    $this->db->insert($this->table,$data);
    return $this->db->insert_id();
  }

  public function get_all_shop()
  {
    $this->db->where('shop_status','1');
    $this->db->order_by('shop_id','desc');
    $this->db->join('users',"users.id = {$this->table}.users_id",'inner');
    return $this->db->get($this->table)->result();
  }

  // แสดงผลร้านถ้าร้านไม่หมดอายุ
  public function get_all_shop_show()
  {
    $this->db->join('users',"users.id = {$this->table}.users_id",'inner');
    $this->db->where('shop_status','0');
    $this->db->order_by('shop_recommend','desc');
    return $this->db->get($this->table)->result();
    // echo $this->db->last_query();
  }

  public function get_shop($uid)
  {
    $this->db->where('users_id',$uid);
    $this->db->order_by('shop_id','desc');
    return $this->db->get($this->table)->row();
  }

  public function get_shop_id($sid)
  {
    $this->db->where('shop_id',$sid);
    return $this->db->get($this->table)->row();
  }

  public function get_amount_add($sid)
  {
    $this->db->where('shop_id',$sid);
    return $this->db->get($this->table)->row();
  }

  public function update_view($uid)
  {
    return $this->db->query("UPDATE {$this->table} SET shop_view = shop_view + 1 WHERE users_id = '{$uid}'");
  }

  public function update_shop($data,$sid)
  {
    return $this->db->update($this->table,$data,['shop_id' => $sid]);
  }

  public function update_shop_profile($data,$sid)
  {
    return $this->db->update($this->table,$data,['shop_id' => $sid]);
  }

  public function update_member_profile($data,$sid)
  {
    return $this->db->update('users',$data,['id' => $sid]);
  }

  public function update_shop_bank($data,$sid)
  {
    return $this->db->update($this->table,$data,['shop_id' => $sid]);
  }

  public function admin_active($data,$uid)
  {
    return $this->db->update($this->table,$data,['users_id' => $uid]);
  }

  public function del_shop($uid)
  {
    return $this->db->delete($this->table,['users_id' => $uid]);
  }

  public function recommend($data,$sid)
  {
    return $this->db->update($this->table,array('shop_recommend' => $data),['shop_id' => $sid]);
  }

  public function get_recommend()
  {
    $this->db->join('users',"users.id = {$this->table}.users_id",'inner');
    $this->db->where('shop_recommend',1);
    $this->db->where('shop_status','0');
    $this->db->limit(20);
    return $this->db->get($this->table)->result();
  }

  public function change_shop_banner($data,$uid)
  {
    return $this->db->update($this->table,$data,['shop_id' => $uid]);
  }

  public function change_shop_qrcode($data,$uid)
  {
    return $this->db->update($this->table,$data,['shop_id' => $uid]);
  }

  public function get_pagination_shop($limit,$start,$text){
    $this->db->join('users',"users.id = {$this->table}.users_id",'inner');
    $this->db->where('shop_status','0');
    $this->db->like('shop_name',$text);
    $this->db->or_like('shop_detail',$text);
    $this->db->or_like('shop_id',$text);
    $this->db->or_like('shop_address',$text);
    $this->db->order_by('shop_recommend','desc');
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }

  public function search_shop($text)
  {
    $this->db->join('users',"users.id = {$this->table}.users_id",'inner');
    $this->db->where('shop_status','0');
    $this->db->like('shop_name',$text);
    $this->db->or_like('shop_detail',$text);
    $this->db->or_like('shop_id',$text);
    $this->db->or_like('shop_address',$text);
    $this->db->order_by('shop_recommend','desc');
    return $this->db->get($this->table)->result();
  }
}
