<?php
/**
 *
 */
date_default_timezone_set('asia/bangkok');

class Static_Model extends CI_Model{

  private $table = "static";
  private $table2 = "view";
  function __construct()
  {
    $this->load->database();
  }

  public function get_static()
  {
    return $this->db->get($this->table)->row();
  }

  public function update_static_admin($data)
  {
    return $this->db->update($this->table,$data);
  }

  public function update_item()
  {
    $query = $this->db->query("SELECT * FROM {$this->table}");
    $numrow = $query->num_rows();
    if ($numrow != 0) {
      $this->db->query("UPDATE `static` SET `static_view_all` = `static_view_all` + 1");
      if (strtotime($this->get_updateDate()) + 86400 < time()) {
        $this->db->query("UPDATE `static` SET `static_view_today` = 0, `static_view_update_on` = now()");
      }else {
        $this->db->query("UPDATE `static` SET `static_view_today` = `static_view_today` + 1");
      }
    }else {
      $data = array(
        'static_view_all' => '1',
        'static_view_today' => '1',
        'static_view_online' => '1'
      );
      $this->db->set('static_view_update_on',date('Y-m-d H:i:s'));
      $this->db->insert($this->table,$data);
    }
  }

  private function get_updateDate()
  {
    return $this->db->get($this->table)->row('static_view_update_on');
  }

  public function useronline()
  {
    if (empty($_SESSION['timenow'])) {
      $_SESSION['timenow'] = time();
    }
    if ($_SESSION['timenow'] + 60 <= time()) {
      $this->db->update($this->table,array('static_view_online' => '0'));
    }else {
      $this->db->query("UPDATE `static` SET `static_view_online` = `static_view_online` + 1");
    }
  }
  public function visitorOnline(){
    date_default_timezone_set('asia/bangkok');
    if(!isset($_SESSION)){
      session_start();
    }
    // $session = session_id();
    $ip= $_SERVER['REMOTE_ADDR'];
    $time = time();
    // $this->db->where('session',$session);
    $this->db->where('ip',$ip);
    $date = date("Y-m-d H:i:s",$time);
    $query = $this->db->get($this->table2);
    if($query->num_rows() == null){
      $data = array(
        'ip' => $ip,
        // 'session' => $session,
        'times' => $time,
        'date' => $date
      );
      $this->db->insert($this->table2,$data);
    }
    else{
      $data1 = array(
        'times' => $time,
        'date' => $date
      );
      
      $this->db->query("delete from visitor where times < $time-600 ");
      // $this->db->query("delete from visitor where times < $time-600");
      // $this->db->where('session',$session);
      $this->db->where('ip',$ip);
      $this->db->update($this->table2,$data1);
      return $this->db->get($this->table2)->num_rows();
    }
  }
}

?>
