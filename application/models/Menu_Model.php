<?php
/**
 *
 */
class Menu_Model extends CI_Model
{

  private $table = "menu";

  function __construct()
  {
    $this->load->database();
  }

  public function get_menu()
  {
    $this->db->order_by('menu_level','ASC');
    return $this->db->get($this->table)->result();
  }

  public function get_menu_by_id($id)
  {
    $this->db->where('menu_id',$id);
    return $this->db->get($this->table)->row();
  }

  public function create_menu($data)
  {
    return $this->db->insert($this->table,$data);
  }

  public function update_menu($data,$id)
  {
    return $this->db->update($this->table,$data,['menu_id' => $id]);
  }

}

 ?>
