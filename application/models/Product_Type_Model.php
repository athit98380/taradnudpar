<?php
/**
 *
 */
class Product_Type_Model extends CI_Model
{

  private $table = "product_type";

  function __construct()
  {
    $this->load->database();
  }
  /*
   * select
   */
  public function get_all_product_type()
  {
    return $this->db->get($this->table)->result();
  }

  public function get_by_id($id)
  {
    $this->db->where('product_type_id',$id);
    return $this->db->get($this->table)->row();
  }

  public function record_count($value)
  {
    $this->db->where('product_type_name',$value);
    return $this->db->count_all_results($this->table);
  }

  /* end select */


  /*
   * insert
   */

  public function set_product_type($value)
  {
    $this->db->insert($this->table,$value);
  }

  /* end insert */

  /*
   * update
   */
  public function update_item($id,$value)
  {
    return $this->db->update($this->table,$value,['product_type_id' => $id]);
  }
  /* end update */

  /*
   * delete
   */
  public function delete_item($id)
  {
    return $this->db->delete($this->table,['product_type_id' => $id]);
  }

  /* end delete */
}

 ?>
