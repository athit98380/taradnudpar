<?php
/**
 *
 */
class Theme_Model extends CI_Model
{
  private $theme = 2;

  private $table = "theme";

  function __construct()
  {
    $this->load->database();
  }

  public function get_theme()
  {
    $this->db->order_by('theme','ASC');
    return $this->db->get($this->table)->row();
  }

  public function get_theme_design()
  {
    $this->db->where('theme',1);
    return $this->db->get($this->table)->row();
  }

  public function update_theme($data,$id)
  {
    return $this->db->update($this->table,$data,['theme' => $id]);
  }

}

 ?>
