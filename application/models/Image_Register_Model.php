<?php
/**
 *
 */
class Image_Register_Model extends CI_Model
{
  private $table = "image_register";

  function __construct()
  {
    $this->load->database();
  }

  public function get_all_by_uid($uid)
  {
    $this->db->where('users_id',$uid);
    return $this->db->get($this->table)->row();
  }

  public function get_image($uid)
  {
    $this->db->where('shop_id',$uid);
    return $this->db->get($this->table)->row();
  }

  public function create_item($data)
  {
    return $this->db->insert($this->table,$data);
  }

  public function update_item($data,$uid)
  {
    $this->db->where('shop_id',$uid);
    return $this->db->update($this->table,$data);
  }

  public function del_image($uid)
  {
    return $this->db->delete($this->table,['users_id' => $uid]);
  }

  public function admin_active($data,$uid)
  {
    return $this->db->update($this->table,$data,['users_id' => $uid]);
  }
}

 ?>
