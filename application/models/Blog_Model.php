<?php
/**
 *
 */
class Blog_Model extends CI_Model
{

  private $table = "blog";

  function __construct()
  {
    $this->load->database();
  }

  public function get_all_blog()
  {
    $this->db->where('blog_status','1');
    return $this->db->get($this->table)->result();
  }

  public function get_pagination($limit,$start)
  {
    $this->db->where('blog_status','1');
    $this->db->limit($limit,$start);
    return $this->db->get($this->table)->result();
  }

  public function get_blog($limit = null)
  {
    $this->db->where('blog_status','1');
    if ($limit != null) {
      $this->db->limit($limit);
    }
    return $this->db->get($this->table)->result();
  }

  public function get_blog_by_id($id)
  {
    $this->db->where('blog_id',$id);
    return $this->db->get($this->table)->row();
  }

  public function getCountAll()
  {
    return $this->db->count_all_results($this->table);
  }

  public function get_by_id($id)
  {
    $this->db->where('blog_id',$id);
    return $this->db->get($this->table)->row();
  }

  public function get_by_uid($uid)
  {
    $this->db->where('users_id',$uid);
    $this->db->where('blog_status','2');
    return $this->db->count_all_results($this->table);
  }

  public function get_data_by_uid($uid)
  {
    $this->db->where('users_id',$uid);
    $this->db->where('blog_status','2');
    return $this->db->get($this->table)->row();
  }

  public function create_news($value)
  {
    return $this->db->insert($this->table,$value);
  }

  public function update_item($id,$value)
  {
    return $this->db->update($this->table,$value,['blog_id' => $id]);
  }

  public function update_item_user($uid,$value)
  {
    return $this->db->update($this->table,$value,['users_id' => $uid]);
  }

  public function delete_item($id)
  {
    return $this->db->delete($this->table,['blog_id' => $id]);
  }

  public function del_blog($uid)
  {
    return $this->db->delete($this->table,['users_id' => $uid, 'blog_status' => '2']);
  }

}

 ?>
