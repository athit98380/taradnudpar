<?php
/**
 *
 */
class Admin_Model extends CI_Model
{

  private $table = "";

  function __construct()
  {
    $this->load->database();
  }

  public function count_product($product_status)
  {
    $this->db->where('product_status',$product_status);
    return $this->db->count_all_results('product');
  }

  public function count_renew()
  {
    $this->db->where('renew_status','0');
    return $this->db->count_all_results('renew');
  }

  public function count_regis_user()
  {
    $this->db->where('admin_active','0');
    return $this->db->count_all_results('users');
  }

  public function count_shop()
  {
    $this->db->where('shop_status','1');
    return $this->db->count_all_results('shop');
  }

  public function get_blog_by_id($id)
  {
    $this->db->where('blog_id',$id);
    return $this->db->get($this->table)->row();
  }

  public function getCountAll()
  {
    return $this->db->count_all_results($this->table);
  }

  public function get_by_id($id)
  {
    $this->db->where('blog_id',$id);
    return $this->db->get($this->table)->row();
  }

  public function get_by_uid($uid)
  {
    $this->db->where('users_id',$uid);
    $this->db->where('blog_status','2');
    return $this->db->count_all_results($this->table);
  }

  public function get_data_by_uid($uid)
  {
    $this->db->where('users_id',$uid);
    $this->db->where('blog_status','2');
    return $this->db->get($this->table)->row();
  }

  public function create_news($value)
  {
    return $this->db->insert($this->table,$value);
  }

  public function update_item($id,$value)
  {
    return $this->db->update($this->table,$value,['blog_id' => $id]);
  }

  public function update_item_user($uid,$value)
  {
    return $this->db->update($this->table,$value,['users_id' => $uid]);
  }

  public function delete_item($id)
  {
    return $this->db->delete($this->table,['blog_id' => $id]);
  }

  public function del_blog($uid)
  {
    return $this->db->delete($this->table,['users_id' => $uid, 'blog_status' => '2']);
  }

}

 ?>
