<?php
/**
 *
 */
class Theme
{
    function __construct()
    {     
        $this->ci =& get_instance();
        $this->ci->load->model('Theme_Model');
    }

  	public function Get_theme()
	{
		$data=$this->ci->Theme_Model->get_theme();
		
		return $data;
	}

}

 ?>