<?php
/**
 *
 */
class Count_admin
{
    function __construct()
    {     
        $this->ci =& get_instance();
        $this->ci->load->model('Admin_Model');
    }

  	public function Count_data()
	{
		$data['count_product_0']=$this->ci->Admin_Model->count_product(0);
		$data['count_product_1']=$this->ci->Admin_Model->count_product(1);
		$data['count_product_2']=$this->ci->Admin_Model->count_product(2);
		$data['count_product_3']=$this->ci->Admin_Model->count_product(3);
		$data['count_product_all']=$data['count_product_1']+$data['count_product_2'];
		$data['count_renew']=$this->ci->Admin_Model->count_renew();
		$data['count_regis_user']=$this->ci->Admin_Model->count_regis_user();
		$data['count_shop']=$this->ci->Admin_Model->count_shop();
		
		return $data;
	}

}

 ?>
