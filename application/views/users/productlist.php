    <div class="col-md-8">
      <div class="card">
        <div class="card-header" style="<?php echo "color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "background-color: #".$theme_for_design->theme_page_font."!important;";?>">
          จัดการร้าน
          <a href="<?= base_url('users/add_product') ?>" class="btn btn-primary btn-sm float-right">เพิ่มสินค้า</a>
        </div>
        <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_page_font."!important;";?>">
            <?php if (count($products) <= 0): ?>
              <p class="text-center">ไม่พบข้อมูล</p>
            <?php else: ?>
              <div class="d-flex justify-content-center">
                <?= $pagination ?>
              </div>
          <div class="row">
            <?php foreach ($products as $row) : ?>
            <div class="col-sm-6 col-md-6 col-lg-3">
              <div class="card card-custom">
                <div class="view">
                  <?php if (!empty($row->product_image)) {?>
                  <?php $images=explode( ",",$row->product_image);?>
                    <img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$images[0]}") ?>" class="card-img-top" alt="...">
                  <?php }else{?>
                    <img src="<?= base_url("assets/images/logo/logo-only.png") ?>" style="height: 100%;" class="card-img-top" alt="...">
                  <?php }?>
                </div>
                <div class="card-body card-text text-ellipsis">
                  <?= iconv_substr($row->product_name, 0,100, "UTF-8") ?>
                <div class="row">
                  <div class="text-center" style="width: 100%;">สถานะ 
                    <?php if($row->product_status==0){?>
                    <strong style="color:green;">เผยแพร่</strong>
                    <?php }?>
                    <?php if($row->product_status==1){?>
                    <strong style="color:red;">รอยืนยัน</strong>
                    <?php }?>
                    <?php if($row->product_status==2){?>
                    <strong style="color:red;">แจ้งลบ</strong>
                    <?php }?>
                  </div>  
                </div>
                  <div class="row">
                    <div class="col-md-6">
                      <a href="<?= base_url("users/edit_product/{$row->product_id}") ?>" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                    </div>
                    <div class="col-md-6">
                      <button type="button" class="btn btn-danger" onclick="del(<?= $row->product_id ?>)" ><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
          </div>
    <div class="d-flex justify-content-center">
      <?= $pagination ?>
    </div>
  <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <!-- END ROW -->
</div>
<!-- END CONTAINER -->
<?= $this->session->msg ?>
<script type="text/javascript">
function del(id)
{
  swal({
    title: "คุณต้องการลบข้อมูลนี้จริงหรือไม่?",
    text: "คุณต้องรอแอดมินยืนยัน",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    })
    .then((willDelete) => {
    if (willDelete) {
      window.location.href = "<?= base_url("users/delete_product/") ?>" + id;
    }
    });
}
</script>
