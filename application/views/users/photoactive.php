    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <div class="form-group row">
                        <?php if($left->image_register_comment != null) : ?>
                        <div class="col-sm-12">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>ไม่ผ่านการยืนยันเพราะ : </strong> <?= $left->image_register_comment ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-sm-12">
                            <h1 class="text-center">ภาพยืนยันพระเครื่อง</h1>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front1}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back1}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail1 ?></p>
                        </div>
                    </div>
<!--                     <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 2</h1>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front2}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back2}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail2 ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 3</h1>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front3}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back3}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail3 ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 1</h1>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front4}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back4}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail4 ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 1</h1>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front5}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back5}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail5 ?></p>
                        </div>
                    </div> -->
        </div>
      </div>
    </div>
    </div>
    <!-- END ROW -->
  </div>
  <!-- END CONTAINER -->
<?= $this->session->msg ?>
