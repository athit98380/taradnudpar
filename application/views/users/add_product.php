      <div class="col-md-8">
        <div class="card">
          <div class="card-header" style="<?php echo "color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "background-color: #".$theme_for_design->theme_page_font."!important;";?>">
            <?= $title ?>
            <a href="<?= base_url('users/productlist') ?>" class="btn btn-danger btn-sm float-right">กลับ</a>
          </div>
          <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_page_font."!important;";?>">
              <?php $path=base_url("users/add_product");?>
              <?php echo form_open_multipart($path, 'id="myform"');?>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ชื่อสินค้า</label>
                <div class="col-md-10">
                  <input type="text" name="pname" class="form-control" value="<?= (isset($product->product_name)) ? $product->product_name : null ?>" required>
                  <?= form_error('pname','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ราคา</label>
                <div class="col-md-10">
                  <input type="text" name="pprice" class="form-control" value="<?= (isset($product->product_price)) ? $product->product_price : null ?>" required>
                  <?= form_error('pprice','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ประเภท</label>
                <div class="col-md-10">
                  <select class="form-control" name="ptype" required>
                    <option value="">เลือก</option>
                    <?php foreach ($ptype as $row) : ?>
                      <option <?php if(!empty($product->product_type_id)){ if($product->product_type_id == $row->product_type_id){ echo 'selected'; } } ?>  value="<?= $row->product_type_id ?>"><?= $row->product_type_name ?></option>
                    <?php endforeach; ?>
                  </select>
                  <?= form_error('ptype','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">สถานะ</label>
                <div class="col-md-10">
                  <select class="form-control" name="pstatusfs" required>
                    <option value="">เลือก</option>
                    <option value="โชว์พระ">โชว์พระ</option>
                    <option value="ขายแล้ว">ขายแล้ว</option>
                    <option value="โทรถาม">โทรถาม</option>
                    <option value="เสนอราคา">เสนอราคา</option>
                  </select>
                  <?= form_error('ptype','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <?php $n=0;for ($i=1; $i < 11; $i++) { ?>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รูป<?=$i; ?></label>
                <div class="col-md-10">
                  <input type="file" accept="image/*" name="pimage<?php echo $i;?>" class="form-control-file" <?php if($i=="1"){echo "required";}?> >
                </div>
              </div>
              <?php }?>
              <div class="form-group row">
               <label for="" class="col-form-label col-sm-2"></label><div class="col-md-10"><strong>ขนาดไฟล์ไม่เกิน 20 Mb</strong></div>
               </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รายละเอียด</label>
                <div class="col-md-10">
                  <textarea name="pdetail" class="form-control" rows="4" cols="80" required><?= (isset($product->product_detail)) ? $product->product_detail : null ?></textarea>
                  <?= form_error('pdetail','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-10 ml-auto">
                  <button type="submit" name="submit" class="btn btn-primary btn-block">ยืนยัน</button>
                </div>
                <div class="col-md-10 ml-auto">
                  <!-- <?php echo $n;?> -->
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END ROW -->
  </div>
<!-- END CONTAINER -->
<?= $this->session->msg ?>
