    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <form action="<?php echo base_url('users/notify/'.$shop->shop_id);?>" method="post">
            <div class="form-group">
              <label for="" class="">ข้อความประกาศ</label>
              <textarea name="blog" id="blog" rows="6" cols="80" class="form-control"><?= (isset($shop->shop_blog)) ? $shop->shop_blog : 'ไม่มีประกาศ' ?></textarea>
              <?= form_error('blog','<small class="text-danger">','</small>') ?>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    </div>
    <!-- END ROW -->
  </div>
  <!-- END CONTAINER -->
<?= $this->session->msg ?>

<script type="text/javascript">
  CKEDITOR.replace( 'blog' );
</script>
