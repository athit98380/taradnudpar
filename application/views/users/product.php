      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <?= $title ?>
            <a href="<?= base_url('users/productlist') ?>" class="btn btn-danger btn-sm float-right">กลับ</a>
          </div>
          <div class="card-body">
            <form id="myform" action="<?= ($this->uri->segment(2) == 'add_product') ? base_url('users/add_product') : base_url("users/edit_product/{$product->product_id}") ?>" method="post" enctype="multipart/form-data">
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ชื่อสินค้า</label>
                <div class="col-md-10">
                  <input type="text" name="pname" class="form-control" value="<?= (isset($product->product_name)) ? $product->product_name : null ?>">
                  <?= form_error('pname','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ราคา</label>
                <div class="col-md-10">
                  <input type="text" name="pprice" class="form-control" value="<?= (isset($product->product_price)) ? $product->product_price : null ?>">
                  <?= form_error('pprice','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ประเภท</label>
                <div class="col-md-10">
                  <select class="form-control" name="ptype">
                    <option value="">เลือก</option>
                    <?php foreach ($ptype as $row) : ?>
                      <option <?php if(!empty($product->product_type_id)){ if($product->product_type_id == $row->product_type_id){ echo 'selected'; } } ?>  value="<?= $row->product_type_id ?>"><?= $row->product_type_name ?></option>
                    <?php endforeach; ?>
                  </select>
                  <?= form_error('ptype','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">สถานะ</label>
                <div class="col-md-10">
                  <select class="form-control" name="pstatusfs">
                    <option value="">เลือก</option>
                    <option <?= ($product->product_status_for_sale == 'โชว์พระ') ? 'selected' : null ?> value="โชว์พระ">โชว์พระ</option>
                    <option <?= ($product->product_status_for_sale == 'ขายแล้ว') ? 'selected' : null ?> value="ขายแล้ว">ขายแล้ว</option>
                    <option <?= ($product->product_status_for_sale == 'โทรถาม') ? 'selected' : null ?> value="โทรถาม">โทรถาม</option>
                  </select>
                  <?= form_error('ptype','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รูป</label>
                <div class="col-md-10">
                  <!-- <div class="dropzone" id="mydropzone" style="font-size: 1.5em;">
                		<h3 class="dropzone-previews ui"></h3>
                		<div class="fallback">
                      <input type="file" name="pimage" class="form-control-file" multiple >
                		</div>
                	</div> -->
                  <input type="file" name="pimage" class="form-control-file" >
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รายละเอียด</label>
                <div class="col-md-10">
                  <textarea name="pdetail" class="form-control" rows="4" cols="80"><?= (isset($product->product_detail)) ? $product->product_detail : null ?></textarea>
                  <?= form_error('pdetail','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-10 ml-auto">
                  <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END ROW -->
  </div>
<!-- END CONTAINER -->
<?= $this->session->msg ?>

<script type="text/javascript">
  // Dropzone.options.myAwesomeDropzone = false;
  // $("div#mydropzone").dropzone({
  //   url : '<?= ($this->uri->segment(2) == 'add_product') ? base_url('users/ajax') : base_url("users/edit_product/{$product->product_id}") ?>',
  //   paramName : "fileOther", // ชื่อไฟล์ปลายทางเมื่อ upload แบบ mutiple จะเป็น array
  //   autoProcessQueue : false,// ใส่เพื่อไม่ให้อัพโหลดทันที หลังจากเลือกไฟล์
  //   uploadMultiple : true, // อัพโหลดไฟล์หลายไฟล์
  //   parallelUploads : 8, // ให้ทำงานพร้อมกัน 10 ไฟล์
  //   maxFiles : 8, // ไฟล์สูงสุด 5 ไฟล์
  //   addRemoveLinks : true, // อนุญาตให้ลบไฟล์ก่อนการอัพโหลด
  //   maxFilesize: 1, // MB
  //   previewsContainer : ".dropzone", // ระบุ element เป้าหลาย
  //   dictRemoveFile : "Remove", // ชื่อ ปุ่ม remove
  //   dictCancelUpload : "Cancel", // ชื่อ ปุ่ม ยกเลิก
  //   dictDefaultMessage : "เลือกรูปภาพ", // ข้อความบนพื้นที่แสดงรูปจะแสดงหลังจากโหลดเพจเสร็จ
  //   dictFileTooBig : "ไม่อนุญาตให้อัพโหลดไฟล์เกิน 1 MB", //ข้อความแสดงเมื่อเลือกไฟล์ขนาดเกินที่กำหนด
  //   acceptedFiles : "image/*", // อนุญาตให้เลือกไฟล์ประเภทรูปภาพได้
  //   filesizeBase: 1000,
  //   // The setting up of the dropzone
  //   init : function() {
  //     myDropzone = this;
  //     this.on("addedfile", function(file) {
  //     }).on("removedfile", function(file) {
  //     }).on("thumbnail", function(file) {
  //     }).on("error", function(file,response) {
  //     }).on("processing", function(file) {
  //     }).on("uploadprogress", function(file,response) {
  //     }).on("success", function(file,response) {
  //           if (response == 'Success') {
  //             swal('ข้อความจากระบบ','บันทึกข้อมูลสำเร็จ','success');
  //             setTimeout(function(){
  //               location.reload();
  //             },500);
  //           }else if (response == 'Error Query') {
  //             swal('ข้อความจากระบบ','บันทึกข้อมูลไม่สำเร็จ','warning');
  //           }else if (response == 'Error Extension') {
  //             swal('ข้อความจากระบบ','ประเภทไฟล์ไม่รองรับ','warning');
  //           }else if (response == 'Error Files') {
  //             swal('ข้อความจากระบบ','กรุณาอัพโหลดไฟล์อีกครั้ง','warning');
  //           }else if (response == 'Require') {
  //             swal('ข้อความจากระบบ','กรุณาข้อมูลให้ครบ','warning');
  //           }else if (response == 'Error File Size') {
  //             swal('ข้อความจากระบบ','ขนาดไฟล์รูปใหญ่เกินไป','warning');
  //           }else {
  //             swal('ข้อความจากระบบ','เกิดข้อผิดพลาด','error');
  //           }
  //     }).on("sendingmultiple", function(file, xhr, formData) {
  //       // formData.append("data",
  //       //   $('#myform').serialize()
  //       // );
  //       formData.append("data",
  //         $('#myform').serialize()
  //       );
  //     });
  //   }
  // });
  //
  // $('#myform').on('submit',function(e){
  //   e.preventDefault();
  //
  //   if (myDropzone.files.length != 0) {
  //     myDropzone.processQueue();
  //   }
  // })
  // if (myDropzone.files.length != 0) {
  //   myDropzone.processQueue();
  // }else {
  //   swal('ข้อความจากระบบ','กรุณาเพิ่มรูปภาพ','warning');
  // }
</script>
