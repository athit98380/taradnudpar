    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <form action="<?= base_url('users/renew') ?>" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-3">เลือกต่ออายุ</label>
              <div class="col-sm-9">
                <select name="year" class="form-control" required>
                  <option value="1">1ปี / 1,500 บาท</option>
                  <option value="2">2ปี / 2,700 บาท</option>
                  <option value="3">3ปี / 3,900 บาท</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-3">หลักฐานการโอนเงิน</label>
              <div class="col-sm-9">
                <input type="file" name="file" class="form-control-file" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-9 ml-auto">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END ROW -->
</div>
<!-- END CONTAINER -->
<?= $this->session->msg ?>
