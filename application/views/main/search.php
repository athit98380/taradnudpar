<?php $URL = $this->uri->segment(2);
			$linkcom='';
			$searchcom ='';
			if ($URL == '') {
				$linkcom = 'main/search';
				$searchcom = "ค้นหาพระเครื่องหรือร้านค้า";
			}
			if ($URL == 'shoplist') {
				$linkcom = 'main/searchran';
				$searchcom = "ค้นหาร้านค้า";
			}
			if ($URL == 'amulet' || $URL == 'amulettop') {
				$linkcom = 'main/searchpra';
				$searchcom = "ค้นหาพระเครื่อง";
			} 
			if ($URL == 'search_result') {
				$linkcom = 'main/search';
				$searchcom = "ค้นหาพระเครื่องหรือร้านค้า";
			} 
			if ($URL == 'search_result_ran') {
				$linkcom = 'main/searchran';
				$searchcom = "ค้นหาร้านค้า";
			}
			if ($URL == 'search_result_pra') {
				$linkcom = 'main/searchpra';
				$searchcom = "ค้นหาพระเครื่อง";
			}?>
<div class="container">
	<div class="row">
		<div class="col-md-12" style="margin-top:10px;">
			<form action="<?= base_url("main/search") ?>" method="post">
				<div class="input-group mb-3">
					<input type="text" id="search-box" class="form-control" placeholder="ค้นหาพระเครื่องหรือร้านค้า" name="search" value="<?php if(!empty(@$search)){echo @$search;} ?>">
					<div class="input-group-append">
						<button type="submit" onclick="return empty()" class="btn btn-primary">ค้นหา</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	function empty() {
		var x;
		x = document.getElementById("search-box").value;
		if (x.trim()=="" ) {
			swal({
				title: "กรุณาใส่คำที่ต้องการค้นหา",
				text: "",
				icon: "warning",
				button: true,
				dangerMode: true,
			});
			return false;
		};
	}
</script>
