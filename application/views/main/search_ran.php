<div class="col-md-8" data-aos="fade-up">
  <h1 class="text-center"><?= $title ?></h1>
  <div class="margin-bottom-60"></div>
  <h3 class="text-center">ผลการค้นหาพระเครื่อง</h3>
  <div class="d-flex justify-content-center">
    <!-- <?= $pagination ?> -->
  </div>
  <div class="d-flex justify-content-center">
    <!-- <?php echo '<span class="price">' . $search . '</span>'; ?> -->
  </div>
  <h3 class="text-center">ผลการค้นหาร้านค้า</h3>
  <?php if (count($shoplist) <= 0) : ?>
    <p class="text-center">ไม่พบข้อมูลร้านค้า</p>
  <?php else : ?>
    <div class="row">
      <?php $nub = 0;
        foreach ($shoplist as $row) : $nub++ ?>
        <div class="col-sm-4 col-md-4 col-lg-2 mb-2 percent_50_mobile">
          <div class="card-custom">
            <div class="view">
              <a href="<?= base_url("main/shop/{$row->shop_id}") ?>">
                <?php if (!empty($row->user_image)) { ?>
                  <img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$row->user_image}") ?>" class="img-fluid rounded-circle" alt="">
                  <?php echo $row->users_id; ?>
                <?php } else { ?>
                  <img src="<?= base_url('assets/images/nobody_m.original.jpg') ?>" class="img-fluid rounded-circle" alt="">
                <?php } ?>
              </a>
            </div>
            <div class="card-body text-center">
              <a href="<?= base_url("main/shop/{$row->shop_id}") ?>">
                <h5 class="card-title text-ellipsis" style="height: 25px;"><?= $row->shop_name ?></h5>
                <small><?= $row->first_name . " " . $row->last_name ?></small>
              </a>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="d-flex justify-content-center">
      <?php echo '<span class="price">' . $nub . '</span>'; ?>
    </div>
    <div class="d-flex justify-content-center">
        <a class="fa fa-eye btn btn-primary" href=""> ชมทั้งหมด</a>
      </div>
  <?php endif; ?>
</div>
<?php $this->view('main/json'); ?>