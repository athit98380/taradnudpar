<div class="col-md-8" data-aos="fade-up">
  <h1 class="text-center"><?= $title ?></h1>
  <div class="row">
    <?php foreach($shoplist as $row) : ?>
    <div class="col-sm-4 col-md-4 col-lg-2 mb-2 percent_50_mobile">
        <div class="card-custom">
            <div class="view">
                <a href="<?= base_url("main/shop/{$row->shop_id}") ?>">
                    <?php if (!empty($row->user_image)) {?>
                        <img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$row->user_image}") ?>" class="img-fluid rounded-circle" alt="">
                        <?php echo $row->users_id;?>
                    <?php }else{?>
                        <img src="<?= base_url('assets/images/nobody_m.original.jpg') ?>" class="img-fluid rounded-circle" alt="">
                    <?php }?>
                </a>
            </div>
            <div class="card-body text-center">
                <a href="<?= base_url("main/shop/{$row->shop_id}") ?>">
                    <h5 class="card-title text-ellipsis" style="height: 25px;"><?= $row->shop_name ?></h5>
                    <small><?= $row->first_name." ".$row->last_name ?></small>
                </a>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
  </div>
</div>