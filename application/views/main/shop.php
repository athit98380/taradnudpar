<?php if(!empty($shop->shop_banner)){?>
<div class="text-center" style="max-height: 600px;margin-bottom: 80px;overflow: hidden;">
  <img src="<?php echo base_url().'assets/uploads/numberid/'.$shop->users_id.'/'.$shop->shop_banner;?>">
</div>
<?php }?>
<div class="container-custom">
  <div class="row mt-2">
    <div class="col-md-2 percent_50_mobile">
      <div class="margin-bottom-60"></div>
      <div class="card" style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>">
        <div class="card-image-shop">
          <?php if (!empty($user->user_image)) {?>
            <img src="<?= base_url("assets/uploads/numberid/{$user->id}/{$user->user_image}") ?>" class="img-fluid rounded-circle" alt="">
          <?php }else{?>
            <img src="<?= base_url('assets/images/nobody_m.original.jpg') ?>" class="img-fluid rounded-circle" alt="">
          <?php }?>
        </div>
        <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>">
          <h4 class="text-center"><i class="fas fa-store"></i> <?= $shop->shop_name ?></h4>
          <table class="table" style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>">
            <tbody>
              <tr>
                <td>ผู้เข้าชม</td>
                <td><?= $shop->shop_view ?></td>
              </tr>
              <tr>
                <td>ที่อยู่</td>
                <td><?= $shop->shop_address ?></td>
              </tr>
              <tr>
                <td>โทร</td>
                <td><?= @$user->user_tel ?></td>
              </tr>
              <?php if(!empty($shop->shop_qrcode)){?>
              <tr>
                <td colspan="2"><?php if(!empty($shop->shop_qrcode)){?><a href="<?php echo $shop->shop_qrcode_link;?>" target="_blank"><?php }?><img src="<?= base_url('assets/uploads/numberid/'.$user->id.'/'.$shop->shop_qrcode) ?>" class="img-fluid" style="max-width:250px!important;" alt="..."><?php if(!empty($shop->shop_qrcode)){?></a><?php }?></td>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>">
          <h6><i class="fas fa-bars"></i> ข้อมูลร้านค้า</h6>
          <p><?= $shop->shop_detail ?></p>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>">
          <h6><i class="fas fa-bars"></i> การชำระเงิน</h6>
          <table class="table" style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>">
            <tbody>
              <tr>
                <td>ชื่อบัญชี</td>
                <td><?= $shop->shop_bank_name_account ?></td>
              </tr>
              <tr>
                <td>เลขที่บัญชี</td>
                <td><?= $shop->shop_bank_number ?></td>
              </tr>
              <tr>
                <td>ธนาคาร</td>
                <td><?= $shop->shop_bank_name ?></td>
              </tr>
              <tr>
                <td>สาขา</td>
                <td><?= $shop->shop_bank_sub ?></td>
              </tr>
              <tr>
                <td>ประเภทบัญชี</td>
                <td><?= $shop->shop_bank_type ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-8" data-aos="fade-up">
      <?php if (!empty($products)) {?><h1 class="text-center">รายการอัพเดทล่าสุด</h1><?php }?>
      <div class="row">
      <?php $num = 0;
        foreach ($products as $row) : $num++ ?>
        <div class="col-sm-6 col-md-6 col-lg-2 mb-2 percent_50_mobile">
          <div class="card card-custom" style="<?php echo "background-color: #" . $theme_for_design->theme_p_list_bg . "!important;"; ?>">
            <div class="view">
              <?php $images = explode(",", $row->product_images); ?>
              <img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$images[0]}") ?>" data-id="<?= $row->product_id ?>" class="card-img-top" width="100px" height="100px" alt="<?= $row->product_name ?>">
            </div>
            <div class="card-body" style="<?php echo "color: #" . $theme_for_design->theme_p_list_font . "!important;"; ?>">
              <h5 class="card-title text-ellipsis" style="height: 25px;"><?= $row->product_name ?></h5>
              <p class="card-text text-ellipsis" style="margin-bottom: 0px; height: 1.5em;"><?= $row->product_detail ?></p>
              <p class="card-text text-ellipsis" style="height: 1.5em;font-size: 11px;"><?= $this->datethai->DateLong($row->product_create_on) ?></p>
              <hr>
              <p class="" style="margin-bottom: 0px; height: 1.5em;">
                <?php
                    if ($row->product_status_for_sale != '') {
                      if ($row->product_status_for_sale == 'ขายแล้ว') {
                        echo '<span class="sale"><i class="fas fa-times"></i>  ' . $row->product_status_for_sale . ' </span>';
                      } elseif ($row->product_status_for_sale == 'โชว์พระ') {
                        echo '<span class="show"><i class="fas fa-check"></i>  ' . $row->product_status_for_sale . ' </span>';
                      } elseif ($row->product_status_for_sale == 'โทรถาม') {
                        echo '<span class="call"><i class="fas fa-phone"></i>  ' . $row->product_status_for_sale . ' </span>';
                      } elseif ($row->product_status_for_sale == 'เสนอราคา') {
                        echo '<span class="price">&#3647 ' . number_format($row->product_price, 0) . '</span>';
                      }
                    }
                    ?>
              </p>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>
    <div class="d-flex justify-content-center">
      <!-- <?= $pagination ?> -->
    </div>
      <?php if (empty($products)) {?><h1 class="text-center">ร้านค้านี้ยังไม่มีสินค้า</h1><?php }?>
    </div>
    <div class="col-md-2">
      <div class="margin-bottom-60"></div>
      <div class="card">
        <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_d_news_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_news_font ."!important;";?>">
          <h6><i class="fas fa-bars"></i> ประกาศจากร้าน</h6>
          <?php if ($shop->shop_id==1) {?>
            <div class="row">
              <?php foreach ($news as $row) : ?>
                <div class="col-md-12">
                  <div class="card text-left">
                    <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_d_news_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_news_font ."!important;";?>">
                      <a style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>" href="<?= base_url("main/news_detail/{$row->blog_id}") ?>">
                      <h5 class="card-title"><i class="fas fa-sign-in-alt"></i> <?= $row->blog_name ?></h5>
                      <!-- <div class="card-text-custom"><?= $row->blog_detail ?></div> -->
                      </a>
                    </div>
                  </div>
                </div>
            <?php endforeach; ?>
            </div>
          <?php }?>
          <p><?= (isset($blog->blog_detail)) ? $blog->blog_detail : null ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->view('main/json'); ?>