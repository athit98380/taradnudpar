<!-- Modal -->
<style type="text/css">
  .img-fluid{
    background-size: contain; background-position: center; background-repeat: no-repeat; background-color: rgba(0,0,0,1); 
  }
  img{
    border-radius: 3px;
    
  }
 /* #long img.opaque, */#thunk img {
    border: 2px solid black;
  }
  #thunk img.active{
    border: 2px solid yellow;
  }
  .products_slider #long {
    position: relative;
    width: auto;height: auto;
    margin:0 auto 10px;
  }
  #long img{
    max-width: 100%!important;max-height: 100%!important;width: auto!important;height: auto!important;
  }
  #long img{
    position: absolute;
    transition: opacity 0.8s ease-in-out;
    opacity: 0;
    left: 0; 
    right: 0;
    margin-left: auto; 
    margin-right: auto;
  }
  #long img.opaque{
    position: relative;
    transition: opacity 0.8s ease-in-out;
    opacity: 1;
  }
  #long img.active{
    width:100% !important;height:100% !important;opacity:0;z-index:2;top:0;left:0;position:absolute;
  }
  img[src=""] {
    visibility: hidden;
    display: none;
  }

</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="<?php echo "background-color: #".$theme_for_design->theme_p_modal_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_p_modal_font."!important;";?>">
      <div class="modal-body">
        <div class="products_slider">
          <div class="carousel mobile shadow" id="long" style="height: auto;">
            <?php for($i=0;$i<10;$i++){?>            
              <img class="open_img" src="" id="top<?=$i?>" >
              <img class="img-fluid" src="<?= base_url("assets/uploads/numberid/blank_500x500.gif") ?>" id="photo_top<?=$i?>" alt="" srcset="">
            <?php }?>
          </div>
          <div class="thumbnails" id="thunk">
            <?php for($i=0;$i<10;$i++){?>
              <span><img src="" id="photo<?=$i?>" alt="" srcset="" onclick="change_photo_top('<?=$i?>');"></span>
            <?php }?>
          </div>          
        </div>

        <h2 id="title"></h2>
        <div class="row">
          <div class="col-md-6">
            <h3>ราคา</h3>
            <h3 id="price">ราคา</h3>
          </div>
          <div class="col-md-6">
            <h3>สถานะ</h3>
            <h3><span id="status"></span></h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h3>รายละเอียด</h3>
            <h4 id="detail"></h4>
            <p id="pid">รหัสรายการ (PID) : </p>
            <p id="countview">จำนวนผู้เข้าดู :  0 ครั้ง</p>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-6">
            <h4 id="sid"></h4>
            <h4 id="sname"></h4>
            <h4 id="sphone"></h4>
            <h4 id="pdate"></h4>
          </div>
          <div class="col-md-6">
            <a href="#" id="image_link_qrcode" target="_blank"><img src="" id="image_focus_qrcode" alt="" style="max-width:250px!important;"></a>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a type="button" id="go-to-shop" class="btn btn-primary" href="">เยี่ยมชมร้าน</a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script>
  $(document).on('click','img',function(){
    $.ajax({
      url: '<?= base_url('main/ajaxmodal') ?>',
      method: 'get',
      data: {
        id: $(this).data('id')
      },
      dataType: 'json',
      success: function(data){
        $('#exampleModal').modal('show');
        $('#exampleModalLabel').html(data.product_name);

        var res = data.product_images.split(",");
        for(var i = 0; i < res.length; i++){
          if(res[i]!=""){
            var roop = '<?= base_url("assets/uploads/numberid/") ?>' + data.users_id + '/' + res[i];
            $('#photo_top'+i).css({
              "background-image": 'url('+roop+')'
            });
            $('#top'+i).attr('src',roop);
          }
          if(i==0){
            // $('#photo_top'+i).addClass('active');
            $('#photo_top'+i).addClass('opaque');
            $('#top'+i).addClass('active');
          }
        }

        var res = data.product_image.split(",");
        for(var i = 0; i < res.length; i++){
          if(res[i]!=""){
            $('#photo'+i).attr('src','<?= base_url("assets/uploads/numberid/") ?>' + data.users_id + '/' + res[i]);
          }
          if(i==0){
            $('#photo'+i).addClass('active');
          }
        }
        
        $('#image_focus_qrcode').attr('src','<?= base_url("assets/uploads/numberid/") ?>' + data.users_id + '/' +  data.shop_qrcode);
        $('#image_link_qrcode').attr('href', data.shop_qrcode_link);

        if(data.product_status_for_sale == null) 
        {
          $('#status').html('มาใหม่');
        }
        else
        {
          if(data.product_status_for_sale == "โชว์พระ") 
          {
            $('#status').html('<span class="show"><i class="fas fa-check"></i>  โชว์พระ </span>');
            $('#price').html('<span class="show">'+data.product_price+ ' บาท</span>');
          }
          if(data.product_status_for_sale == "ขายแล้ว") 
          {
            $('#status').html('<span class="sale"><i class="fas fa-times"></i>  ขายแล้ว </span>');
            $('#price').html('<span class="sale">'+data.product_price+ ' บาท</span>');
          }
          if(data.product_status_for_sale == "โทรถาม") 
          {
            $('#status').html('<span class="call"><i class="fas fa-phone"></i>  โทรถาม </span>');
            $('#price').html('<span class="call">'+data.product_price+ ' บาท</span>');
          }
          if(data.product_status_for_sale == "เสนอราคา") 
          {
            $('#status').html('<span class="price"><i class="fas fa-dollar-sign"></i>   เสนอราคา </span>');
            $('#price').html('<span class="price">'+data.product_price+ ' บาท</span>');
          }
        }
        $('#title').html(data.product_name);
        $('#detail').html(data.product_detail);
        $('#pid').html('รหัสรายการ (PID) : ' + data.product_no);
        $('#countview').html('จำนวนผู้เข้าดู : ' + data.product_view + ' ครั้ง');

        $('#sid').html('รหัสร้านค้า (SID) ' + data.shop_id);
        $('#sname').html('ชื่อร้านค้า ' + data.shop_name);
        $('#sphone').html('เบอร์โทรศัพท์ ' + data.user_tel);
        $('#pdate').html('วันที่ลงประกาศ ' + data.product_create_on);

        $('#go-to-shop').attr('href','<?= base_url("main/shop/") ?>' + data.shop_id);
        $( ".open_img" ).click(function(e) {      
          window.open($(this).attr('src'), "_self");    
        });
      }
    });
  });
</script>
<script type="text/javascript">
  function change_photo_top(id)
  {
    for (i = 0; i < 10; i++) {
      if (id==i) {
        // document.getElementById("photo_top"+i).classList.add('active');
        document.getElementById("photo_top"+i).classList.add('opaque');
        document.getElementById("top"+i).classList.add('active');
        document.getElementById("photo"+i).classList.add('active');
      }else{
        // document.getElementById("photo_top"+i).classList.remove('active');
        document.getElementById("photo_top"+i).classList.remove('opaque');
        document.getElementById("top"+i).classList.remove('active');
        document.getElementById("photo"+i).classList.remove('active');
      }
    }
  }
</script>