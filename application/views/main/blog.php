<div class="col-md-9" data-aos="fade-up">
  <h2 class="text-center">ข่าวประชาสัมพันธ์</h2>
  <div class="card" >
    <div class="card-body-news" >
      <div class="row">
        <div class="col-sm-4 percent_50_mobile">
          <img src="<?= base_url("assets/uploads/{$newsDetail->blog_image}") ?>" style="height: unset!important;">
        </div>
        <div class="col-sm-8">
          <h3 class="text-left"><?= $newsDetail->blog_name ?></h3>
          <small class="text-center"  ><?= $this->datethai->DateLong($newsDetail->blog_update_on) ?></small>
          <?= $newsDetail->blog_detail ?>
        </div>
      </div>
    </div>
  </div>
</div>
