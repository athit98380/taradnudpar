<div class="col-md-8" data-aos="fade-up">
  <h1 class="text-center">รายการอัพเดท</h1>
  <div class="d-flex justify-content-center">
    <?= $pagination ?>
  </div>
  <div class="row">
    <?php $s=0; foreach ($products as $row) : ?>
    <div class="col-sm-6 col-md-6 col-lg-2 mb-2 percent_50_mobile">
      <div class="card card-custom"  style="<?php echo "background-color: #".$theme_for_design->theme_p_list_bg."!important;";?>">
        <div class="view">
          <?php  if($row->product_image!=''){$s++;} $theme=$theme_for_design->theme_p_list_font;
          $images=explode( ",",$row->product_image);?>
          <img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$images[0]}") ?>" data-id="<?= $row->product_id ?>" class="img-fluid" alt="<?= $row->product_name ?>">
        </div>
        <div class="card-body" style="<?php echo "color: #".$theme_for_design->theme_p_list_font."!important;";?>">
          <h5 class="card-title text-ellipsis" style="height: 25px;"><?= $row->product_name ?></h5>
          <p class="card-text text-ellipsis" style="margin-bottom: 0px; height: 1.5em;"><?= $row->product_detail ?></p>
          <!--  <p class="card-text text-ellipsis" style="height: 1.5em;font-size: 11px;"><?= $this->datethai->DateLong($row->product_create_on) ?></p> -->
          <p class="card-text text-ellipsis" style="height: 1.5em;font-size: 0.75em;"><?= $this->datethai->DateLong($row->product_create_on) ?></p>
          <p align="right" class="card-text text-ellipsis" style="height: 1.5em;font-size: 0.7em;"><?= $row->product_view ?></p>
          <hr >
          <p class="" style="margin-bottom: 0px; height: 1.5em;">
            <?php
            if($row->product_status_for_sale != '') {
              if ($row->product_status_for_sale == 'ขายแล้ว') {
                echo '<span class="sale"><i class="fas fa-times"></i>  '.$row->product_status_for_sale.' </span>';
              }elseif ($row->product_status_for_sale == 'โชว์พระ') {
                echo '<span class="show"><i class="fas fa-check"></i>  '.$row->product_status_for_sale.' </span>';
              }elseif ($row->product_status_for_sale == 'โทรถาม') {
                echo '<span class="call"><i class="fas fa-phone"></i>  '.$row->product_status_for_sale.' </span>';
              }elseif ($row->product_status_for_sale == 'เสนอราคา') {
                echo '<span class="price">&#3647 '.number_format($row->product_price,0).'</span>';
              }
            }
            ?>
          </p>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>
<div class="d-flex justify-content-center">
  <?= $pagination ?>
</div>
<div class="d-flex justify-content-center">
  <!-- <?php $pagination ?> -->
</div>
</div>
<?= $this->session->msg ?>
<?php $this->view('main/json'); ?>
<script>
  $(document).ready(function(){
    $(".card-body").hover(function(){
      $(this).css("color", "#ffc107");
    }, function(){
      $(this).css("color", "#ffffff");
    });
  });
</script>
