<div class="col-md-9" data-aos="fade-up">
  <h2 class="text-center">ข่าวประชาสัมพันธ์</h2>
  <div class="row">
    <?php foreach ($news as $row) : ?>
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 percent_50_mobile" data-aos="fade-up">
        <div class="card text-center" style="min-height: 280px;max-height: 280px;">
          <div class="view">
            <a href="<?= base_url("main/news_detail/{$row->blog_id}") ?>"><img src="<?= base_url("assets/uploads/$row->blog_image") ?>" class="card-img-top" alt="<?= $row->blog_name ?>"></a>
          </div>
          <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_p_list_bg."!important;color: #".$theme_for_design->theme_p_list_font."!important;";?>">
            <h5 class="card-title"><?= $row->blog_name ?></h5>
            <!-- <div class="card-text-custom"><?= $row->blog_detail ?></div> -->
            <a href="<?= base_url("main/news_detail/{$row->blog_id}") ?>" class="btn btn-primary">อ่านเพิ่มเติม</a>
          </div>
        </div>
      </div>
  <?php endforeach; ?>
  </div>
  <div class="row">
    <div class="">
      <?= $pagination ?>
    </div>
  </div>
</div>
