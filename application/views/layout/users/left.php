<div class="container mt-2">
  <div class="row">
    <div class="col-md-4">

      <div class="card">
        <div class="card-header" style="<?php echo "color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "background-color: #".$theme_for_design->theme_page_font."!important;";?>">
          เมนู
        </div>
        <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_page_font."!important;";?>">
          <div class="list-group">
            <a href="<?= base_url('users') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == null) ? 'active' : null ?>">โปรไฟล์</a>
            <a href="<?= base_url('main/reset_password') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == 'reset_password') ? 'active' : null ?>">เปลี่ยนรหัสผ่าน</a>
          <?php if (!empty($shop->shop_id)) {?>
            <a href="<?= base_url('users/add_product') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == 'add_product') ? 'active' : null ?>">เพิ่มสินค้า</a>
            <a href="<?= base_url('users/productlist/0') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(3) == '0') ? 'active' : null ?>">สินค้าเผยแพร่</a>
            <a href="<?= base_url('users/productlist/1') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(3) == '1') ? 'active' : null ?>">สินค้ารออนุมัติ</a>
            <?php if(@$shop->users_id==1){?>
            <a href="<?= base_url('admin/news') ?>" target="_blank" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == 'notify') ? 'active' : null ?>">ประกาศจากร้าน</a>
            <?php }else{?>
            <a href="<?= base_url('users/notify') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == 'notify') ? 'active' : null ?>">ประกาศจากร้าน</a>
            <?php }?>
            <?php if(!empty($left)){?>
              <?php if($left->users_id!=1){?>
                <?php if (@$left->sub_order_status == 0) : ?>
                <a href="<?= base_url('users/photoactive') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == 'photoactive') ? 'active' : null ?>">รูปพระเครื่องยืนยัน <?= (@$left->image_register_comment != null) ? '(มีอัพเดต)' : null; ?></a>
                <?php endif; ?>
                <a href="<?= base_url('users/renew') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == 'renew') ? 'active' : null ?>">ต่ออายุ</a>
                <?php }?>
              <?php }?>
            <?php }else{?>
            <a href="<?= base_url('users/register_shop') ?>" class="list-group-item list-group-item-action <?= ($this->uri->segment(2) == 'register_shop') ? 'active' : null ?>">สมัครร้านค้า</a>
            <?php }?>
          </div>
        </div>
      </div>

    </div>
  <!-- END COL 4 -->
