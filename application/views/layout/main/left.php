<!-- Container -->
<div class="container-custom">
  <!-- Row -->
  <div class="row">
    <div class="col-md-2">
      <!-- Product Type -->
      <div class="row">
        <div class="col-md-12">
          <h5><i class="fas fa-bars"></i> หมวดพระเครื่อง</h5>
          <div class="list-group">
            <?php foreach ($product_type as $row) : ?>
            <a id="type" style="<?php echo "background-color: #".$theme_for_design->theme_d_category_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_category_font."!important;";?>" href="<?= base_url("main/category/{$row->product_type_id}") ?>" class="list-group-item list-group-item-action prapet<?= ($this->uri->segment(2) == 'category' && $this->uri->segment(3) == $row->product_type_id) ? 'active' : null ?>"><i id="tname" class="fas fa-caret-right"></i> <?= $row->product_type_name ?></a>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <!-- End Product Type -->
    <div class="row mt-2">
      <div class="col-md-12">
        <h5><i class="fas fa-bars"></i> ร้านค้าแนะนำ</h5>
        <div>
          <?php foreach ($recommend as $row) : ?>
            <a  class="mini-image-profile-shop" href="<?= base_url("main/shop/{$row->shop_id}") ?>"><img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$row->user_image_profile}") ?>"></a>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <div class="row mt-2">
      <div class="col-md-12">
        <h5><i class="fas fa-bars"></i> สถิติเว็บไซต์</h5>
        <table class="table">
          <tbody>
            <tr>
              <td>สินค้า</td>
              <td><?= $countProduct ?></td>
            </tr>
            <tr>
              <td>ผู้ชมทั้งหมด</td>
              <td><?= @$static->static_view_all ?></td>
            </tr>
            <tr>
              <td>ผู้ชมวันนี้</td>
              <td><?= @$static->static_view_today ?></td>
            </tr>
            <tr>
              <td>ผู้ชมขณะนี้</td>
              <td><?php echo $visitorOnline ;?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function () {
      $(".prapet").hover(function () {
        $(this).css("background-color", 'white');
        $(this).css("color", '#191919');
        $(".prapet").not(this).each(function(){
          $(this).css("background-color", "#191919");
        });
      }, function(){
       $(".prapet").css("background-color", '#191919');
       $(this).css("color", 'white');
     });
    });
  </script>
