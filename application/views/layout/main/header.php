<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php if(!empty($title)){echo @$title;} ?></title>
	<meta name="description" content="<?php if(!empty($description)){echo @$description;}?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">
	<link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/logo/favicon.png') ?>"/>

	<script type="text/javascript" src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/popper.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/dropzone.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/dropzone-amd-module.js') ?>"></script>
	<script type="text/javascript" src="//cdn.ckeditor.com/4.11.3/basic/ckeditor.js"></script>
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</head>
<?php $theme=$this->theme->Get_theme();?>
<?php if (!empty($theme->theme_bg_image)) {
	$theme_bg_image="background-image: url(".base_url()."assets/images/theme/".$theme->theme_bg_image.");";
}else{
	$theme_bg_image="";
}?>
<body style="<?php echo $theme_bg_image;?><?php echo "background-color: #".$theme_for_design->theme_color_bg_all."!important;";?><?php echo "color: #".$theme_for_design->theme_color_font_all."!important;";?>">
	<?php if (!empty($theme->theme_h_image)) {
		$theme->theme_h_image="background: url(".base_url()."assets/images/theme/".$theme->theme_h_image.")  no-repeat top center fixed!important;";
	}else{
		$theme->theme_h_image="";
	}?>
	<?php
	$theme->theme_h_height="max-height:".$theme->theme_h_height."px!important;";
	?>
	<!-- Header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark"  style="<?php echo $theme->theme_h_image;echo $theme->theme_h_height;?>-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;margin-bottom: 60px;border-bottom: 2px solid;">
			<div class="container height_header">
				<div class="naver-logo-top">
					<a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url('assets/images/logo/logo-w800.png') ?>"></a>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-top: 20px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;<?php echo "background-color: #".$theme_for_design->theme_d_menu_bg."!important;";?>">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item <?= ($this->uri->segment(1) == null) ? 'active' : null ?>">
							<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link" href="<?= base_url() ?>"><?= $menu[0]->menu_name ?> <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(2) == 'amulet') ? 'active' : null ?>">
							<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link" href="<?= base_url('main/amulet') ?>"><?= $menu[1]->menu_name ?></a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(2) == 'amulettop') ? 'active' : null ?>">
							<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link" href="<?= base_url('main/amulettop') ?>"><?= $menu[2]->menu_name ?></a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(2) == 'shoplist') ? 'active' : null ?>">
							<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link" href="<?= base_url('main/shoplist') ?>"><?= $menu[3]->menu_name ?></a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(2) == 'news') ? 'active' : null ?>">
							<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link" href="<?= base_url('main/news') ?>"><?= $menu[4]->menu_name ?></a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(2) == 'usage') ? 'active' : null ?>">
							<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link" href="<?= base_url('page/usage') ?>"><?= $menu[5]->menu_name ?></a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(2) == 'contact') ? 'active' : null ?>">
							<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link" href="<?= base_url('page/contact') ?>"><?= $menu[6]->menu_name ?></a>
						</li>
					</ul>
					<?php if (isset($this->session->uid)): ?>
						<ul class="navbar-nav ml-auto">
							<li class="nav-item dropdown">
								<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fas fa-sliders-h"></i> ข้อมูลส่วนตัว
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown" style="<?php echo "background-color: #".$theme_for_design->theme_d_menu_bg."!important;";?>">
									<?php if ($this->session->user_type == 'admin'): ?>
										<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="dropdown-item" href="<?= base_url('admin') ?>"><i class="fas fa-archway"></i> ผู้ดูแลระบบ</a>
									<?php endif; ?>
									<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="dropdown-item" href="<?= base_url('users') ?>"><i class="fas fa-address-card"></i> โปรไฟล์</a>
									<div class="dropdown-divider"></div>
									<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="dropdown-item" href="<?= base_url('login/logout') ?>"><i class="fas fa-sign-out-alt"></i> ออกจากระบบ</a>
								</div>
							</li>
						</ul>
						<?php else: ?>
							<ul class="navbar-nav ml-auto">
								<li class="nav-item mr-2">
									<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="btn btn btn-outline-warning btn-sm" href="<?= base_url('register') ?>"><i class="fas fa-user-edit"></i> ลงทะเบียน</a>
								</li>
								<li class="nav-item">
									<a style="<?php echo "color: #".$theme_for_design->theme_d_menu_font."!important;";?>" class="btn btn btn-outline-success btn-sm" href="<?= base_url('main/login') ?>"><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</a>
								</li>
							</ul>
						<?php endif; ?>

				    <!-- <form class="form-inline my-2 my-lg-0">
				      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				  </form> -->
				</div>
			</div>
		</nav>
	</header>
	<!-- End Header -->
	<script >
		$(document).ready(function(){
			$(".nav-link").hover(function(){
				// $(this).css("background-color", "#ffffff");
				$(this).css("color", "#ffc107");
			}, function(){
				// $(this).css("background-color", "#191919");
				$(this).css("color", "#ffffff");
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(".dropdown-item").hover(function(){
				$(this).css("color", "#ffc107");
			}, function(){
				$(this).css("color", "#ffffff");
			});
		});
	</script>
