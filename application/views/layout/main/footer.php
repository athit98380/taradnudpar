  <script>
  	AOS.init();
  </script>
  <!--footer starts from here-->
  <?php $theme=$this->theme->Get_theme();?>
  <?php if (!empty($theme->theme_f_image)) {
  	$theme->theme_f_image="background: url(".base_url()."assets/images/theme/".$theme->theme_f_image.")  no-repeat top center;";
  }else{
  	$theme->theme_f_image="";
  }?>
  <?php
  $theme->theme_f_height="height:".$theme->theme_f_height."px!important;";
  ?>
  <div class="margin-bottom-60"></div>
<!-- <div class="container">
	<div class="row">
		<div class="col-md-12"><?php echo $theme->theme_f_detail;?></div>
	</div>
</div> -->
<div id="footer" style="<?php echo $theme->theme_f_image;echo $theme->theme_f_height;?>>-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;margin-top: 50px;width: 100%;">
</div>
<div class="footer_licen"><?php echo $theme->theme_f_all_right;?></div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142148105-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-142148105-1');
</script>
</body>
</html>

