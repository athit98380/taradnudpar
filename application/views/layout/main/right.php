  <div class="col-md-2">
    <div class="row">
      <div class="col-md-12">
        <h5><i class="fas fa-newspaper"></i> ข่าวประชาสัมพันธ์</h5>
        <div class="list-group">
          <?php $num=0; foreach ($news as $row) : $num++;?>
          <a  style="<?php echo "background-color: #".$theme_for_design->theme_d_news_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_d_news_font ."!important;";?>" href="<?= base_url("main/news_detail/{$row->blog_id}") ?>" class="list-group-item list-group-item-action khaw">
            <p><?= $row->blog_name ?></p>
            <small><?= $this->datethai->DateLong($row->blog_update_on) ?></small>
          </a>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End Row -->
</div>
<!-- End Container -->
<script type="text/javascript">
  $(document).ready(function () {
    $(".khaw").hover(function () {
      $(this).css("color", '#ffc107');
      $(".khaw").not(this).each(function(){
        $(this).css("color", '#ffffff');
      });
    }, function(){
     $(this).css("color", '#ffffff');
   });
  });
</script>
