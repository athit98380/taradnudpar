<div class="margin-bottom-60"></div>
<div class="container mt-2">
 <div class="row">
   <div class="col-md-6 mx-auto text-center">
   	<a class="btn btn btn-outline-primary btn-lg" href="<?= base_url('register/member') ?>"><i class="fas fa-sign-in-alt"></i> สมัครสมาชิก</a>
   </div>
   <div class="col-md-6 mx-auto text-center">
   	<a class="btn btn btn-outline-warning btn-lg" href="<?= base_url('register/member/shop') ?>"><i class="fas fa-sign-in-alt"></i> สมัครร้านค้า</a>	
   </div>
 </div>
</div>