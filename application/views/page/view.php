<div class="container">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="card">
        <div class="card-header" style="<?php echo "color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "background-color: #".$theme_for_design->theme_page_font."!important;";?>">
          <?= $page_detail->menu_name ?>
        </div>
        <div class="card-body" style="<?php echo "background-color: #".$theme_for_design->theme_page_bg."!important;";?><?php echo "color: #".$theme_for_design->theme_page_font."!important;";?>">
            <?= (isset($page_detail->menu_detail)) ? $page_detail->menu_detail : 'ไม่มีรายละเอียด' ?>
        </div>
      </div>
    </div>
    <div class="col-md-2"></div>
  </div>  
</div> 