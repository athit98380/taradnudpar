<div class="container mt-2">
 <div class="row">
   <div class="col-md-12 mx-auto">
		 <div class="card">
			 <div class="card-header">
				 สมัครสมาชิก
			 </div>
			 <div class="card-body">
          <div class="row">
            <div class="col-md-6 text-center">
              <span class="dot-circle" style="background-color: #ddd;">1</span> ขั้นตอนที่ 1 สมัครสมาชิก
            </div>
            <div class="col-md-6 text-center">
              <span class="dot-circle" style="background-color: #d1a500;">2</span> ขั้นตอนที่ 2 เปิดร้านค้า
            </div>
          </div>
          <div class="margin-bottom-60"></div>
         <?php echo $this->session->msg; //Show Alert Error Login ?>
				 <form action="<?= base_url('register/shop') ?>" method="post" enctype="multipart/form-data">
<!--            <h2 class="text-center">แบบฟอร์มสมัครสมาชิกร้านค้า</h2>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Email : </label>
						 <div class="col-sm-10">
							 <input type="email" name="email" class="form-control" value="<?= set_value('email') ?>" required>
               <?= form_error('email','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Password : </label>
						 <div class="col-sm-10">
							 <input type="password" name="password" class="form-control" minlength="8" required>
               <?= form_error('password','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Confirm Password : </label>
						 <div class="col-sm-10">
							 <input type="password" name="ConfirmPassword" class="form-control" minlength="8" required>
               <?= form_error('ConfirmPassword','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
           <h2 class="text-center">ข้อมูลประวัติส่วนตัว</h2>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ชื่อ : </label>
             <div class="col-sm-10">
               <input type="text" name="fname" class="form-control" value="<?= set_value('fname') ?>" required>
               <?= form_error('fname','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">นามสกุล : </label>
             <div class="col-sm-10">
               <input type="text" name="lname" class="form-control" value="<?= set_value('lname') ?>" required>
               <?= form_error('lname','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">เบอร์โทรศัพท์ : </label>
             <div class="col-sm-10">
               <input type="text" name="tel" class="form-control" value="<?= set_value('tel') ?>" minlength="10" maxlength="10" required>
               <?= form_error('tel','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">เลขที่บัตรประชาชน : </label>
             <div class="col-sm-10">
               <input type="text" name="numberid" class="form-control" value="<?= set_value('numberid') ?>" minlength="13" maxlength="13" required>
               <?= form_error('numberid','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ไฟล์รูปบัตรประชาชน : </label>
             <div class="col-sm-10">
               <input type="file" name="imageid" class="form-control-file" required accept="image/jpeg">
               <?= form_error('imageid','<small class="text-danger">','</small>') ?>
             </div>
           </div> -->
          
           <h2 class="text-center">รายละเอียดร้านค้า</h2>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ชื่อร้านค้า : </label>
             <div class="col-sm-10">
               <input type="text" name="shopname" class="form-control" value="<?= set_value('shopname') ?>" required>
               <?= form_error('shopname','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ที่ตั้งร้านค้า : </label>
             <div class="col-sm-10">
               <textarea name="shop_address" class="form-control" rows="4" cols="80" required><?= set_value('shop_address') ?></textarea>
               <?= form_error('shop_address','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">รายละเอียดร้านค้า : </label>
             <div class="col-sm-10">
               <textarea name="shopdetail" class="form-control" rows="4" cols="80" required><?= set_value('shopdetail') ?></textarea>
               <?= form_error('shopdetail','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">เงื่อนไขการรับประกัน : </label>
             <div class="col-sm-10">
               <textarea name="shop_guarantee" class="form-control" rows="4" cols="80" required><?= set_value('shop_guarantee') ?></textarea>
               <?= form_error('shop_guarantee','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ผู้รับรองการเปิดร้านค้า : </label>
             <div class="col-sm-10">
               <input type="text" name="shop_guarantee_by" class="form-control" value="<?= set_value('shop_guarantee_by') ?>" required>
               <?= form_error('shop_guarantee_by','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">โทรศัพท์ผู้รับรอง : </label>
             <div class="col-sm-10">
               <input type="text" name="shop_guarantee_by_tel" class="form-control" value="<?= set_value('shop_guarantee_by_tel') ?>" minlength="10" maxlength="10" required>
               <?= form_error('shop_guarantee_by_tel','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">การจำหน่ายเครื่องราง : </label>
             <div class="col-sm-10">
               <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="customCheck1" name="checkbox1" required>
                  <label class="custom-control-label" for="customCheck1">ยอมรับข้อตกลง "ไม่ขายหรือโชว์เครื่องรางของขลังทุกชนิด"</label>
                </div>
                <?= form_error('checkbox1','<small class="text-danger">','</small>') ?>
             </div>
           </div>

           <h2 class="text-center">ข้อมูลบัญชีธนาคาร</h2>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ธนาคาร : </label>
             <div class="col-sm-10">
               <select class="form-control" name="shop_bank_name" required>
                 <option value="" selected="selected">เลือกธนาคาร</option>
                 <option value="ธนาคารกรุงเทพ">ธนาคารกรุงเทพ</option>
                 <option value="ธนาคารทหารไทย">ธนาคารทหารไทย</option>
                 <option value="ธนาคารกรุงศรีอยุธยา">ธนาคารกรุงศรีอยุธยา</option>
                 <option value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
                 <option value="ธนาคารไทยพาณิชย์">ธนาคารไทยพาณิชย์</option>
                 <option value="ธนาคารธนชาต">ธนาคารธนชาต</option>
                 <option value="ธนาคารยูโอบี">ธนาคารยูโอบี</option>
                 <option value="ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)">ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)</option>
                 <option value="ธนาคารกรุงไทย">ธนาคารกรุงไทย</option>
                 <option value="ธนาคารออมสิน">ธนาคารออมสิน</option>
                 <option value="ธนาคารอาคารสงเคราะห์">ธนาคารอาคารสงเคราะห์</option>
                 <option value="ธนาคารซีไอเอ็มบีไทย">ธนาคารซีไอเอ็มบีไทย</option>
                 <option value="ธนาคารทิสโก้">ธนาคารทิสโก้</option>
                 <option value="ธนาคารไทยเครดิตเพื่อรายย่อย">ธนาคารไทยเครดิตเพื่อรายย่อย</option>
                 <option value="ธนาคารแลนด์">ธนาคารแลนด์ แอนด์ เฮาส์</option>
                 <option value="ธนาคารเกียรตินาคิน">ธนาคารเกียรตินาคิน</option>
                 <option value="ธนาคารอิสลามแห่งประเทศไทย">ธนาคารอิสลามแห่งประเทศไทย</option>
                 <option value="ธนาคารไอซีบีซี (ไทย)">ธนาคารไอซีบีซี (ไทย)</option>
                 <option value="ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร">ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร</option>
                 <option value="ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย">ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย</option>
                 <option value="ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย">ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย</option>
               </select>
               <?= form_error('shop_bank_name','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ประเภทบัญชี : </label>
             <div class="col-sm-10">
               <select class="form-control" name="shop_bank_type" required>
                 <option value="" selected="selected">เลือกประเภทบัญชี</option>
                 <option value="1">ออมทรัพย์</option>
                 <option value="0">กระแสรายวัน</option>
               </select>
               <?= form_error('shop_bank_type','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">สาขา : </label>
             <div class="col-sm-10">
               <input type="text" class="form-control" name="shop_bank_sub" value="<?= set_value('shop_bank_sub') ?>" required>
               <?= form_error('shop_bank_sub','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ชื่อบัญชี : </label>
             <div class="col-sm-10">
               <input type="text" class="form-control" name="shop_bank_name_account" value="<?= set_value('shop_bank_name_account') ?>" required>
               <?= form_error('shop_bank_name_account','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">เลขที่บัญชี : </label>
             <div class="col-sm-10">
               <input type="text" class="form-control" name="shop_bank_number" value="<?= set_value('shop_bank_number') ?>" minlength="10" maxlength="15" required>
               <?= form_error('shop_bank_number','<small class="text-danger">','</small>') ?>
             </div>
           </div>

           <h2 class="text-center">รูปพระเครื่องเพื่อพิจารณา</h2>
           <h3 class="text-center">(รูปด้านหน้า/หลัง และ รูปด้านข้าง/ก้้น)</h3>
           <!-- พระองค์ที่ 1 -->
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">พระองค์ที่ 1 : </label>
             <div class="col-sm-10">
               <div class="row">
                 <div class="col">
                   <label for="" class="">รูปด้านหน้า/หลัง</label>
                   <input type="file" class="form-control-file" name="image_register_front1" required accept="image/jpeg">
                   <?= form_error('image_register_front1','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 3.5mb</small>
                 </div>
                 <div class="col">
                   <label for="" class="">รูปด้านข้าง/ก้น</label>
                   <input type="file" class="form-control-file" name="image_register_back1" required accept="image/jpeg">
                   <?= form_error('image_register_back1','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 3.5mb</small>
                 </div>
               </div>
             </div>
           </div>
           <div class="form-group row">
             <div class="col-sm-10 ml-auto">
               <textarea name="image_register_detail1" class="form-control" rows="4" cols="80" placeholder="รายละเอียด" required><?= set_value('image_register_detail1') ?></textarea>
               <?= form_error('image_register_detail1','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <!-- 1 -->

           <!-- พระองค์ที่ 2 -->
<!--            <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">พระองค์ที่ 2 : </label>
             <div class="col-sm-10">
               <div class="row">
                 <div class="col">
                   <label for="" class="">รูปด้านหน้า/หลัง</label>
                   <input type="file" class="form-control-file" name="image_register_front2" required accept="image/jpeg">
                   <?= form_error('image_register_front2','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
                 <div class="col">
                   <label for="" class="">รูปด้านข้าง/ก้น</label>
                   <input type="file" class="form-control-file" name="image_register_back2" required accept="image/jpeg">
                   <?= form_error('image_register_back2','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
               </div>
             </div>
           </div>
           <div class="form-group row">
             <div class="col-sm-10 ml-auto">
               <textarea name="image_register_detail2" class="form-control" rows="4" cols="80" placeholder="รายละเอียด" required><?= set_value('image_register_detail2') ?></textarea>
               <?= form_error('image_register_detail2','<small class="text-danger">','</small>') ?>
             </div>
           </div> -->
           <!-- 2 -->

           <!-- พระองค์ที่ 3 -->
<!--            <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">พระองค์ที่ 3 : </label>
             <div class="col-sm-10">
               <div class="row">
                 <div class="col">
                   <label for="" class="">รูปด้านหน้า/หลัง</label>
                   <input type="file" class="form-control-file" name="image_register_front3" required accept="image/jpeg">
                   <?= form_error('image_register_front3','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
                 <div class="col">
                   <label for="" class="">รูปด้านข้าง/ก้น</label>
                   <input type="file" class="form-control-file" name="image_register_back3" required accept="image/jpeg">
                   <?= form_error('image_register_back3','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
               </div>
             </div>
           </div>
           <div class="form-group row">
             <div class="col-sm-10 ml-auto">
               <textarea name="image_register_detail3" class="form-control" rows="4" cols="80" placeholder="รายละเอียด" required><?= set_value('image_register_detail3') ?></textarea>
               <?= form_error('image_register_detail3','<small class="text-danger">','</small><br>') ?>
             </div>
           </div> -->
           <!-- 3 -->

           <!-- พระองค์ที่ 4 -->
<!--            <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">พระองค์ที่ 4 : </label>
             <div class="col-sm-10">
               <div class="row">
                 <div class="col">
                   <label for="" class="">รูปด้านหน้า/หลัง</label>
                   <input type="file" class="form-control-file" name="image_register_front4" required accept="image/jpeg">
                   <?= form_error('image_register_front4','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
                 <div class="col">
                   <label for="" class="">รูปด้านข้าง/ก้น</label>
                   <input type="file" class="form-control-file" name="image_register_back4" required accept="image/jpeg">
                   <?= form_error('image_register_back4','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
               </div>
             </div>
           </div>
           <div class="form-group row">
             <div class="col-sm-10 ml-auto">
               <textarea name="image_register_detail4" class="form-control" rows="4" cols="80" placeholder="รายละเอียด" required><?= set_value('image_register_detail4') ?></textarea>
               <?= form_error('image_register_detail4','<small class="text-danger">','</small>') ?>
             </div>
           </div> -->
           <!-- 4 -->

           <!-- พระองค์ที่ 5 -->
<!--            <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">พระองค์ที่ 5 : </label>
             <div class="col-sm-10">
               <div class="row">
                 <div class="col">
                   <label for="" class="">รูปด้านหน้า/หลัง</label>
                   <input type="file" class="form-control-file" name="image_register_front5" required accept="image/jpeg">
                   <?= form_error('image_register_front5','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
                 <div class="col">
                   <label for="" class="">รูปด้านข้าง/ก้น</label>
                   <input type="file" class="form-control-file" name="image_register_back5" required accept="image/jpeg">
                   <?= form_error('image_register_back5','<small class="text-danger">','</small><br>') ?>
                   <small>ไฟล์ JPEGs ขนาดไม่เกิน 350Kb</small>
                 </div>
               </div>
             </div>
           </div>
           <div class="form-group row">
             <div class="col-sm-10 ml-auto">
               <textarea name="image_register_detail5" class="form-control" rows="4" cols="80" placeholder="รายละเอียด" required><?= set_value('image_register_detail5') ?></textarea>
               <?= form_error('image_register_detail5','<small class="text-danger">','</small>') ?>
             </div>
           </div> -->
           <!-- 5 -->

           <div class="container">
             <h3>เงื่อนไขและข้อตกลง</h3>
             <p>ผู้สมัครจะต้องปฏิบัติตามระเบียบการใช้งานอย่างเคร่งครัด บริษัทฯสามารถยกเลิกสมาชิกของผู้สมัครได้ทันทีหากพบเห็นการฝ่าฝืนระเบียบการใช้งาน และในกรณที่เกิดการร้องเรียนจากลูกค้าหรือผู้ที่เกี่ยวข้อง  หากตรวจพบว่าผู้สมัครมีพฤติกรรมที่ไม่เหมาะสมหรือส่อไปในทางทุจริตไม่ว่าด้วยกรณีใด บริษัทฯขอสงวนสิทธิที่จะยกเลิกสมาชิกของผู้สมัครทันทีโดยไม่คืนเงินค่าบริการ ทั้งนี้ผู้สมัครต้องพึงปฏิบัติตามระเบียบต่อไปนี้ด้วย</p>
             <ol>
               <li>ผู้สมัครต้องชำระค่าสมาชิกตามที่กำหนด (หากเลยจากที่กำหนด บริษัทฯจะนับ "วันที่ต่ออายุ" ต่อเนื่องจาก "วันที่หมดอายุ")</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องมีอยู่จริงและตรงตามรูปที่นำเสนอ</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องเป็นของแท้ตรงตามมาตรฐานสากล</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องเป็นรายการของผู้สมัครและอยู่ในความรับผิดชอบของผู้สมัคร ห้ามรับฝากลงประกาศโดยเด็ดขาด</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องระบุสภาพให้ตรงตามจริง หากมีการอุด ซ่อม แต่ง ผู้สมัครจะต้องแจ้งให้ลูกค้าหรือผู้ที่เกี่ยวข้องทราบก่อนทำการตกลง ซื้อ-ขาย</li>
               <li>ผู้สมัครต้องรับผิดชอบในกรณีเกิดปัญหา หากลูกค้าหรือผู้ที่เกี่ยวข้องเกิดความไม่พอใจในรายการที่ตกลง ซื้อ-ขาย จะต้องไกล่เกลี่ย ตกลงด้วยเหตุผลอันสมควร</li>
               <li>การนำเสนอข้อมูลทั้งหมดของผู้สมัครต้องอยู่ภายใต้ พ.ร.บ. คอมพิวเตอร์ 2560</li>
             </ol>
           </div>

           <div class="form-group row">
             <div class="mx-auto">
               <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="customCheck2" name="checkbox2" required>
                  <label class="custom-control-label" for="customCheck2">ข้าพเจ้าได้อ่านและยอมรับในเงื่อนไขข้างต้นทุกประการ</label>
                </div>
                <?= form_error('checkbox2','<small class="text-danger">','</small>') ?>
             </div>
           </div>

					 <div class="form-group row">
						 <div class="col-sm-12">
							 <button type="submit" class="btn btn-primary btn-block">สมัครสมาชิก</button>
						 </div>
					 </div>
				 </form>
			 </div>
		 </div>
   </div>
 </div>
</div>
<?= $this->session->msg ?>
