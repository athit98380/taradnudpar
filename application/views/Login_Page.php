<div class="container mt-2">
 <div class="row">
   <div class="col-md-8 mx-auto">
		 <div class="card">
			 <div class="card-header">
				 Login
			 </div>
			 <div class="card-body">
         <?php echo $this->session->msg; //Show Alert Error Login ?>
				 <form action="<?= base_url('main/login') ?>" method="post">
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Email : </label>
						 <div class="col-sm-10">
							 <input type="email" name="email" class="form-control" value="<?= set_value('email') ?>">
               <?= form_error('email','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Password : </label>
						 <div class="col-sm-10">
							 <input type="password" name="password" class="form-control" value="<?= set_value('password') ?>">
               <?= form_error('password','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2"></label>
						 <div class="col-sm-10">
							 <button type="submit" class="btn btn-primary btn-block">เข้าสู่ระบบ</button>
						 </div>
					 </div>
				 </form>
    <br>
    <p>ยังไม่ได้ลงทะเบียน? <a href="<?php echo site_url();?>register">
สมัครสมาชิก</a></p>
    <p>ลืมรหัสผ่านหรือไม่? <a href="<?php echo site_url();?>main/forgot">ลืมรหัสผ่าน</a></p> 
			 </div>
		 </div>
   </div>
 </div>
</div>
