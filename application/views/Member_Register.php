<div class="container mt-2">
 <div class="row">
   <div class="col-md-12 mx-auto">
		 <div class="card">
			 <div class="card-header">
				 <?php if($get_shop=="shop"){?>
          สมัครสมาชิกเพื่อเปิดร้านค้า
         <?php }else{?>
          สมัครสมาชิก
        <?php }?>
			 </div>
			 <div class="card-body">
        <?php if($get_shop=="shop"){?>
          <div class="row">
            <div class="col-md-6 text-center">
              <span class="dot-circle" style="background-color: #d1a500;">1</span> ขั้นตอนที่ 1 สมัครสมาชิก
            </div>
            <div class="col-md-6 text-center">
              <span class="dot-circle" style="background-color: #ddd;">2</span> ขั้นตอนที่ 2 เปิดร้านค้า
            </div>
          </div>
          <div class="margin-bottom-60"></div>
        <?php }?>
         <?php echo $this->session->msg; //Show Alert Error Login ?>
				 <form action="<?= base_url('register/member') ?><?php if($get_shop=="shop"){?>/shop<?php }?>" method="post" enctype="multipart/form-data">
           <h2 class="text-center">
         <?php if($get_shop=="shop"){?>
          แบบฟอร์มสมัครสมาชิกเพื่อเปิดร้านค้า
         <?php }else{?>
          แบบฟอร์มสมัครสมาชิก
        <?php }?>
           </h2>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Email : </label>
						 <div class="col-sm-10">
							 <input type="email" name="email" class="form-control" value="<?= set_value('email') ?>" required>
               <?= form_error('email','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Password : </label>
						 <div class="col-sm-10">
							 <input type="password" name="password" class="form-control" minlength="8" required>
               <?= form_error('password','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
					 <div class="form-group row">
						 <label for="" class="col-form-label col-sm-2">Confirm Password : </label>
						 <div class="col-sm-10">
							 <input type="password" name="ConfirmPassword" class="form-control" minlength="8" required>
               <?= form_error('ConfirmPassword','<small class="text-danger">','</small>') ?>
						 </div>
					 </div>
           <h2 class="text-center">ข้อมูลประวัติส่วนตัว</h2>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ชื่อ : </label>
             <div class="col-sm-10">
               <input type="text" name="fname" class="form-control" value="<?= set_value('fname') ?>" required>
               <?= form_error('fname','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">นามสกุล : </label>
             <div class="col-sm-10">
               <input type="text" name="lname" class="form-control" value="<?= set_value('lname') ?>" required>
               <?= form_error('lname','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">เบอร์โทรศัพท์ : </label>
             <div class="col-sm-10">
               <input type="text" name="tel" class="form-control" value="<?= set_value('tel') ?>" minlength="10" maxlength="10" required>
               <?= form_error('tel','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">เลขที่บัตรประชาชน : </label>
             <div class="col-sm-10">
               <input type="text" name="numberid" class="form-control" value="<?= set_value('numberid') ?>" minlength="13" maxlength="13" required>
               <?= form_error('numberid','<small class="text-danger">','</small>') ?>
             </div>
           </div>
           <div class="form-group row">
             <label for="" class="col-form-label col-sm-2">ไฟล์รูปบัตรประชาชน : </label>
             <div class="col-sm-10">
               <input type="file" name="imageid" class="form-control-file" required accept="image/jpeg">
               <?= form_error('imageid','<small class="text-danger">','</small>') ?>
             </div>
           </div>

           <div class="container">
             <h3>เงื่อนไขและข้อตกลง</h3>
             <p>ผู้สมัครจะต้องปฏิบัติตามระเบียบการใช้งานอย่างเคร่งครัด บริษัทฯสามารถยกเลิกสมาชิกของผู้สมัครได้ทันทีหากพบเห็นการฝ่าฝืนระเบียบการใช้งาน และในกรณที่เกิดการร้องเรียนจากลูกค้าหรือผู้ที่เกี่ยวข้อง  หากตรวจพบว่าผู้สมัครมีพฤติกรรมที่ไม่เหมาะสมหรือส่อไปในทางทุจริตไม่ว่าด้วยกรณีใด บริษัทฯขอสงวนสิทธิที่จะยกเลิกสมาชิกของผู้สมัครทันทีโดยไม่คืนเงินค่าบริการ ทั้งนี้ผู้สมัครต้องพึงปฏิบัติตามระเบียบต่อไปนี้ด้วย</p>
             <ol>
               <li>ผู้สมัครต้องชำระค่าสมาชิกตามที่กำหนด (หากเลยจากที่กำหนด บริษัทฯจะนับ "วันที่ต่ออายุ" ต่อเนื่องจาก "วันที่หมดอายุ")</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องมีอยู่จริงและตรงตามรูปที่นำเสนอ</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องเป็นของแท้ตรงตามมาตรฐานสากล</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องเป็นรายการของผู้สมัครและอยู่ในความรับผิดชอบของผู้สมัคร ห้ามรับฝากลงประกาศโดยเด็ดขาด</li>
               <li>ทุกรายการที่ผู้สมัครลงประกาศ ต้องระบุสภาพให้ตรงตามจริง หากมีการอุด ซ่อม แต่ง ผู้สมัครจะต้องแจ้งให้ลูกค้าหรือผู้ที่เกี่ยวข้องทราบก่อนทำการตกลง ซื้อ-ขาย</li>
               <li>ผู้สมัครต้องรับผิดชอบในกรณีเกิดปัญหา หากลูกค้าหรือผู้ที่เกี่ยวข้องเกิดความไม่พอใจในรายการที่ตกลง ซื้อ-ขาย จะต้องไกล่เกลี่ย ตกลงด้วยเหตุผลอันสมควร</li>
               <li>การนำเสนอข้อมูลทั้งหมดของผู้สมัครต้องอยู่ภายใต้ พ.ร.บ. คอมพิวเตอร์ 2560</li>
             </ol>
           </div>

           <div class="form-group row">
             <div class="mx-auto">
               <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="customCheck2" name="checkbox2" required>
                  <label class="custom-control-label" for="customCheck2">ข้าพเจ้าได้อ่านและยอมรับในเงื่อนไขข้างต้นทุกประการ</label>
                </div>
                <?= form_error('checkbox2','<small class="text-danger">','</small>') ?>
             </div>
           </div>

					 <div class="form-group row">
						 <div class="col-sm-12">
							 <button type="submit" class="btn btn-primary btn-block">
         <?php if($get_shop=="shop"){?>
          สมัครสมาชิกเพื่อเปิดร้านค้า
         <?php }else{?>
          สมัครสมาชิก
        <?php }?>
               </button>
						 </div>
					 </div>
				 </form>
			 </div>
		 </div>
   </div>
 </div>
</div>
<?= $this->session->msg ?>
