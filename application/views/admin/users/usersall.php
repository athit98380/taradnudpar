<div class="container mt-2">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><?= $title ?></div>
                <div class="card-body">
                    <table class="table table-striped table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <th>อีเมล์</th>
                                <th>ชื่อ-สกุล</th>
                                <th>สมัครเมื่อ</th>
                                <th>สถานะ</th>
                                <th>การทำงาน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($users as $row) : ?>
                            <tr>
                                <td><?= $row->email ?></td>
                                <td><?= $row->first_name.' '.$row->last_name ?></td>
                                <td><?= $this->datethai->DateLong($row->created_on) ?></td>
                                <td><?= ($row->admin_active == 0) ? '<font color="red">รอดำเนินการ</font>' : '<font color="green">ยืนยันแล้ว</font>'; ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            เลือก
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="<?= base_url("admin/userslist/view_member/{$row->id}") ?>">ดูข้อมูลสมาชิก</a>
                                            <!-- <a class="dropdown-item" href="<?= base_url("admin/userslist/edit_member/{$row->id}") ?>">แก้ไขข้อมูลสมาขิก</a>
                                            <a class="dropdown-item" href="<?= base_url("admin/userslist/photo/{$row->id}") ?>" target="_blank">ดูรูปพระเครื่อง</a> -->
                                            <a class="dropdown-item" href="<?= base_url("admin/userslist/confirm_member/{$row->id}") ?>">ยืนยัน</a>
                                            <a class="dropdown-item" href="<?= base_url("admin/userslist/unconfirm/{$row->id}") ?>" id="cancle" data-uid="<?= $row->id ?>">ยกเลิก</a>
                                            <a class="dropdown-item" href="<?= base_url("admin/userslist/remove/{$row->id}") ?>" id="cancle" data-uid="<?= $row->id ?>">ลบ</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ปิดการแสดงผลร้าน</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url("admin/userslist/unconfirm") ?>" method="post">
            <input type="hidden" name="uid" id="uid" value="" required>
            <div class="form-group">
                <label for="">ข้อความ</label>
                <textarea name="comment" id="" cols="30" rows="10" class="form-control" required></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-block">ทำรายการ</button>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script>
    $('#myTable').DataTable();
    $('#cancle').click(function(){
        $('#uid').val($(this).data('uid'));
    });
</script>
<?= $this->session->msg ?>