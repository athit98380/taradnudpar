<div class="container mt-2">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><?= $title ?></div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 1</h1>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front1}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back1}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail1 ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 2</h1>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front2}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back2}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail2 ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 3</h1>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front3}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back3}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail3 ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 4</h1>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front4}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back4}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail4 ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h1 class="text-center">พระองค์ที่ 5</h1>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front5}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านหน้า/หลัง</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="justify-content-center">
                                <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back5}") ?>" alt="" srcset="">
                            </div>
                            <p class="text-center">รูปด้านข้าง/ก้น</p>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <p class="text-center"><?= $photo->image_register_detail5 ?></p>
                        </div>
                    </div>
                    <hr>
                    <a class="btn btn-warning btn-block" href="<?= base_url('admin/userslist') ?>">ย้อนกลับ</a><br/>
                </div>
            </div>
        </div>
    </div>
</div>