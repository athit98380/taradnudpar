    <div class="container">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <?= $title ?>
          </div>
          <div class="card-body row">
            <div class="col-md-4 text-center">
              <?php if (!empty($users->user_image)) {?>
                <img src="<?= base_url('assets/uploads/numberid/'.$users->id.'/'.$users->user_image_profile) ?>" class="img-fluid rounded" alt="...">
              <?php }else{ ?>
                <img src="<?= base_url('assets/images/nobody_m.original.jpg') ?>" class="img-fluid rounded" alt="">
              <?php } ?>
              <form id="image_profile" action="<?= base_url("admin/userslist/change_image_profile/".$users->id) ?>"  method="post" enctype="multipart/form-data" hidden>
                  <input type="file" name="pimage" class="form-control-file" required>
                  <button class="btn btn-warning btn-block" type="submit">Upload</button>
              </form>
              <a id="link_upload_profile" onclick="upload_profile()" href="#">เปลี่ยนภาพโปรไฟล์</a>
                    <script type="text/javascript">
                      function upload_profile()
                      {
                        $("#image_profile").removeAttr('hidden');
                        $("#link_upload_profile").attr('hidden','hidden');
                      }
                    </script>
            </div>
            <div class="col-md-8">
              <div class="container">
                <h2><?= $users->first_name." ".$users->last_name ?></h2>
                <p>สิทธิ์ <b><?= $users->user_type ?></b> </p>
                <p>อีเมล์ห <?= $users->email ?></p>
                <p>เบอร์โทร <?= $users->user_tel ?> </p>
                <p>รหัสบัตรประชาชน <?= $users->user_number_id ?></p>
                <?php if (!empty($users->user_tel_add)) {?>
                <p> <?= $users->user_tel_add ?> </p>
                <?php } ?>
                <?php if ($users->admin_active==0){echo '<b style="color:red;">รอยืนยันสมัครสมาชิก</b><br/>';}else{echo '<span style="color:green;"><i class="fas fa-check-circle"></i> <b>ผ่านการยืนยันสมาชิก</b></span><br/>';}?>
                <?php if (!empty($shop->shop_id)) {
                 if ($shop->shop_status==0){
                  echo '<span style="color:green;"><i class="fas fa-check-circle"></i> <b>ร้านค้าผ่านการตรวจสอบ</b>';
                }elseif($shop->shop_status==1){
                  echo '<b style="color:red;">ร้านค้ายังไม่ผ่านการตรวจสอบ</b></span>';
                }else{
                  echo '<b style="color:red;">ร้านค้าถูกลบ</b></span>';
                }
                 echo "<br/>";
                 if ($shop->shop_recommend==1){echo '<span style="color:green;"><i class="fas fa-check-circle"></i> <b>ร้านค้าเป็นร้านแนะนำ</b>';}else{echo '<b style="color:red;">ร้านค้ายังไม่เป็นร้านแนะนำ</b></span>';}
                }?><br/>
                <?php if ($users->admin_active==0){?>
                <a class="btn btn-success" href="<?= base_url("admin/userslist/confirm_member/{$users->id}/comeback") ?>">ยืนยันสมาชิก</a>
                <?php }else{?>
                <a class="btn btn-warning" href="<?= base_url("admin/userslist/unconfirm_member/{$users->id}/comeback") ?>" id="cancle" data-uid="<?= $users->id ?>">ยกเลิกสมาชิก</a>
                <?php }?>
              </div>
              <hr>
    <?php if (!empty($shop->shop_id)) {?>
              <ul class="container">
                <li>
                  <p>สมัครเมื่อ <?= $this->datethai->DateLong($shop->shop_create_on) ?></p>
                </li>
                <li>
                  <p>วันหมดอายุ <?= $this->datethai->DateLong($shop->shop_expired_on) ?>
                    <?php
                    if (date('Y-m-d H:i:s') < date('Y-m-d H:i:s',strtotime("{$shop->shop_expired_on}"))) {
                      //echo '<strong>ใกล้หมดอายุ</strong>';
                    }elseif (date('Y-m-d H:i:s') <= date('Y-m-d H:i:s',strtotime("{$shop->shop_expired_on} + 3 month"))) {
                      echo '<strong>สมาชิกหมดอายุ ใน 3 เดือน</strong>';
                    }elseif (date('Y-m-d H:i:s') <= date('Y-m-d H:i:s',strtotime("{$shop->shop_expired_on} + 6 month"))) {
                      echo '<strong>สมาชิกหมดอายุ ใน 6 เดือน</strong>';
                    }
                    ?>
                  </p>
                 </li>
                 <li> <a href="<?= base_url("main/shop/{$shop->shop_id}") ?>" target="_blank">ดูหน้าร้าน</a> </li>
              </ul>
                <?php if ($shop->shop_status==0){?>
                  <a class="btn btn-warning" href="<?= base_url("admin/userslist/unconfirm_shop/{$users->id}/comeback") ?>">ยกเลิกร้านค้า</a> 
                <?php }else{?>
                  <a class="btn btn-success" href="<?= base_url("admin/userslist/confirm_shop/{$users->id}/comeback") ?>">ยืนยันร้านค้า</a>
                <?php }?>
                <?php if ($shop->shop_recommend==0){?>
                  <a class="btn btn-success" href="<?= base_url("admin/shop/recommend/{$shop->shop_id}/{$users->id}") ?>">ตั้งเป็นร้านแนะนำ</a>
                <?php }else{?>
                  <a class="btn btn-danger" href="<?= base_url("admin/shop/unrecommend/{$shop->shop_id}/{$users->id}") ?>">ยกเลิกร้านแนะนำ</a>
                <?php }?>
                <a class="btn btn-info" target="_blank" href="<?= base_url("admin/shop/product/{$shop->shop_id}") ?>">จัดการสินค้า</a>
    <?php }?>
                <a class="btn btn-danger" id="del" data-uid="<?= $users->id ?>">ลบข้อมูลทั้งหมด</a>

  <script type="text/javascript">
  $('#del').click(function(){
    swal({
      title: "คุณต้องการลบข้อมูลนี้หรือไม่?",
      text: "ข้อมูลนี้จะหายไปถาวร!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = "<?= base_url("admin/userslist/remove/{$users->id}") ?>";
      }
    });
  });
</script>
            </div>
            <div class="col-md-12">
              <hr>
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">ข้อมูลส่วนตัว</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="photo_profile-tab" data-toggle="tab" href="#photo_profile" role="tab" aria-controls="photo_profile" aria-selected="false">รูปยืนยันสมาชิก</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pass-tab" data-toggle="tab" href="#pass" role="tab" aria-controls="pass" aria-selected="false">รหัสผ่าน</a>
                </li>
                <?php if (!empty($shop->shop_id)) {?>
                <li class="nav-item">
                  <a class="nav-link" id="shop-tab" data-toggle="tab" href="#shop" role="tab" aria-controls="shop" aria-selected="false">ร้านค้า</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="blog-tab" data-toggle="tab" href="#blog" role="tab" aria-controls="blog" aria-selected="false">ประกาศร้านค้า</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="shop-img-tab" data-toggle="tab" href="#shopimg" role="tab" aria-controls="shop" aria-selected="false">จัดการภาพร้านค้า</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="bank-tab" data-toggle="tab" href="#bank" role="tab" aria-controls="shop" aria-selected="false">บัญชีธนาคาร</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="photo-tab" data-toggle="tab" href="#photo" role="tab" aria-controls="shop" aria-selected="false">รูปยืนยันร้านค้า</a>
                </li>
                <?php }?>
              </ul>
              <div class="container">
                <div class="tab-content mt-2" id="myTabContent">
                  <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="form-group row">
                      <label for="" class="col-form-label col-sm-3">IP Adress</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?= $users->ip_address ?>" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="" class="col-form-label col-sm-3">เข้าสู่ระบบล่าสุด</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?= $this->datethai->DateLong($users->last_login) ?>" readonly>
                      </div>
                    </div>
                    <div id="edit_member">
                      <form action="<?= base_url('admin/userslist/edit_member/'.$users->id) ?>" method="post">
                        <div class="form-group row">
                          <label for="" class="col-form-label col-sm-3">เบอร์โทรศัพท์ : </label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="user_tel" name="user_tel" minlength="9" maxlength="10" value="<?= $users->user_tel ?>" required readonly>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="" class="col-form-label col-sm-3">เบอร์โทรศัพท์<br/>เพิ่มเติม: </label>
                          <div class="col-sm-9">
                            <textarea class="form-control" id="user_tel_add" name="user_tel_add" minlength="9" maxlength="40" readonly><?php if (!empty($users->user_tel_add)) {?><?= $users->user_tel_add ?><?php } ?></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-12">
                            <button type="button" class="btn btn-warning btn-block" onclick="edit_member()" id="btnMemberEdit">แก้ไข</button>
                            <button type="submit" class="btn btn-warning btn-block" id="btnMemberEditHidden" hidden>แก้ไข</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                    <script type="text/javascript">
                      function edit_member()
                      {
                        $("#user_tel").removeAttr('readonly');
                        $("#user_tel_add").removeAttr('readonly');
                        $("#btnMemberEditHidden").removeAttr('hidden');
                        $("#btnMemberEdit").attr('hidden','hidden');
                      }
                    </script>
                  <div class="tab-pane fade" id="pass" role="tabpanel" aria-labelledby="photo_profile">
                      <div class="form-group row">
                        <div class="col-sm-12 text-center">
                          <?php $path=base_url("admin/userslist/reset_password/".$users->id);?>
                          <?php echo form_open_multipart($path);?>
                          <div class="form-group">
                            <?php echo form_password(array('name'=>'password', 'id'=> 'password', 'placeholder'=>'Password', 'class'=>'form-control', 'value' => set_value('password'))); ?>
                            <?php echo form_error('password') ?>
                          </div>
                          <div class="form-group">
                            <?php echo form_password(array('name'=>'passconf', 'id'=> 'passconf', 'placeholder'=>'Confirm Password', 'class'=>'form-control', 'value'=> set_value('passconf'))); ?>
                            <?php echo form_error('passconf') ?>
                          </div>
                          <?php echo form_submit(array('value'=>'Reset Password', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
                          <?php echo form_close(); ?>
                        </div>
                      </div>
                  </div>
                  <?php if (!empty($shop->shop_id)) {?>
                  <div class="tab-pane fade" id="shopimg" role="tabpanel" aria-labelledby="shopimg-tab">
                      <div class="form-group row">
                        <div class="col-sm-12 text-center">
                          <?php if (!empty($shop->shop_banner)) {?>
                            <h4>ภาพแบนเนอร์ร้านค้า</h4>
                            <img src="<?= base_url('assets/uploads/numberid/'.$users->id.'/'.$shop->shop_banner) ?>" class="img-fluid rounded" alt="..."><br/>
                          <?php }else{ ?>
                            <strong>ไม่มี Banner ร้านค้า</strong>
                          <?php } ?>
                          <?php $path=base_url("admin/userslist/change_shop_banner/".$shop->users_id."/".$shop->shop_id);?>
                          <?php echo form_open_multipart($path, 'id="shop_banner" hidden');?>
                            ขนาดภาพที่เหมาะสมความกว้าง 1920px ความสูง 600px <strong>ขนาดไฟล์ไม่เกิน 15 Mb</strong>
                              <input type="file" name="shop_banner" class="form-control-file" required>
                              <button class="btn btn-warning btn-block" type="submit">Upload</button>
                          </form>
                          <span class="btn btn-primary" id="link_upload_banner" onclick="shop_banner()" href="#">เปลี่ยน Banner ร้านค้า</span>
                                <script type="text/javascript">
                                  function shop_banner()
                                  {
                                    $("#shop_banner").removeAttr('hidden');
                                    $("#link_upload_banner").attr('hidden','hidden');
                                  }
                                </script>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-12 text-center">
                          <?php if (!empty($shop->shop_qrcode)) {?>
                            <h4>ภาพ QRcode ร้านค้า</h4>
                            <?php if (!empty($shop->shop_qrcode_link)) {?><a href="<?php echo $shop->shop_qrcode_link;?>" target="blank"><?php }?>
                            <img src="<?= base_url('assets/uploads/numberid/'.$users->id.'/'.$shop->shop_qrcode) ?>" class="img-fluid" style="max-width:300px!important;" alt="...">
                            <?php if (!empty($shop->shop_qrcode_link)) {?></a><?php }?>
                            <br/>
                          <?php }else{ ?>
                            <strong>ไม่มี QRcode ร้านค้า</strong>
                          <?php } ?>
                          <?php $path=base_url("admin/userslist/change_shop_banner/".$shop->users_id."/".$shop->shop_id);?>
                          <?php echo form_open_multipart($path, 'id="shop_qrcode" hidden');?>
                            ขนาดภาพที่เหมาะสมความกว้าง 300px ความสูง 300px <strong>ขนาดไฟล์ไม่เกิน 5 Mb</strong>
                            <input type="file" name="shop_qrcode" class="form-control-file"><br/>
                            <input type="hidden" name="shop_qrcode_old" value="<?= $shop->shop_qrcode ?>">
                            ลิงค์ Line ตัวอย่างเช่น http://line.me/ti/p/@taladnutpar
                              <input type="text" name="shop_qrcode_link" class="form-control" placeholder="http://line.me/ti/p/@taladnutpar" value="<?php if(!empty($shop->shop_qrcode_link)) { echo $shop->shop_qrcode_link;}?>">
                              <button class="btn btn-warning btn-block" type="submit">Upload</button>
                          </form>
                          <span class="btn btn-primary" id="link_upload_qrcode" onclick="shop_qrcode()" href="#">เปลี่ยน QRcode ร้านค้า</span>
                                <script type="text/javascript">
                                  function shop_qrcode()
                                  {
                                    $("#shop_qrcode").removeAttr('hidden');
                                    $("#link_upload_qrcode").attr('hidden','hidden');
                                  }
                                </script>
                        </div>
                      </div>
                  </div>
                  <div class="tab-pane fade" id="shop" role="tabpanel" aria-labelledby="shop-tab">
                    <!-- Form Edit Profile -->
                    <form action="<?= base_url('admin/userslist/edit_profile/'.$users->id."/".$shop->shop_id) ?>" method="post">
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">รหัสร้านค้า : </label>
                        <div class="col-sm-9">
                          <input type="text" name="shopid" id="shopid" class="form-control" value="<?= $shop->shop_id ?>" readonly required>
                          <?= form_error('shopid','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">ชื่อร้านค้า : </label>
                        <div class="col-sm-9">
                          <input type="text" name="shopname" id="shopname" class="form-control" value="<?= $shop->shop_name ?>" readonly required>
                          <?= form_error('shopname','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">ที่ตั้งร้านค้า : </label>
                        <div class="col-sm-9">
                          <textarea name="shop_address" id="shop_address" class="form-control" rows="4" cols="80" readonly required><?= $shop->shop_address ?></textarea>
                          <?= form_error('shop_address','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">รายละเอียดร้านค้า : </label>
                        <div class="col-sm-9">
                          <textarea name="shopdetail" id="shopdetail" class="form-control" rows="4" cols="80" readonly required><?= $shop->shop_detail ?></textarea>
                          <?= form_error('shopdetail','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">เงื่อนไขการรับประกัน : </label>
                        <div class="col-sm-9">
                          <textarea name="shop_guarantee" id="shop_guarantee" class="form-control" rows="4" cols="80" readonly required><?= $shop->shop_guarantee ?></textarea>
                          <?= form_error('shop_guarantee','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">ผู้รับรองการเปิดร้าน : </label>
                        <div class="col-sm-9">
                          <input type="text" name="shop_guarantee_by" id="shop_guarantee_by" class="form-control" value="<?= $shop->shop_guarantee_by ?>" readonly required>
                          <?= form_error('shop_guarantee_by','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">โทรศัพท์ผู้รับรอง : </label>
                        <div class="col-sm-9">
                          <input type="text" name="shop_guarantee_by_tel" id="shop_guarantee_by_tel" class="form-control" value="<?= $shop->shop_guarantee_by_tel ?>" minlength="10" maxlength="10" readonly required>
                          <?= form_error('shop_guarantee_by_tel','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-12">
                          <button type="button" class="btn btn-warning btn-block" onclick="edit()" id="btnEdit">แก้ไข</button>
                          <button type="submit" class="btn btn-warning btn-block" id="btnEditHidden" hidden>แก้ไข</button>
                        </div>
                      </div>
                    </form>
                    <!-- End Form Edit Profile -->
                    <script type="text/javascript">
                      function edit()
                      {
                        $("#shopname").removeAttr('readonly');
                        $("#shop_address").removeAttr('readonly');
                        $("#shopdetail").removeAttr('readonly');
                        $("#shop_guarantee").removeAttr('readonly');
                        $("#shop_guarantee_by").removeAttr('readonly');
                        $("#shop_guarantee_by_tel").removeAttr('readonly');
                        $("#btnEditHidden").removeAttr('hidden');
                        $("#btnEdit").attr('hidden','hidden');
                      }
                    </script>
                  </div>
                  <div class="tab-pane fade" id="blog" role="tabpanel" aria-labelledby="blog-tab">
                    <!-- Form Edit Profile -->
                    <form action="<?= base_url('admin/userslist/edit_blog/'.$users->id."/".$shop->shop_id) ?>" method="post">
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">ประกาศร้านค้า : </label>
                        <div class="col-sm-9">
                          <textarea name="shop_blog" id="shop_blog" class="form-control" readonly required><?= $shop->shop_blog ?></textarea>
                          <?= form_error('shopid','<small class="text-danger">','</small>') ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-12">
                          <button type="button" class="btn btn-warning btn-block" onclick="edit_shop_blog()" id="btnEditshop_blog">แก้ไข</button>
                          <button type="submit" class="btn btn-warning btn-block" id="btnEditHiddenshop_blog" hidden>แก้ไข</button>
                        </div>
                      </div>
                    </form>
                    <!-- End Form Edit Profile -->
                    <script type="text/javascript">
                      function edit_shop_blog()
                      {
                        $("#shop_blog").removeAttr('readonly');
                        $("#btnEditHiddenshop_blog").removeAttr('hidden');
                        $("#btnEditshop_blog").attr('hidden','hidden');
                      }
                    </script>
                  </div>
                   <?php }?>
                  <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">
                    <!-- Form Bank -->
                    <?php if (!empty($shop->shop_id)) {?>
                    <form action="<?= base_url("admin/userslist/edit_bank/".$users->id."/".$shop->shop_id) ?>" method="post">
                      <input type="hidden" name="shopid" id="shopid" class="form-control" value="<?= $shop->shop_id ?>" readonly required>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">ชื่อธนาคาร</label>
                        <div class="col-sm-9">
                          <select class="form-control" name="sbankname" id="sbanknameHidden" hidden required>
                            <option <?php if($shop->shop_bank_name==""){echo 'selected="selected"';}?>  >เลือกธนาคาร</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารกรุงเทพ"){echo 'selected="selected"';}?>  value="ธนาคารกรุงเทพ">ธนาคารกรุงเทพ</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารทหารไทย"){echo 'selected="selected"';}?>  value="ธนาคารทหารไทย">ธนาคารทหารไทย</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารกรุงศรีอยุธยา"){echo 'selected="selected"';}?>  value="ธนาคารกรุงศรีอยุธยา">ธนาคารกรุงศรีอยุธยา</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารกสิกรไทย"){echo 'selected="selected"';}?>  value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารไทยพาณิชย์"){echo 'selected="selected"';}?>  value="ธนาคารไทยพาณิชย์">ธนาคารไทยพาณิชย์</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารธนชาต"){echo 'selected="selected"';}?>  value="ธนาคารธนชาต">ธนาคารธนชาต</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารยูโอบี"){echo 'selected="selected"';}?>  value="ธนาคารยูโอบี">ธนาคารยูโอบี</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)"){echo 'selected="selected"';}?>  value="ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)">ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารกรุงไทย"){echo 'selected="selected"';}?>  value="ธนาคารกรุงไทย">ธนาคารกรุงไทย</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารออมสิน"){echo 'selected="selected"';}?>  value="ธนาคารออมสิน">ธนาคารออมสิน</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารอาคารสงเคราะห์"){echo 'selected="selected"';}?>  value="ธนาคารอาคารสงเคราะห์">ธนาคารอาคารสงเคราะห์</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารซีไอเอ็มบีไทย"){echo 'selected="selected"';}?>  value="ธนาคารซีไอเอ็มบีไทย">ธนาคารซีไอเอ็มบีไทย</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารทิสโก้"){echo 'selected="selected"';}?>  value="ธนาคารทิสโก้">ธนาคารทิสโก้</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารไทยเครดิตเพื่อรายย่อย"){echo 'selected="selected"';}?>  value="ธนาคารไทยเครดิตเพื่อรายย่อย">ธนาคารไทยเครดิตเพื่อรายย่อย</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารแลนด์"){echo 'selected="selected"';}?>  value="ธนาคารแลนด์">ธนาคารแลนด์ แอนด์ เฮาส์</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารเกียรตินาคิน"){echo 'selected="selected"';}?>  value="ธนาคารเกียรตินาคิน">ธนาคารเกียรตินาคิน</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารอิสลามแห่งประเทศไทย"){echo 'selected="selected"';}?>  value="ธนาคารอิสลามแห่งประเทศไทย">ธนาคารอิสลามแห่งประเทศไทย</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารไอซีบีซี (ไทย)"){echo 'selected="selected"';}?>  value="ธนาคารไอซีบีซี (ไทย)">ธนาคารไอซีบีซี (ไทย)</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร"){echo 'selected="selected"';}?>  value="ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร">ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย"){echo 'selected="selected"';}?>  value="ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย">ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย</option>
                            <option <?php if($shop->shop_bank_name=="ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย"){echo 'selected="selected"';}?>  value="ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย">ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย</option>
                          </select>
                          <?= form_error('sbankname','<small class="text-danger">','</small>') ?>
                          <input type="text" id="sbankname" class="form-control" value="<?= $shop->shop_bank_name ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">ประเภทบัญชี</label>
                        <div class="col-sm-9">
                          <select class="form-control" name="sbanktype" id="sbanktypeHidden" hidden required>
                              <option value="" <?php if($shop->shop_bank_type==""){echo 'selected="selected"';}?> >เลือกประเภทบัญชี</option>
                              <option value="1" <?php if($shop->shop_bank_type==1){echo 'selected="selected"';}?> >ออมทรัพย์</option>
                              <option value="0" <?php if($shop->shop_bank_type==0){echo 'selected="selected"';}?> >กระแสรายวัน</option>
                            </select>
                          <?= form_error('sbanktype','<small class="text-danger">','</small>') ?>
                          <input type="text" id="sbanktype" class="form-control" value="<?= $shop->shop_bank_type ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">สาขา</label>
                        <div class="col-sm-9">
                          <input type="text" name="sbanksub" id="sbanksub" class="form-control" value="<?= $shop->shop_bank_sub ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">ชื่อบัญชี</label>
                        <div class="col-sm-9">
                          <input type="text" name="sbankaccount" id="sbankaccount" class="form-control" value="<?= $shop->shop_bank_name_account ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="" class="col-form-label col-sm-3">เลขที่บัญชี</label>
                        <div class="col-sm-9">
                          <input type="text" name="sbanknumber" id="sbanknumber" class="form-control" value="<?= $shop->shop_bank_number ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-12">
                          <button type="button" class="btn btn-warning btn-block" onclick="editBank()" id="btnEditBank">แก้ไข</button>
                          <button type="submit" class="btn btn-warning btn-block" id="btnEditBankHidden" hidden>แก้ไข</button>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- End Form Bank -->
                    <script type="text/javascript">
                      function editBank()
                      {
                        $("#sbanknameHidden").removeAttr('hidden');
                        $("#sbankname").attr('hidden','hidden');
                        $("#sbanktypeHidden").removeAttr('hidden');
                        $("#sbanktype").attr('hidden','hidden');
                        $("#sbanksub").removeAttr('readonly');
                        $("#sbankaccount").removeAttr('readonly');
                        $("#sbanknumber").removeAttr('readonly');
                        $("#btnEditBankHidden").removeAttr('hidden');
                        $("#btnEditBank").attr('hidden','hidden');
                      }
                    </script>
                  </div>
                  <?php if (!empty($shop->shop_id)) {?>
                  <div class="tab-pane fade" id="photo" role="tabpanel" aria-labelledby="photo-tab">
                    <!-- Form Edit Profile -->
                      <div class="col-md-12">
                                  <div class="card">
                                      <div class="card-body">
                                          <?php if (!empty($photo->image_register_back1)){ ?>
                                          <div class="form-group row">
                                              <div class="col-sm-12">
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front1}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านหน้า/หลัง</p>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back1}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านข้าง/ก้น</p>
                                              </div>
                                              <div class="col-sm-12 mt-2">
                                                  <p class="text-center"><?= $photo->image_register_detail1 ?></p>
                                              </div>
                                          </div>
                                          <?php }else{?>
                                            <h3 class="text-center">ไม่มีข้อมูล</h3>
                                          <?php }?>
<!--                                           <hr>
                                          <div class="form-group row">
                                              <div class="col-sm-12">
                                                  <h1 class="text-center">พระองค์ที่ 2</h1>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front2}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านหน้า/หลัง</p>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back2}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านข้าง/ก้น</p>
                                              </div>
                                              <div class="col-sm-12 mt-2">
                                                  <p class="text-center"><?= $photo->image_register_detail2 ?></p>
                                              </div>
                                          </div>
                                          <hr>
                                          <div class="form-group row">
                                              <div class="col-sm-12">
                                                  <h1 class="text-center">พระองค์ที่ 3</h1>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front3}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านหน้า/หลัง</p>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back3}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านข้าง/ก้น</p>
                                              </div>
                                              <div class="col-sm-12 mt-2">
                                                  <p class="text-center"><?= $photo->image_register_detail3 ?></p>
                                              </div>
                                          </div>
                                          <hr>
                                          <div class="form-group row">
                                              <div class="col-sm-12">
                                                  <h1 class="text-center">พระองค์ที่ 4</h1>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front4}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านหน้า/หลัง</p>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back4}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านข้าง/ก้น</p>
                                              </div>
                                              <div class="col-sm-12 mt-2">
                                                  <p class="text-center"><?= $photo->image_register_detail4 ?></p>
                                              </div>
                                          </div>
                                          <hr>
                                          <div class="form-group row">
                                              <div class="col-sm-12">
                                                  <h1 class="text-center">พระองค์ที่ 5</h1>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_front5}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านหน้า/หลัง</p>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="justify-content-center">
                                                      <img src="<?= base_url("assets/uploads/numberid/{$photo->users_id}/{$photo->image_register_back5}") ?>" alt="" srcset="">
                                                  </div>
                                                  <p class="text-center">รูปด้านข้าง/ก้น</p>
                                              </div>
                                              <div class="col-sm-12 mt-2">
                                                  <p class="text-center"><?= $photo->image_register_detail5 ?></p>
                                              </div>
                                          </div>
                                          <hr>
                                          <a class="btn btn-warning btn-block" href="<?= base_url('admin/userslist') ?>">ย้อนกลับ</a><br/> -->
                                      </div>
                                  </div>
                              </div>
                  </div>
                   <?php }?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- END ROW -->
  </div>
<!-- END CONTAINER -->
</div>  
<?= $this->session->msg ?>