<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          ข่าวประชาสัมพันธ์
          <a href="<?= base_url('admin/news/add') ?>" class="btn btn-primary btn-sm float-right">เพิ่มข้อมูล</a>
        </div>
        <div class="card-body">
          <div class="table-responsive-sm">
            <table class="table table-striped table-bordered" id="myTable">
              <thead>
                <tr>
                  <th>รูป</th>
                  <th>หัวข้อ</th>
                  <!-- <th>รายละเอียด</th> -->
                  <th>แก้ไข</th>
                  <th>ลบ</th>
                </tr>
              </thead>
              <tbody>
                <?php if (count($items) <= 0): ?>
                  <tr>
                    <td colspan="5" class="text-center">ไม่พบข้อมูล</td>
                  </tr>
                <?php endif; ?>
                <?php foreach ($items as $row) : ?>
                <tr>
                  <td width="80px">
                    <a href="<?= base_url("admin/news/edit/{$row->blog_id}") ?>">
                      <img src="<?= base_url('assets/uploads/'.$row->blog_image) ?>" class="img-thumbnail" width="50" height="50" alt="">
                    </a>
                  </td>
                  <td width="100px" class="hidden-text"><a href="<?= base_url("admin/news/edit/{$row->blog_id}") ?>"><?= $row->blog_name ?></a></td>
                  <!-- <td class="hidden-text"><a href="<?= base_url("admin/news/edit/{$row->blog_id}") ?>"><?= $row->blog_detail ?></a></td> -->
                  <td width="10">
                    <a href="<?= base_url("admin/news/edit/{$row->blog_id}") ?>" class="btn btn-warning btn-sm btn-block"><i class="fas fa-edit"></i></a>
                  </td>
                  <td width="10">
                    <button type="button" class="btn btn-danger btn-sm btn-block" onclick="del(<?= $row->blog_id ?>)"><i class="fas fa-trash-alt"></i></button>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>
<script type="text/javascript">
  $('#myTable').DataTable();
  function del(id)
  {
    swal({
      title: "คุณต้องการลบข้อมูลนี้หรือไม่?",
      text: "หากลบแล้วข้อมูลจะหายาไปถาวร!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = "<?= base_url('admin/news/delete/') ?>" + id;
      }
    });
  }
</script>
