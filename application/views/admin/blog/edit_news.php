<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <form action="<?= base_url("admin/news/edit/{$items->blog_id}") ?>" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">หัวข้อ</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" value="<?= $items->blog_name ?>">
                <?= form_error('name','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">รูปภาพ</label>
              <div class="col-sm-10">
                <input type="file" name="file" class="form-control-file" value="">
                <?= form_error('file','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <!-- <div class="form-group row">
              <label for="" class="col-form-label col-sm-2"></label>
              <div class="col-sm-10">
                <img src="<?= base_url("assets/uploads/{$items->blog_image}") ?>" height="100px" class="" alt="">
              </div>
            </div> -->
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">รายละเอียด</label>
              <div class="col-sm-10">
                <!-- <textarea name="detail" id="editor1" class="ckkeditor" rows="8" cols="80"><?= $items->blog_detail ?></textarea> -->
                <textarea name="detail" id="detail" class="fckeditor" rows="8" cols="80"><?= $items->blog_detail ?></textarea>
                <?= form_error('detail','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2"></label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>

<!-- <script type="text/javascript">
CKEDITOR.replace( 'editor1',{
        filebrowserBrowseUrl : '../assets/uploads/blogs/',
        filebrowserUploadUrl : '../assets/uploads/blogs/',
        filebrowserImageBrowseUrl : '../assets/uploads/blogs/'
    });
</script> -->
<script type="text/javascript">
var oFCKeditor = new FCKeditor( 'detail' ) ;
oFCKeditor.BasePath = '<?php echo base_url(); ?>fckeditor/';
//oFCKeditor.Height = 300;
oFCKeditor.ReplaceTextarea() ;
</script>
