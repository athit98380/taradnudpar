<?php $data_count=$this->count_admin->Count_data();?>
<div class="container">
							<ul class="navbar-nav admin-mr-auto">
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/news') ?>"><i class="fas fa-newspaper"></i> ข่าวสาร</a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/shop') ?>"><i class="fas fa-newspaper"></i> สมัครร้านค้า <?php if($data_count['count_shop']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_shop'];?></span><?php }?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/userslist') ?>"><i class="fas fa-newspaper"></i> สมาชิก สมัครใหม่ <?php if($data_count['count_regis_user']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_regis_user'];?></span><?php }?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/renew') ?>"><i class="fas fa-newspaper"></i> ต่ออายุ <?php if($data_count['count_renew']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_renew'];?></span><?php }?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/product/get/0') ?>"><i class="fas fa-newspaper"></i> รายการสินค้ากำลังเผยแพร่ <?php if($data_count['count_product_0']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_product_0'];?></span><?php }?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/product/get/1') ?>"><i class="fas fa-newspaper"></i> รายการสินค้ารอยืนยันเผยแพร่ <?php if($data_count['count_product_1']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_product_1'];?></span><?php }?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/product/get/2') ?>"><i class="fas fa-newspaper"></i> รายการสินค้ารอยืนยันการลบ <?php if($data_count['count_product_2']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_product_2'];?></span><?php }?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/product/get/3') ?>"><i class="fas fa-newspaper"></i> รายการสินค้าลบแล้วทั้งหมด <?php if($data_count['count_product_3']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_product_3'];?></span><?php }?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="<?= base_url('admin/group') ?>"><i class="fas fa-newspaper"></i> หมวดหมู่พระเครื่อง</a>
								</li>
							</ul>		
</div>
