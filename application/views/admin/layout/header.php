<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?= $title ?></title>
		<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">


		<script type="text/javascript" src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/popper.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		<script type="text/javascript" src="//cdn.ckeditor.com/4.11.3/basic/ckeditor.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>fckeditor/fckeditor.js"></script>
	</head>
	<body>
		<?php $data_count=$this->count_admin->Count_data();?>
			<!-- Header -->
			<header>
				<!-- <img src="<?= base_url('assets/images/Logo/logo.jpg">') ?>" class="image" alt=""> -->
				<nav class="navbar navbar-expand-lg navbar-light bg-light image-header">
					<div class="container">
						<a class="navbar-brand" href="<?= base_url('admin') ?>"><img src="<?= base_url('assets/images/logo/logo-only.png') ?>" height="50px"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>

						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav mr-auto">
								<li class="nav-item <?= ($this->uri->segment(2) == 'news') ? 'active' : null ?>">
									<a class="nav-link" href="<?= base_url('admin/news') ?>"><i class="fas fa-newspaper"></i> ข่าวสาร</a>
								</li>
<!-- 								<li class="nav-item <?= ($this->uri->segment(2) == 'shop') ? 'active' : null ?>">
									<a class="nav-link" href="<?= base_url('admin/shop') ?>"><i class="fas fa-newspaper"></i> ร้านค้า <?php if($data_count['count_shop']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_shop'];?></span><?php }?></a>
								</li> -->
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fas fa-newspaper"></i> ร้านค้า <?php if($data_count['count_shop']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_shop'];?></span><?php }?>
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item <?= ($this->uri->segment(2) == 'shop') ? 'active' : null ?>" href="<?= base_url('admin/shop') ?>">สม้ครร้านค้า <?php if($data_count['count_shop']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_shop'];?></span><?php }?></a>
										<a class="dropdown-item <?= ($this->uri->segment(2) == 'shopall') ? 'active' : null ?>" href="<?= base_url('admin/shop/all') ?>">
											ร้านค้าทั้งหมด 
										</a>
									</div>
								</li>
<!-- 								<li class="nav-item <?= ($this->uri->segment(2) == 'userslist') ? 'active' : null ?>">
									<a class="nav-link" href="<?= base_url('admin/userslist') ?>"><i class="fas fa-newspaper"></i> สมัครใหม่ <?php if($data_count['count_regis_user']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_regis_user'];?></span><?php }?></a>
								</li> -->
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fas fa-cogs"></i> สมาชิก <?php if($data_count['count_regis_user']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_regis_user'];?></span><?php }?>
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item <?= ($this->uri->segment(2) == 'userslist') ? 'active' : null ?>" href="<?= base_url('admin/userslist') ?>">สมัครใหม่ <?php if($data_count['count_regis_user']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_regis_user'];?></span><?php }?></a>
										<a class="dropdown-item <?= ($this->uri->segment(2) == 'userslistall') ? 'active' : null ?>" href="<?= base_url('admin/userslist/all') ?>">
											สมาชิกทั้งหมด 
										</a>
									</div>
								</li>
								<li class="nav-item <?= ($this->uri->segment(2) == 'renew') ? 'active' : null ?>">
									<a class="nav-link" href="<?= base_url('admin/renew') ?>"><i class="fas fa-newspaper"></i> ต่ออายุ <?php if($data_count['count_renew']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_renew'];?></span><?php }?></a>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fas fa-cogs"></i> จัดการสินค้า <?php if($data_count['count_product_all']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_product_all'];?></span><?php }?>
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item <?= ($this->uri->segment(4) == '0') ? 'active' : null ?>" href="<?= base_url('admin/product/get/0') ?>">รายการสินค้ากำลังเผยแพร่ <?php if($data_count['count_product_0']!=0){?><span class="badge badge-info"><?php echo $data_count['count_product_0'];?></span><?php }?></a>
										<a class="dropdown-item <?= ($this->uri->segment(4) == '1') ? 'active' : null ?>" href="<?= base_url('admin/product/get/1') ?>">รายการสินค้ารอยืนยันเผยแพร่ <?php if($data_count['count_product_1']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_product_1'];?></span><?php }?></a>
										<a class="dropdown-item <?= ($this->uri->segment(4) == '2') ? 'active' : null ?>" href="<?= base_url('admin/product/get/2') ?>">รายการสินค้ารอยืนยันการลบ <?php if($data_count['count_product_2']!=0){?><span class="badge badge-danger"><?php echo $data_count['count_product_2'];?></span><?php }?></a>
										<a class="dropdown-item <?= ($this->uri->segment(4) == '3') ? 'active' : null ?>" href="<?= base_url('admin/product/get/3') ?>">รายการสินค้าลบแล้วทั้งหมด <?php if($data_count['count_product_3']!=0){?><span class="badge badge-info"><?php echo $data_count['count_product_3'];?></span><?php }?></a>
									</div>
								</li>
								<li class="nav-item <?= ($this->uri->segment(2) == 'group') ? 'active' : null ?>">
									<a class="nav-link" href="<?= base_url('admin/group') ?>">หมวดหมู่พระเครื่อง</a>
								</li>
								
							</ul>
							<ul class="navbar-nav ml-auto">
								<li class="nav-item">
									<a class="nav-link" href="<?= base_url() ?>" target="_blank"><i class="fas fa-external-link-alt"></i> ดูหน้าเว็บ</a>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fas fa-cogs"></i> ตั้งค่า
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item <?= ($this->uri->segment(3) == 'index') ? 'active' : null ?>" href="<?= base_url('admin/theme/index') ?>">ตกแต่งเว็บไซต์</a>
										<a class="dropdown-item <?= ($this->uri->segment(3) == 'color') ? 'active' : null ?>" href="<?= base_url('admin/theme/color') ?>">ตกแต่งสีสันเว็บไซต์</a>
										<a class="dropdown-item <?= ($this->uri->segment(2) == 'statics') ? 'active' : null ?>" href="<?= base_url('admin/statics') ?>">สถิติเว็บไซต์</a>
										<a class="dropdown-item <?= ($this->uri->segment(2) == 'menu') ? 'active' : null ?>" href="<?= base_url('admin/menu') ?>">จัดการชื่อเมนูทั้งหมด</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="<?= base_url('login/logout') ?>">ออกจากระบบ</a>
									</div>
								</li>
							</ul>
							<!-- <form class="form-inline my-2 my-lg-0">
								<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
								<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
							</form> -->
						</div>
					</div>
				</nav>
			</header>
			<!-- End Header -->
