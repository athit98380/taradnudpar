<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
          <a href="<?= base_url('admin/menu') ?>" class="btn btn-danger btn-sm float-right">กลับ</a>
        </div>
        <div class="card-body">
          <form action="<?= base_url('admin/menu/add') ?>" method="post">
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ลำดับ</label>
              <div class="col-sm-10">
                <input type="text" name="level" class="form-control" value="" placeholder="ใส่เป็นตัวเลข [เลขน้อยจะอยู่อันดับแรก]">
                <?= form_error('level','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ชื่อเมนู</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" value="" placeholder="ชื่อเมนูที่จะแสดง">
                <?= form_error('name','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ลิ้ง</label>
              <div class="col-sm-10">
                <input type="text" name="link" class="form-control" value="" placeholder="ใส่เป็นที่อยู่เต็มเช่น : https://www.google.com">
                <?= form_error('link','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-10 ml-auto">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
