<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered" id="myTable">
              <thead>
                <tr>
                  <td>ลำดับ</td>
                  <td>ชื่อเมนู</td>
                  <!-- <td>ลิ้ง</td> -->
                  <td>แก้ไข</td>
                  <!-- <td>ลบ</td> -->
                </tr>
              </thead>
              <tbody>
                <?php foreach ($menu as $row) : ?>
                <tr>
                  <td width="10"><?= $row->menu_level ?></td>
                  <td><?= $row->menu_name ?></td>
                  <!-- <td><?= $row->menu_link ?></td> -->
                  <td width="10">
                    <a href="<?= base_url("admin/menu/edit/{$row->menu_id}") ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                  </td>
                  <!-- <td width="10">
                    <button type="button" class="btn btn-danger btn-sm" id="del" data-id="$row->menu_id"><i class="fas fa-trash-alt"></i></button>
                  </td> -->
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>
<script type="text/javascript">
  $('#myTable').DataTable();

  $('#del').click(function(){
    swal({
      title: "คุณต้องการลบข้อมูลนี้จริงหรือไม่?",
      text: "ข้อมูลจะหายไปถาวร!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = "<?= base_url('admin/menu/delete/') ?>" + $(this).data('id');
      }
    });
  });
</script>
