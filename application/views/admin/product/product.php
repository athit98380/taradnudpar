<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered" id="myTable">
              <thead>
                <tr align="center">
                  <th>รูป</th>
                  <th>ชื่อสินค้า</th>
                  <th>รายละเอียด</th>
                  <th>ราคา</th>
                  <th>วันที่อัพเดต</th>
                  <th>สถานะการแจ้ง</th>
                  <th>ดู</th>
                  <th>ยืนยัน</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($products as $row) : ?>
                  <?php $images=explode( ",",$row->product_image);?>
                <tr align="center">
                  <td>
                    <img style="max-width: 300px;" src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$images[0]}") ?>" width="50" height="50" class="img-thumbnail img-fluid" alt="">
                  </td>
                  <td><?= $row->product_name ?></td>
                  <td><?php echo iconv_substr($row->product_detail, 0,300, "UTF-8");?></td>
                  <td><?= $row->product_price ?></td>
                  <td><?= $row->product_update_on ?></td>
                  <td><button type="button" class="btn btn-danger btn-sm" onclick="del(<?= $row->product_id ?>)"><i class="fas fa-trash-alt"></i></button></td>
                  <td width="10">
                    <a href="<?= base_url("admin/product/detail/{$row->product_id}") ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fas fa-search"></i></a>
                  </td>
                  <td width="10">
                    <?php if ($row->product_status!=0){ ?>
                    <button type="button" class="btn btn-danger btn-sm" onclick="confirm(<?= $row->product_id ?>,<?= $row->shop_id ?>)"><i class="fas fa-check"></i></button>
                    <?php } ?>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>
<script type="text/javascript">
  $('#myTable').DataTable();
  function confirm(pid) {
        window.location.href = "<?= base_url('admin/product/confirm/') ?>" + pid;
  }
  function del(pid,sid) {
    swal({
      title: "คุณต้องการยืนยันจริงหรือไม่?",
      text: "หากยืนยันแล้วจะส่งผลทันที!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      })
      .then((willDelete) => {
      if (willDelete) {
        window.location.href = "<?= base_url('admin/product/confirm_del/') ?>" + sid+ "/" + pid;
      }
    });
  }
</script>
