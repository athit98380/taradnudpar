<div class="container mt-2">
  <div class="row">
    <div class="col-md-8 mx-auto">
      <div class="card">
        <div class="card-header">
          รายละอียดสินค้า
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-2">รหัสสินค้า</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="<?= $products->product_no ?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-2">ชื่อสินค้า</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="<?= $products->product_name ?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-2">รายละเอียด</label>
            <div class="col-sm-10">
              <textarea rows="8" cols="80" class="form-control" readonly><?= $products->product_detail ?></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-2">จำนวนคนดู</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="<?= $products->product_view ?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-form-label col-sm-2">ราคา</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="<?= $products->product_price ?>" readonly>
            </div>
          </div>
              <?php $images=explode( ",",$products->product_images);
              ?>
              <?php for ($i=1; $i < 11; $i++) { ?>
              <div class="form-group row margin-bottom-60">
                <label for="" class="col-form-label col-sm-2">รูป</label>
                <div class="col-md-10">
                  <?php if(!empty($images[$i-1])){?>
                    <img class="card-img-top" src="<?= base_url("assets/uploads/numberid/{$products->users_id}/".$images[$i-1]) ?>">
                  <?php }?>
                </div>
              </div>
              <?php }?>
      </div>
    </div>
  </div>
</div>
