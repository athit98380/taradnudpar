<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <form action="<?= base_url('admin/statics/edit') ?>" method="post">
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ผู้ชมทั้งหมด</label>
              <div class="col-sm-10">
                <input type="text" name="view_all" class="form-control" value="<?= $static->static_view_all ?>">
                <?= form_error('view_all','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ผู้ชมวันนี้</label>
              <div class="col-sm-10">
                <input type="text" name="view_to_day" class="form-control" value="<?= $static->static_view_today ?>">
                <?= form_error('view_to_day','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ออนไลน์ขณะนี้</label>
              <div class="col-sm-10">
                <input type="text" name="view_online" class="form-control" value="<?= $static->static_view_online ?>">
                <?= form_error('view_online','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-10 ml-auto">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
