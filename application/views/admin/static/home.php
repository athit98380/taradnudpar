<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered" id="myTable">
              <thead>
                <tr>
                  <th>ผู้ชมทั้งหมด</th>
                  <th>ผู้ชมวันนี้</th>
                  <th>ออนไลน์ขณะนี้</th>
                  <th>แก้ไข</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><?= $static->static_view_all ?></td>
                  <td><?= $static->static_view_today ?></td>
                  <td><?= $static->static_view_online ?></td>
                  <td width="10">
                    <a href="<?= base_url("admin/statics/edit") ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>
<script type="text/javascript">
  // $('#myTable').DataTable();
</script>
