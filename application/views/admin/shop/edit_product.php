<div class="container mt-2">
  <div class="row">
    <div class="col-md-12 mx-auto">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
          <a href="#" class="btn btn-danger btn-sm float-right">กลับ</a>
        </div>
        <div class="card-body">
          <form action="<?= base_url("admin/shop/edit_product/{$product->users_id}/{$product->shop_id}/{$product->product_id}") ?>" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="text-center" style="width: 100%;">สถานะสินค้า 
                  <?php if($product->product_status==0){?>
                  <strong style="color:green;">เผยแพร่</strong>
                  <?php }?>
                  <?php if($product->product_status==1){?>
                  <strong style="color:red;">รอยืนยัน</strong>
                  <?php }?>
                  <?php if($product->product_status==2){?>
                  <strong style="color:red;">รอยืนยัน</strong>
                  <?php }?>
                </div>  
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ชื่อสินค้า</label>
                <div class="col-md-10">
                  <input type="text" name="pname" class="form-control" value="<?= (isset($product->product_name)) ? $product->product_name : null ?>" required>
                  <?= form_error('pname','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ราคา</label>
                <div class="col-md-10">
                  <input type="text" name="pprice" class="form-control" value="<?= (isset($product->product_price)) ? $product->product_price : null ?>" required>
                  <?= form_error('pprice','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">ประเภท</label>
                <div class="col-md-10">
                  <select class="form-control" name="ptype" required>
                    <option value="">เลือก</option>
                    <?php foreach ($ptype as $row) : ?>
                      <option <?php if(!empty($product->product_type_id)){ if($product->product_type_id == $row->product_type_id){ echo 'selected'; } } ?>  value="<?= $row->product_type_id ?>"><?= $row->product_type_name ?></option>
                    <?php endforeach; ?>
                  </select>
                  <?= form_error('ptype','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">สถานะ</label>
                <div class="col-md-10">
                  <select class="form-control" name="pstatusfs" required>
                    <option value="">เลือก</option>
                    <option <?= ($product->product_status_for_sale == 'โชว์พระ') ? 'selected' : null ?> value="โชว์พระ">โชว์พระ</option>
                    <option <?= ($product->product_status_for_sale == 'ขายแล้ว') ? 'selected' : null ?> value="ขายแล้ว">ขายแล้ว</option>
                    <option <?= ($product->product_status_for_sale == 'โทรถาม') ? 'selected' : null ?> value="โทรถาม">โทรถาม</option>
                    <option <?= ($product->product_status_for_sale == 'เสนอราคา') ? 'selected' : null ?> value="เสนอราคา">เสนอราคา</option>
                  </select>
                  <?= form_error('ptype','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รายละเอียด</label>
                <div class="col-md-10">
                  <textarea name="pdetail" class="form-control" rows="4" cols="80" required><?= (isset($product->product_detail)) ? $product->product_detail : null ?></textarea>
                  <?= form_error('pdetail','<small class="text-danger">','</small>') ?>
                </div>
              </div>
              <?php $images=explode( ",",$product->product_images);
              $image_thumb=explode( ",",$product->product_image);
              ?>
              <?php for ($i=1; $i < 6; $i++) { ?>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รูป</label>
                <div class="col-md-10">
                  <?php 
                  if(!empty($images[$i-1])){?>
                <img class="card-img-top" src="<?= base_url("assets/uploads/numberid/{$product->users_id}/".$images[$i-1]) ?>">
                <input type="hidden" name="image_check_front<?php echo $i; ?>" value="<?php echo $images[$i-1];?>">
                <input type="hidden" name="file_thumb<?php echo $i; ?>" value="<?php echo $image_thumb[$i-1];?>">
                เปลี่ยนภาพ 
                <?php }else{?>
                อัพโหลดภาพ
                <?php }?>
                  <input type="file" name="pimage<?php echo $i;?>" class="form-control-file" >
                </div>
              </div>
              <?php }?>
              <div class="form-group row">
                <div class="col-md-10 ml-auto">
                  <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
