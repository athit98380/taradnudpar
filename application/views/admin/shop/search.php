<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <table class="table table-striped table-bordered" id="myTable">
            <thead>
              <tr class="text-center">
                <th>ชื่อร้าน</th>
                <!-- <th>เยียมชม</th> -->
                <th>วันหมดอายุ</th>
                <th>สถานะ</th>
                <th>ดู</th>
                <th>แก้ไข</th>
                <th>ลบ</th>
              </tr>
            </thead>
            <tbody>
              <tr class="text-center">
                <td><?= $shop->shop_name ?></td>
                <!-- <td><?= $shop->shop_view ?></td> -->
                <td><?= $this->datethai->DateLong($shop->shop_expired_on) ?></td>
                <td><?= ($shop->shop_status == 1) ? '<font color="red">รอดำเนินการ</font>' : '<font color="green">ยืนยันแล้ว</font>'; ?></td>
                <td width="10">
                  <div class="dropdown">
                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-search"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="<?= base_url("main/shop/{$shop->shop_id}") ?>" target="_blank">ดูหน้าร้าน</a>
                      <a class="dropdown-item" href="<?= base_url("admin/shop/product/{$shop->shop_id}") ?>">จัดการสินค้า</a>
                    </div>
                  </div>
                </td>
                <td width="10">
                  <a href="<?= base_url("admin/shop/edit_shop/{$shop->shop_id}") ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                </td>
                <td width="10">
                  <button type="button" class="btn btn-danger btn-sm" id="del" data-id="<?= $shop->shop_id ?>"><i class="fas fa-trash-alt"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#myTable').DataTable();

  $('#del').click(function(){
    swal({
      title: "คุณต้องการลบข้อมูลนี้หรือไม่?",
      text: "ข้อมูลนี้จะหายไปถาวร! (รวมทั้ง ผู้ใช้ด้วย)",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = "<?= base_url('admin/shop/delete/') ?>" + $(this).data('id');
      }
    });
  });
</script>
<?= $this->session->msg ?>
