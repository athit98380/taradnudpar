<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
        </div>
        <div class="card-body">
          <form action="<?= base_url("admin/shop/edit_shop/{$shop->shop_id}") ?>" method="post">
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">รหัสร้านค้า</label>
              <div class="col-sm-10">
                <input type="text" name="sid" class="form-control" value="<?= $shop->shop_id ?>" readonly>
                <?= form_error('sid','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ชื่อร้านค้า</label>
              <div class="col-sm-10">
                <input type="text" name="sname" class="form-control" value="<?= $shop->shop_name ?>">
                <?= form_error('sname','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ที่อยู่</label>
              <div class="col-sm-10">
                <textarea name="saddress" rows="4" cols="80" class="form-control"><?= $shop->shop_address ?></textarea>
                <?= form_error('saddress','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ข้อมูลร้าน</label>
              <div class="col-sm-10">
                <textarea name="sdetail" rows="4" cols="80" class="form-control"><?= $shop->shop_detail ?></textarea>
                <?= form_error('sdetail','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">เงื่อนไขการรับประกัน : </label>
              <div class="col-sm-10">
                <textarea name="sguarantee" class="form-control" rows="4" cols="80" required><?= $shop->shop_guarantee ?></textarea>
                <?= form_error('sguarantee','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ผู้รับรองการเปิดร้าน : </label>
              <div class="col-sm-10">
                <input type="text" name="sguarantee_by" class="form-control" value="<?= $shop->shop_guarantee_by ?>" required>
                <?= form_error('sguarantee_by','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">โทรศัพท์ผู้รับรอง : </label>
              <div class="col-sm-10">
                <input type="text" name="sguarantee_by_tel" class="form-control" value="<?= $shop->shop_guarantee_by_tel ?>" minlength="10" maxlength="10" required>
                <?= form_error('sguarantee_by_tel','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ธนาคาร : </label>
              <div class="col-sm-10">
                <select class="form-control" name="sbankname" required>
                  <option value="" selected="selected">เลือกธนาคาร</option>
                  <option value="ธนาคารกรุงเทพ" <?= ($shop->shop_bank_name == 'ธนาคารกรุงเทพ') ? 'selected' : null ?> >ธนาคารกรุงเทพ</option>
                  <option value="ธนาคารทหารไทย" <?= ($shop->shop_bank_name == 'ธนาคารทหารไทย') ? 'selected' : null ?>>ธนาคารทหารไทย</option>
                  <option value="ธนาคารกรุงศรีอยุธยา" <?= ($shop->shop_bank_name == 'ธนาคารกรุงศรีอยุธยา') ? 'selected' : null ?>>ธนาคารกรุงศรีอยุธยา</option>
                  <option value="ธนาคารกสิกรไทย" <?= ($shop->shop_bank_name == 'ธนาคารกสิกรไทย') ? 'selected' : null ?>>ธนาคารกสิกรไทย</option>
                  <option value="ธนาคารไทยพาณิชย์" <?= ($shop->shop_bank_name == 'ธนาคารไทยพาณิชย์') ? 'selected' : null ?>>ธนาคารไทยพาณิชย์</option>
                  <option value="ธนาคารธนชาต" <?= ($shop->shop_bank_name == 'ธนาคารธนชาต') ? 'selected' : null ?>>ธนาคารธนชาต</option>
                  <option value="ธนาคารยูโอบี" <?= ($shop->shop_bank_name == 'ธนาคารยูโอบี') ? 'selected' : null ?>>ธนาคารยูโอบี</option>
                  <option value="ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)" <?= ($shop->shop_bank_name == 'ธนาคารสแตนดาร์ดชาร์เตอร์ด') ? 'selected' : null ?>>ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)</option>
                  <option value="ธนาคารกรุงไทย" <?= ($shop->shop_bank_name == 'ธนาคารกรุงไทย') ? 'selected' : null ?>>ธนาคารกรุงไทย</option>
                  <option value="ธนาคารออมสิน" <?= ($shop->shop_bank_name == 'ธนาคารออมสิน') ? 'selected' : null ?>>ธนาคารออมสิน</option>
                  <option value="ธนาคารอาคารสงเคราะห์" <?= ($shop->shop_bank_name == 'ธนาคารอาคารสงเคราะห์') ? 'selected' : null ?>>ธนาคารอาคารสงเคราะห์</option>
                  <option value="ธนาคารซีไอเอ็มบีไทย" <?= ($shop->shop_bank_name == 'ธนาคารซีไอเอ็มบีไทย') ? 'selected' : null ?>>ธนาคารซีไอเอ็มบีไทย</option>
                  <option value="ธนาคารทิสโก้" <?= ($shop->shop_bank_name == 'ธนาคารทิสโก้') ? 'selected' : null ?>>ธนาคารทิสโก้</option>
                  <option value="ธนาคารไทยเครดิตเพื่อรายย่อย" <?= ($shop->shop_bank_name == 'ธนาคารไทยเครดิตเพื่อรายย่อย') ? 'selected' : null ?>>ธนาคารไทยเครดิตเพื่อรายย่อย</option>
                  <option value="ธนาคารแลนด์" <?= ($shop->shop_bank_name == 'ธนาคารแลนด์ แอนด์ เฮาส') ? 'selected' : null ?>>ธนาคารแลนด์ แอนด์ เฮาส์</option>
                  <option value="ธนาคารเกียรตินาคิน" <?= ($shop->shop_bank_name == 'ธนาคารเกียรตินาคิน') ? 'selected' : null ?>>ธนาคารเกียรตินาคิน</option>
                  <option value="ธนาคารอิสลามแห่งประเทศไทย" <?= ($shop->shop_bank_name == 'ธนาคารอิสลามแห่งประเทศไทย') ? 'selected' : null ?>>ธนาคารอิสลามแห่งประเทศไทย</option>
                  <option value="ธนาคารไอซีบีซี (ไทย)" <?= ($shop->shop_bank_name == 'ธนาคารไอซีบีซี (ไทย)') ? 'selected' : null ?>>ธนาคารไอซีบีซี (ไทย)</option>
                  <option value="ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร" <?= ($shop->shop_bank_name == 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร') ? 'selected' : null ?>>ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร</option>
                  <option value="ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย" <?= ($shop->shop_bank_name == 'ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย') ? 'selected' : null ?>>ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย</option>
                  <option value="ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย" <?= ($shop->shop_bank_name == 'ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย') ? 'selected' : null ?>>ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย</option>
                </select>
                <?= form_error('sbankname','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ประเภทบัญชี : </label>
              <div class="col-sm-10">
                <select class="form-control" name="sbanktype" required>
                  <option value="" selected="selected">เลือกประเภทบัญชี</option>
                  <option value="ออมทรัพย์" <?= ($shop->shop_bank_type == 'ออมทรัพย์') ? 'selected' : null ?>>ออมทรัพย์</option>
                  <option value="กระแสรายวัน" <?= ($shop->shop_bank_type == 'กระแสรายวัน') ? 'selected' : null ?>>กระแสรายวัน</option>
                </select>
                <?= form_error('sbanktype','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">สาขา : </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="sbanksub" value="<?= $shop->shop_bank_sub ?>" required>
                <?= form_error('sbanksub','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ชื่อบัญชี : </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="sbanknameaccount" value="<?= $shop->shop_bank_name_account ?>" required>
                <?= form_error('sbanknameaccount','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">เลขที่บัญชี : </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="sbanknumber" value="<?= $shop->shop_bank_number ?>" minlength="15" maxlength="15" required>
                <?= form_error('sbanknumber','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">วันหมดอายุ : </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="sexpired" value="<?= $shop->shop_bank_number ?>" required>
                <?= form_error('sexpired','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">สถานะ : </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="sstatus" value="<?= $shop->shop_status ?>" required>
                <?= form_error('sstatus','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">จำนวนคนดู : </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="sview" value="<?= $shop->shop_view ?>" required>
                <?= form_error('sview','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-10 ml-auto">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
