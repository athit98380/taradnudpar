<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
          <a href="<?= base_url('admin/shop') ?>" class="btn btn-danger btn-sm float-right">กลับ</a>
        </div>
        <div class="card-body">
          <table class="table table-striped table-bordered" id="myTable">
            <thead>
              <tr>
                <th>รหัสสินค้า</th>
                <th>ชื่อ</th>
                <th>ราคา</th>
                <th>สถานะสินค้า</th>
                <!-- <th>สถานะ</th> -->
                <th>แก้ไข</th>
                <th>ลบ</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $row) : ?>
                <?php $images=explode( ",",$row->product_image);?>
              <tr>
                <td><a href="<?= base_url("admin/shop/edit_product/{$row->users_id}/{$row->shop_id}/{$row->product_id}") ?>"><img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$images[0]}") ?>" style="width: 60px!important;"></a></td>
                <td><a href="<?= base_url("admin/shop/edit_product/{$row->users_id}/{$row->shop_id}/{$row->product_id}") ?>"><?= $row->product_name ?></a></td>
                <td><?= $row->product_price ?></td>
                <td>สถานะสินค้า 
                  <?php if($row->product_status==0){?>
                  <strong style="color:green;">เผยแพร่</strong>
                  <?php }?>
                  <?php if($row->product_status==1){?>
                  <strong style="color:red;">รอยืนยัน</strong>
                  <?php }?>
                  <?php if($row->product_status==2){?>
                  <strong style="color:red;">รอยืนยัน</strong>
                  <?php }?></td>
                <!-- <td><?= $row->product_status ?></td> -->
                <td width="10">
                  <a href="<?= base_url("admin/shop/edit_product/{$row->users_id}/{$row->shop_id}/{$row->product_id}") ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                </td>
                <td width="10">
                  <button type="button" class="btn btn-danger btn-sm" id="del<?= $row->product_id ?>" data-id="<?= $row->product_id ?>"><i class="fas fa-trash-alt"></i></button>
                </td>
              </tr>
<script type="text/javascript">
  $('#del<?= $row->product_id ?>').click(function(){
    swal({
      title: "คุณต้องการลบข้อมูลนี้หรือไม่?",
      text: "ข้อมูลนี้จะหายไปถาวร!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = "<?= base_url('admin/shop/delete_product/'.$row->shop_id.'/') ?>" + $(this).data('id');
      }
    });
  });
</script>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#myTable').DataTable();
</script>
<?= $this->session->msg ?>
