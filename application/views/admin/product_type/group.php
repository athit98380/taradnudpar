<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Group
          <a href="<?= base_url('admin/group/add') ?>" class="btn btn-primary btn-sm float-right">เพิ่มข้อมูล</a>
        </div>
        <div class="card-body">
          <div class="table-responsive-sm">
            <table class="table table-striped table-bordered" id="myTable">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <!-- <th>Status</th> -->
                  <th>Tools</th>
                </tr>
              </thead>
              <tbody>
                <?php if (count($items) <= 0): ?>
                  <tr class="text-center">
                    <td colspan="4">ไม่พบข้อมูล!</td>
                  </tr>
                <?php endif; ?>
                <?php foreach ($items as $row) : ?>
                  <tr class="text-center">
                    <td width="100px"><?= $row->product_type_id ?></td>
                    <td><?= $row->product_type_name ?></td>
                    <!-- <td width="100px"><?= $row->product_type_status ?></td> -->
                    <td width="160px">
                      <div class="row">
                        <div class="col">
                          <a href="<?= base_url("admin/group/edit/{$row->product_type_id}") ?>" class="btn btn-warning btn-sm w-100">แก้ไข</a>
                        </div>
                        <div class="col">
                          <button type="button" name="button" class="btn btn-danger btn-sm w-100" onclick="del(<?= $row->product_type_id ?>)">ลบ</button>
                        </div>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>

<script type="text/javascript">
  $('#myTable').DataTable();
  function del(id)
  {
    swal({
    title: "คุณต้องการลบข้อมูลนี้หรือไม่?",
    text: "หากลบแล้วข้อมูลจะหายาไปถาวร!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      window.location.href = "<?= base_url('admin/group/delete/') ?>" + id;
    }
  });
  }
</script>
