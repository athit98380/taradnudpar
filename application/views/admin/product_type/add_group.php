<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          เพิ่มหมวดหมู่พระเครื่อง
          <a href="<?= base_url('admin/group') ?>" class="btn btn-danger btn-sm float-right">กลับ</a>
        </div>
        <div class="card-body">
          <form action="<?= base_url('admin/group/add') ?>" method="post">
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2">ชื่อหมวดหมู่พระเครื่อง</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control" >
                <?= form_error('name','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-2"></label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
