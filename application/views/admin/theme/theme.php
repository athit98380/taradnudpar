<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
          <a href="<?= base_url('admin/menu') ?>" class="btn btn-danger btn-sm float-right">กลับ</a>
        </div>
        <div class="card-body">
          <form action="<?= base_url("admin/theme/edit") ?>" method="post" enctype="multipart/form-data">
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รูป Background</label>
                <div class="col-md-10">
                  <?php 
                  if(!empty($theme->theme_bg_image)){?>
                <img id="theme_bg_image_old_img" class="card-img-top" style="width: unset" src="<?= base_url("assets/images/theme/".$theme->theme_bg_image) ?>">
                <input type="hidden" id="theme_bg_image_old" name="theme_bg_image_old" value="<?php echo $theme->theme_bg_image;?>"><span onclick="hidden_show('theme_bg_image_old_img','theme_bg_image_old')" class="btn btn-danger">ลบภาพ</span>
                เปลี่ยนภาพ 
                <?php }else{?>
                อัพโหลดภาพ
                <?php }?>
                  <input type="file" name="theme_bg_image" class="form-control-file" >
                </div>
              </div>

              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รูป Header</label>
                <div class="col-md-10">
                  <?php 
                  if(!empty($theme->theme_h_image)){?>
                <img id="image_header_old_img" class="card-img-top" src="<?= base_url("assets/images/theme/".$theme->theme_h_image) ?>">
                <input type="hidden" id="image_header_old" name="image_header_old" value="<?php echo $theme->theme_h_image;?>"><span onclick="hidden_show('image_header_old_img','image_header_old')" class="btn btn-danger">ลบภาพ</span>
                เปลี่ยนภาพ 
                <?php }else{?>
                อัพโหลดภาพ
                <?php }?>
                  <input type="file" name="image_header" class="form-control-file" >
                </div>
              </div>
            <div class="form-group row">
              <div class="col-sm-6">
                ความสูง Header  หน่วยความสูง px  กว้างมากสุด 1920 px
                <input type="text" name="theme_h_height" class="form-control" value="<?= $theme->theme_h_height ?>" placeholder="ความสูง">
                <?= form_error('name','<small class="text-danger">','</small>') ?>
              </div>
              <div class="col-sm-6">
              </div>
            </div>
            <hr>
              <div class="form-group row">
                <label for="" class="col-form-label col-sm-2">รูป Footer</label>
                <div class="col-md-10">
                  <?php 
                  if(!empty($theme->theme_f_image)){?>
                <img id="image_footer_old_img" class="card-img-top" src="<?= base_url("assets/images/theme/".$theme->theme_f_image) ?>">
                <input type="hidden" id="image_footer_old" name="image_footer_old" value="<?php echo $theme->theme_f_image;?>"><span  onclick="hidden_show('image_footer_old_img','image_footer_old')" class="btn btn-danger">ลบภาพ</span>
                เปลี่ยนภาพ 
                <?php }else{?>
                อัพโหลดภาพ
                <?php }?>
                  <input type="file" name="image_footer" class="form-control-file" >
                </div>
              </div>
            <div class="form-group row">
              <div class="col-sm-6">
                ความสูง Footer  หน่วยความสูง px  กว้างมากสุด 1920 px
                <input type="text" name="theme_f_height" class="form-control" value="<?= $theme->theme_f_height ?>" placeholder="ความสูง">
                <?= form_error('name','<small class="text-danger">','</small>') ?>
              </div>
              <div class="col-sm-6">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12">
                รายละเอียด Footer
                <textarea name="detail" id="detail" class="fckeditor" rows="8" cols="80"><?= $theme->theme_f_detail ?></textarea>
                <?= form_error('name','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12">
                รายละเอียดลิขสิทธิ์
                <textarea name="detail_footer" id="detail_footer" class="fckeditor" rows="8" cols="80"><?= $theme->theme_f_all_right ?></textarea>
                <?= form_error('name','<small class="text-danger">','</small>') ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 ml-auto">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>
<script type="text/javascript">
var oFCKeditor = new FCKeditor( 'detail' ) ;
oFCKeditor.BasePath = '<?php echo base_url(); ?>fckeditor/';
//oFCKeditor.Height = 300;
oFCKeditor.ReplaceTextarea() ;

var oFCKeditor = new FCKeditor( 'detail_footer' ) ;
oFCKeditor.BasePath = '<?php echo base_url(); ?>fckeditor/';
//oFCKeditor.Height = 300;
oFCKeditor.ReplaceTextarea() ;

function hidden_show(id,old) {
  document.getElementById(old).value="";
  var x = document.getElementById(id);
  if (x.style.display === "none") {
    
  } else {
    x.style.display = "none";
  }
}
</script>

