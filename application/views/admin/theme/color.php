<script src="<?= base_url('assets/js/jscolor.js') ?>"></script>
<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <?= $title ?>
          <a href="<?= base_url('admin/menu') ?>" class="btn btn-danger btn-sm float-right">กลับ</a>
        </div>
        <div class="card-body">
          <form action="<?= base_url("admin/theme/color") ?>" method="post" enctype="multipart/form-data">
            <h5>สีอักษรทั้งหมด และ พื้นหลังทั้งหมด โดยรวม</h5>
            <div class="form-group row">
              <div class="col-sm-4">
                สี BG: 1F0C00 <br/>
                สี Font: FFFFFF
              </div>
              <div class="col-sm-8">
                <input name="theme_color_bg_all" class="jscolor" value="<?php if(!empty($theme->theme_color_bg_all)){echo $theme->theme_color_bg_all;} ?>"> <br/>
                <input name="theme_color_font_all" class="jscolor" value="<?php if(!empty($theme->theme_color_font_all)){echo $theme->theme_color_font_all;} ?>">
              </div>
            </div>
            <hr>
            <h5>สีหมวดหมู่พระเครื่องด้านซ้ายมือ</h5>
            <div class="form-group row">
              <div class="col-sm-4">
                สี BG: 191919 <br/>
                สี Font: FFFFFF
              </div>
              <div class="col-sm-8">
                <input name="theme_d_category_bg" class="jscolor" value="<?php if(!empty($theme->theme_d_category_bg)){echo $theme->theme_d_category_bg;} ?>"> <br/>
                <input name="theme_d_category_font" class="jscolor" value="<?php if(!empty($theme->theme_d_category_font)){echo $theme->theme_d_category_font;} ?>">
              </div>
            </div>
            <hr>
            <h5>รายการพระเครื่องเวลาที่คลิกเปิดชม</h5>
            <div class="form-group row">
              <div class="col-sm-4">
                สี BG: 191919 <br/>
                สี Font: FFFFFF
              </div>
              <div class="col-sm-8">
                <input name="theme_p_modal_bg" class="jscolor" value="<?php if(!empty($theme->theme_p_modal_bg)){echo $theme->theme_p_modal_bg;} ?>"> <br/>
                <input name="theme_p_modal_font" class="jscolor" value="<?php if(!empty($theme->theme_p_modal_font)){echo $theme->theme_p_modal_font;} ?>">
              </div>
            </div>
            <hr>
            <h5>รายการพระเครื่อง</h5>
            <div class="form-group row">
              <div class="col-sm-4">
                สี BG: FFFFFF<br/>
                สี Font: 191919
              </div>
              <div class="col-sm-8">
                <input name="theme_p_list_bg" class="jscolor" value="<?php if(!empty($theme->theme_p_list_bg)){echo $theme->theme_p_list_bg;} ?>"> <br/>
                <input name="theme_p_list_font" class="jscolor" value="<?php if(!empty($theme->theme_p_list_font)){echo $theme->theme_p_list_font;} ?>">
              </div>
            </div>
            <hr>
            <h5>แทบข่าวด้านข้างขวามือ</h5>
            <div class="form-group row">
              <div class="col-sm-4">
                สี BG: 191919<br/>
                สี Font: FFFFFF
              </div>
              <div class="col-sm-8">
                <input name="theme_d_news_bg" class="jscolor" value="<?php if(!empty($theme->theme_d_news_bg)){echo $theme->theme_d_news_bg;} ?>"> <br/>
                <input name="theme_d_news_font" class="jscolor" value="<?php if(!empty($theme->theme_d_news_font)){echo $theme->theme_d_news_font;} ?>">
              </div>
            </div>
            <hr>
            <h5>รายชื่อเมนูด้านบน</h5>
            <div class="form-group row">
              <div class="col-sm-4">
                สี BG: 191919 <br/>
                สี Font: FFFFFF
              </div>
              <div class="col-sm-8">
                <input name="theme_d_menu_bg" class="jscolor" value="<?php if(!empty($theme->theme_d_menu_bg)){echo $theme->theme_d_menu_bg;} ?>"> <br/>
                <input name="theme_d_menu_font" class="jscolor" value="<?php if(!empty($theme->theme_d_menu_font)){echo $theme->theme_d_menu_font;} ?>">
              </div>
            </div>
            <h5>หน้า เช่น ติดต่อเรา - ระเบียบการใช้งาน</h5>
            <div class="form-group row">
              <div class="col-sm-4">
                สี BG: 191919 <br/>
                สี Font: FFFFFF
              </div>
              <div class="col-sm-8">
                <input name="theme_page_bg" class="jscolor" value="<?php if(!empty($theme->theme_page_bg)){echo $theme->theme_page_bg;} ?>"> <br/>
                <input name="theme_page_font" class="jscolor" value="<?php if(!empty($theme->theme_page_font)){echo $theme->theme_page_font;} ?>">
              </div>
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-sm-12 ml-auto">
                <button type="submit" class="btn btn-primary btn-block">ยืนยัน</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->session->msg ?>
<script type="text/javascript">
function hidden_show(id,old) {
  document.getElementById(old).value="";
  var x = document.getElementById(id);
  if (x.style.display === "none") {
    
  } else {
    x.style.display = "none";
  }
}
</script>

