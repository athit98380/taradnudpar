<div class="container mt-2">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><?= $title ?></div>
                <div class="card-body">
                    <table class="table table-striped table-bordered" id="myTable">
                        <thead>
                            <tr align="center">
                            <th>รูป</th>
                            <th>จำนวนปี</th>
                            <th>เวลาอัพไฟล์</th>
                            <th>การทำงาน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($renew as $row) : ?>
                            <tr align="center">
                                <td>
                                    <a href="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$row->renew_confirm}") ?>" target="_blank">
                                        <img src="<?= base_url("assets/uploads/numberid/{$row->users_id}/{$row->renew_confirm}") ?>" width="50" height="50" class="img-thumbnail img-fluid" alt="">
                                    </a>
                                </td>
                                <td><?= $row->renew_confirm_by ?></td>
                                <td><?= $row->renew_create_on ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            เลือก
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="<?= base_url("admin/shop/search/{$row->users_id}") ?>" target="_blank">ข้อมูลร้าน</a>
                                            <a class="dropdown-item" href="<?= base_url("admin/renew/confirm/{$row->renew_id}?year={$row->renew_confirm_by}") ?>">ยืนยัน</a>
                                            <a class="dropdown-item" href="<?= base_url("admin/renew/unconfirm/{$row->renew_id}") ?>" >ยกเลิก</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->session->msg ?>
<script>
    $('#myTable').DataTable();
</script>