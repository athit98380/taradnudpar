        <?php
        //check user level
	    $dataLevel = $this->userlevel->checkLevel($role);

      $result = $this->user_model->getAllSettings();
	    $site_title = $result->site_title;
	    //check user level
        ?>
        <nav class="navbar navbar-inverse">
            <div class="container">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="<?php echo site_url();?>main/"><?php echo $site_title; ?></a>
                </div>
            
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li <?php if(@$active_menu=="main"){echo 'class="active"';}?>><a href="<?php echo site_url();?>main/"><i class="fas fa-tachometer-alt"></i> แผงควบคุม</a></li>
                            <li <?php if(@$active_menu=="customer"){echo 'class="active"';}?>><a href="<?php echo site_url('Newcustomer');?>"><i class="fa fa-users" aria-hidden="true"></i> จัดการข้อมูลลูกค้า</a></li>
                            <!-- <li <?php if(@$active_menu=="order"){echo 'class="active"';}?>><a href="<?php echo site_url('Order');?>"><i class="fas fa-file-invoice-dollar"></i> จัดการข้อมูลออเดอร์</a></li>  -->
                            <li <?php if(@$active_menu=="facebook" || @$active_menu=="product_type"){echo 'class="active dropdown"';}else{echo 'class="dropdown"';}?>>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-list"></i> จัดการอื่นๆ <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('Fanpage');?>">จัดการแฟนเพจ</a></li>
                                <li><a href="<?php echo site_url('Product_type');?>">จัดการประเภทพระเครื่อง</a></li>
                                <?php if($dataLevel == 'is_admin'){?><li><a href="<?php echo site_url('Order_export_excel');?>">Export ออเดอร์</a><?php } ?>
                              </ul>
                            </li>
                    <?php
                        if($dataLevel == 'is_admin'){ ?>
                            <li <?php if(@$active_menu=="setting"){echo 'class="active dropdown"';}else{echo 'class="dropdown"';}?>>
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i> ตั้งค่า <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url();?>main/settings">ตั้งค่า</a></li>
                                <li><a href="<?php echo site_url();?>main/users">รายชื่อผู้ใช้</a></li>
                                <li><a href="<?php echo site_url();?>main/adduser">เพิ่มผู้ใช้</a></li>
                                <li><a href="<?php echo site_url();?>main/show_approved">อนุมัติผู้ใช้</a></li>
                                <li><a href="<?php echo site_url();?>main/banuser">ยกเลิกการใช้งานสมาชิก</a></li>
                                <li><a href="<?php echo site_url();?>main/changelevel">ระดับสมาชิก</a></li>
                              </ul>
                            </li>
                        <?php }
                    ?>
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                    <li <?php if(@$active_menu=="profile"){echo 'class="active dropdown"';}else{echo 'class="dropdown"';}?>>
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle" aria-hidden="true"></i> <?php echo $first_name; ?> <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url();?>main/profile"><?php echo $email; ?></a></li>
                        <li><a href="<?php echo site_url();?>main/changeuser">แก้ไขโปรไฟล์</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo base_url().'main/logout' ?>">ออกจากระบบ</a></li>
                      </ul>
                    </li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </div>
        </nav>
