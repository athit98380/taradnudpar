<div class="container">
<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <h2>ตั้งค่า</h2>
        <h5>ยินดีต้อนรับ <span><?php echo $first_name; ?></span>.</h5>
        <hr>
        <?php
        $fattr = array('class' => 'form-signin');
        echo form_open(site_url().'main/settings/', $fattr); 
        
        function tz_list() {
            $zones_array = array();
            $timestamp = time();
            foreach(timezone_identifiers_list() as $key => $zone) {
              date_default_timezone_set($zone);
              $zones_array[$key]['zone'] = $zone;
            }
            return $zones_array;
        }
        ?>
        
        <?php echo '<input type="hidden" name="id" value="'.$id.'">'; ?>
        <div class="form-group">
        <span>ชื่อไซต์</span>
          <?php echo form_input(array('name'=>'site_title', 'id'=> 'site_title', 'placeholder'=>'ชื่อไซต์', 'class'=>'form-control', 'value' => set_value('site_title', $site_title))); ?>
          <?php echo form_error('site_title');?>
        </div>
        <?php
        $result = $this->user_model->getAllSettings();
        $result->site_address=str_replace ("<br>", "\r\n", $result->site_address );
        ?>
        <div class="form-group">
        <span>ชื่อที่อยู่เบอร์โทร</span>
        <textarea name="site_address" id="site_address" rows="5" class="form-control" placeholder="ชื่อที่อยู่เบอร์โทร"><?php echo $result->site_address;?></textarea>
          <?php echo form_error('site_address');?>
        </div>
        <div class="form-group" style="display:none;">
        <span>เขตเวลา</span>
        <select name="timezone" id="timezone" class="form-control">
            <option value="<?php echo $timezonevalue; ?>"><?php echo $timezone; ?></option>
          <?php foreach(tz_list() as $t) { ?>
            <option value="<?php echo $t['zone']; ?>"> <?php echo $t['zone']; ?></option>
          <?php } ?>
        </select>
        </div>
        
        <div class="form-group" style="display:none">
        <span>ระบบป้องกันโปรแกรมอัตโนมัติ</span>
        <select name="recaptcha" id="recaptcha" class="form-control">
            <option value="no">ไม่</option>
            <option value="yes">ใช้</option>
        </select>
        </div>
        <div class="form-group">
        <span>รูปแบบ</span>
        <select name="theme" id="theme" class="form-control">
            <option value="cosmo.bootstrap.min.css" <?php if($result->theme=="cosmo.bootstrap.min.css"){echo "selected";}?>>Cosmo</option>
            <option value="darkly.bootstrap.min.css" <?php if($result->theme=="darkly.bootstrap.min.css"){echo "selected";}?>>Darkly</option>
            <option value="flatly.bootstrap.min.css" <?php if($result->theme=="flatly.bootstrap.min.css"){echo "selected";}?>>Flatly</option>
            <option value="journal.bootstrap.min.css" <?php if($result->theme=="journal.bootstrap.min.css"){echo "selected";}?>>Journal</option>
            <option value="lumen.bootstrap.min.css" <?php if($result->theme=="lumen.bootstrap.min.css"){echo "selected";}?>>Lumen</option>
            <option value="slate.bootstrap.min.css" <?php if($result->theme=="slate.bootstrap.min.css"){echo "selected";}?>>Slate</option>
            <option value="superhero.bootstrap.min.css" <?php if($result->theme=="superhero.bootstrap.min.css"){echo "selected";}?>>Superhero</option>
            <option value="yeti.bootstrap.min.css" <?php if($result->theme=="yeti.bootstrap.min.css"){echo "selected";}?>>Yeti</option>
        </select>
        </div>
        <?php echo form_submit(array('value'=>'บันทึก', 'name'=>'submit', 'class'=>'btn btn-primary btn-block')); ?>
        <?php echo form_close(); ?>
    </div>
</div>
</div>
<script type="text/javascript">
$('textarea').keypress(function(event) {
  if (event.which == 13) {
    event.preventDefault();
    this.value = this.value + "\n";
  }
});​
</script>