<div class="row">
<div class="col-lg-4 col-lg-offset-4"></div>
<div class="col-lg-4 col-lg-offset-4">
    <h2>
ลืมรหัสผ่าน</h2>
    <p>
โปรดป้อนที่อยู่อีเมลของคุณและเราจะส่งคำแนะนำเกี่ยวกับวิธีรีเซ็ตรหัสผ่านของคุณ</p>
    <br>
    <?php $fattr = array('class' => 'form-signin');
         echo form_open(site_url().'main/forgot/', $fattr); ?>
    <div class="form-group">
      <?php echo form_input(array(
          'name'=>'email', 
          'id'=> 'email', 
          'placeholder'=>'อีเมล', 
          'class'=>'form-control', 
          'value'=> set_value('email'))); ?>
      <?php echo form_error('email') ?>
    </div>
    <?php if($recaptcha == 'yes'){ ?>
    <div style="text-align:center;" class="form-group">
        <div style="display: inline-block;"><?php echo $this->recaptcha->render(); ?></div>
    </div>
    <?php
    }
    echo form_submit(array('value'=>'ตกลง', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
    <?php echo form_close(); ?>    
    <br>
    <p>สมัครสมาชิก? <a href="<?php echo site_url();?>main/login">เข้าสู่ระบบ</a></p>
</div>
<div class="col-lg-4 col-lg-offset-4"></div></div>