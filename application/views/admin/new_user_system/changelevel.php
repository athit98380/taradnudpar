<div class="col-lg-4 col-lg-offset-4">
    <h2>ระดับสมาชิก</h2>
    <h5>ยินดีต้อนรับ <span><?php echo $first_name; ?></span>, <br>โปรดเลือกผู้ใช้ระดับ.</h5>     
    <?php $fattr = array('class' => 'form-signin');
         echo form_open(site_url().'main/changelevel/', $fattr); ?>
    
    <div class="form-group">
        <select class="form-control" name="email" id="email">
            <?php
            foreach($groups as $row)
            { 
              echo '<option value="'.$row->email.'">'.$row->email.'</option>';
            }
            ?>
            </select>
    </div>

    <div class="form-group">
    <?php
        $dd_list = array(
                  '1'   => 'ผู้ดูแลระบบ',
                  '3'   => 'ผู้แก้ไข',
                );

        $dd_name = "level";
        echo form_dropdown($dd_name, $dd_list, set_value($dd_name),'class = "form-control" id="level"');
    ?>
    </div>
    <?php echo form_submit(array('value'=>'บันทึก', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
    <a href="<?php echo site_url().'main/users/';?>"><button type="button" class="btn btn-default btn-lg btn-block">ยกเลิก</button></a>
    <?php echo form_close(); ?>
</div>