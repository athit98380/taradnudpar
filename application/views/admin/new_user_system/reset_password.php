<div class="col-lg-8 col-lg-offset-8">
    <h2>เปลี่ยนรหัสผ่าน</h2>
    <h5>สวัสดีคุณ <span><?php echo $users->first_name; ?></span></h5>     
<?php 
    $fattr = array('class' => 'form-signin');
    echo form_open(site_url().'main/reset_password/'); ?>
    <div class="form-group">
      <?php echo form_password(array('name'=>'password', 'id'=> 'password', 'placeholder'=>'Password', 'class'=>'form-control', 'value' => set_value('password'))); ?>
      <?php echo form_error('password') ?>
    </div>
    <div class="form-group">
      <?php echo form_password(array('name'=>'passconf', 'id'=> 'passconf', 'placeholder'=>'Confirm Password', 'class'=>'form-control', 'value'=> set_value('passconf'))); ?>
      <?php echo form_error('passconf') ?>
    </div>
    <?php echo form_submit(array('value'=>'Reset Password', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
    <?php echo form_close(); ?>
</div>