</div> 
<?php $data_check_login = $this->session->userdata;
        if(!empty($data_check_login['status'])){ ?>
<div class="row footer_menu">
<?php
$min_date=date('Y-m')."-01";
$max_date=date('Y-m-d', strtotime('+1 month', strtotime($min_date)));

if ($data_check_login['id']==1) {
    for ($i=0; $i < 7; $i++) { 
        $this->db->order_by('product_id', 'desc');
        $this->db->where('product_status',0);
        $this->db->where('product_status_process',$i+1);
        $this->db->where('product_buy_date >',$min_date);
        $this->db->where('product_send_date <',$max_date);
        $data_footer=$this->db->get('products')->result_array();
        $data_footer_array[$i]=count($data_footer);
    }
    $r=1;
    for ($i=7; $i < 12; $i++) { 
        $this->db->order_by('product_id', 'desc');
        $this->db->where('product_status',0);
        $this->db->where('product_status_payment',$r);
        $this->db->where('product_buy_date >',$min_date);
        $this->db->where('product_send_date <',$max_date);
        $data_footer=$this->db->get('products')->result_array();
        $data_footer_array[$i]=count($data_footer);
        $r=$r+1;
    }
}else{
    for ($i=0; $i < 7; $i++) { 
        $this->db->order_by('product_id', 'desc');
        $this->db->where('product_status',0);
        $this->db->where('product_status_process',$i+1);
        $this->db->where('product_buy_date >',$min_date);
        $this->db->where('product_send_date <',$max_date);
        $this->db->where('users_id',$data_check_login['id']);
        $data_footer=$this->db->get('products')->result_array();
        $data_footer_array[$i]=count($data_footer);
    }
    $r=1;
    for ($i=7; $i < 12; $i++) { 
        $this->db->order_by('product_id', 'desc');
        $this->db->where('product_status',0);
        $this->db->where('product_status_payment',$r);
        $this->db->where('product_buy_date >',$min_date);
        $this->db->where('product_send_date <',$max_date);
        $this->db->where('users_id',$data_check_login['id']);
        $data_footer=$this->db->get('products')->result_array();
        $data_footer_array[$i]=count($data_footer);
        $r=$r+1;
    }
}
?>
        <div class="container">
            <table width="100%" class="table table-bnewcustomered" bnewcustomer="1">
                <tr class="navbar-inverse">
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-1");?>">ดำเนินการสั่งซื้อ</a></th>
                    <!-- <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-2");?>">ชำระเงินแล้ว</a></th> -->
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-3");?>">จัดส่งสินค้า</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-4");?>">ทวง</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-5");?>">ติดตาม</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-6");?>">ติดสิทธิ์</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-7");?>">ขอคืนสินค้า</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/shipping-8");?>">เสร็จสิ้น</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/payment-1");?>">รอชำระเงิน</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/payment-2");?>">ชำระเงินแล้ว</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/payment-3");?>">กำลังตรวจสอบ</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/payment-4");?>">การชำระเงินมีปัญหา</a></th>
                    <th class="text-center"><a href="<?php echo base_url("newcustomer/view/payment-5");?>">ชำระเงินเสร็จสิ้น</a></th>
                </tr>  
                <tr>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/shipping-1");?>"><?php echo $data_footer_array[0];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/shipping-2");?>"><?php echo $data_footer_array[1];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/shipping-3");?>"><?php echo $data_footer_array[2];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/shipping-4");?>"><?php echo $data_footer_array[3];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/shipping-5");?>"><?php echo $data_footer_array[4];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/shipping-6");?>"><?php echo $data_footer_array[5];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/shipping-7");?>"><?php echo $data_footer_array[6];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/payment-1");?>"><?php echo $data_footer_array[7];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/payment-2");?>"><?php echo $data_footer_array[8];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/payment-3");?>"><?php echo $data_footer_array[9];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/payment-4");?>"><?php echo $data_footer_array[10];?></td>
                    <td class="text-center btn-primary"><a class="footer_link" href="<?php echo base_url("newcustomer/view/payment-5");?>"><?php echo $data_footer_array[11];?></td>
                </tr>       
            </table>
        </div>
</div>

<?php        }
?>
     <footer>
        <div class="col-md-12" style="text-align:center;">
            <hr>
            Copyright - 2018 | <a href="https://www.workidea.co.th/">workidea.co.th</a>
        </div>
    </footer> 
    </div><!-- /container -->  
    
    <!-- /Load Js -->
    
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().'public/js/main.js' ?>"></script>
    <?php if (@$want_print!=1) {?>
    <?php }?>

<!--          <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  -->  
    </body>
</html>