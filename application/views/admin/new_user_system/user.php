    <div class="container">
        <h2>รายชื่อผู้ใช้
</h2>
        <table class="table table-hover table-bordered table-striped">
          <tr>
              <th>
                  ชื่อ
              </th>
              <th>
                  ชื่อผู้ใช้หรืออีเมล์
              </th>
              <th>
                  ชื่อย่อ
              </th>
              <th>
                  เข้าสู่ระบบครั้งล่าสุด
              </th>
              <th>
                  ระดับผู้ใช้งาน

              </th>
              <th>
                  สถานะ
              </th>
              <th colspan="2">
                  แก้ไข
              </th>
          </tr>
                <?php
                    foreach($groups as $row)
                    { 
                    if($row->role == 1){
                        $rolename = "ผู้ดูแลระบบ";
                    }elseif($row->role == 2){
                        $rolename = "ผู้เขียน";
                    }elseif($row->role == 3){
                        $rolename = "ผู้แก้ไข";
                    }elseif($row->role == 4){
                        $rolename = "ผู้ติดตาม";
                    }
                    
                    echo '<tr>';
                    echo '<td>'.$row->first_name.'</td>';
                    echo '<td>'.$row->email.'</td>';
                    echo '<td>'.$row->mini_name.'</td>';
                    echo '<td>'.$row->last_login.'</td>';
                    echo '<td>'.$rolename.'</td>';
                    echo '<td>'.$row->status.'</td>';
                    echo '<td><a href="'.site_url().'main/changelevel"><button type="button" class="btn btn-primary">เปลี่ยนระดับผู้ใช้งาน</button></a></td>';?>
                    <td><a onclick="return confirm('ยืนยันการลบอาจทำให้ข้อมูลผิดพลาดทั้งหมด');" href="<?php echo site_url().'main/deleteuser/'.$row->id;?>"><button type="button" class="btn btn-danger">ลบผู้ใช้งาน</button></a></td><?php
                    echo '</tr>';
                    }
                ?>
        </table>
    </div>