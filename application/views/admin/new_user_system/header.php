<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$result = $this->user_model->getAllSettings();
$theme = $result->theme;
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <meta name="robots" content="nofollow" />
        <!--CSS-->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
        <!--<link rel="stylesheet" href="<?php echo base_url().'public/css/font-awesome.min.css'; ?>">-->

         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
         <link rel="stylesheet" href="<?php echo base_url().'public/css/'.$theme; ?>"> 
         <link rel="stylesheet" href="<?php echo base_url().'public/css/main.css';?>"> 
        

<script type="text/javascript" src="<?php echo base_url('/public/date/');?>jquery/jquery-3.3.1.min.js" charset="UTF-8"></script>
<!-- <script type="text/javascript" src="<?php echo base_url('/public/date/');?>js/bootstrap.min.js"></script>    -->    
        <?php if (@$want_date_picker==1) {?>
<!--   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

 <!-- <link href="<?php echo base_url('/public/date/');?>bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"> 
 <link href="<?php echo base_url('/public/date/');?>bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen"> -->

<!--  Date Master
<script type="text/javascript" src="<?php echo base_url('/public/dateold/');?>/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url('/public/dateold/');?>/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('/public/dateold/');?>/js/bootstrap-datetimepicker.js" charset="UTF-8"></script> -->
        <?php }?>

        <?php if (@$want_print==1) {?>
        <link rel="stylesheet" href="<?php echo base_url().'public/css/print.css';?>"> 
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
        <script src="<?php echo base_url().'public/js/jquery.PrintArea.js';?>"></script>
        <?php }?>

        <!--link jquery ui css-->
        <!-- <link type="text/css" rel="stylesheet" href="<?php echo base_url('public/css/jquery-ui.css'); ?>" /> -->
        <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
    </head>
    <body>
