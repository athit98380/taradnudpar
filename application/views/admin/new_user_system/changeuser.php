<div class="col-lg-4 col-lg-offset-4">
    <h2>เปลี่ยนโปรไฟล์</h2>
    <h5>ยินดีต้อนรับ <span><?php echo $first_name; ?></span>.</h5>     
<?php 
    $fattr = array('class' => 'form-signin');
    echo form_open(site_url().'main/changeuser/', $fattr); ?>
    
    <div class="form-group">
      <?php echo form_input(array('name'=>'first_name', 'id'=> 'first_name', 'placeholder'=>'ชื่อ ', 'class'=>'form-control', 'value' => set_value('first_name', $groups->first_name))); ?>
      <?php echo form_error('first_name');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'lastname', 'id'=> 'lastname', 'placeholder'=>'นามสกุล', 'class'=>'form-control', 'value'=> set_value('lastname', $groups->last_name))); ?>
      <?php echo form_error('lastname');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'mini_name', 'id'=> 'mini_name', 'placeholder'=>'ชื่อย่อ 2 อักษร', 'class'=>'form-control', 'value'=> set_value('mini_name', $groups->mini_name))); ?>
      <?php echo form_error('mini_name');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'email', 'id'=> 'email', 'placeholder'=>'อีเมล', 'class'=>'form-control', 'value'=> set_value('email', $groups->email))); ?>
    </div>
    <div class="form-group">
      <?php echo form_password(array('name'=>'password', 'id'=> 'password', 'placeholder'=>'รหัสผ่าน', 'class'=>'form-control', 'value' => set_value('password'))); ?>
      <?php echo form_error('password') ?>
    </div>
    <div class="form-group">
      <?php echo form_password(array('name'=>'passconf', 'id'=> 'passconf', 'placeholder'=>'ยืนยันรหัสผ่าน ', 'class'=>'form-control', 'value'=> set_value('passconf'))); ?>
      <?php echo form_error('passconf') ?>
    </div>
    <?php echo form_submit(array('value'=>'เปลี่ยนแปลง', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
    <?php echo form_close(); ?>
</div>