<div class="container center-word">
    <div class="page-header">
      <h1>ส่งอีเมลสำเร็จแล้ว</h1>
    </div>
    <div class="alert alert-info" role="alert">
        โปรดตรวจสอบกล่องจดหมายของคุณ
        <br>
        Login? <a href="<?php echo base_url().'main/login' ?>">เข้าสู่ระบบ</a>
    </div>
</div>