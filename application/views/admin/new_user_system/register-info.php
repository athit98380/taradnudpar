<div class="container center-word">
    <div class="page-header">
      <h1>สมัครสมาชิกเสร็จสิ้น</h1>
    </div>
    <div class="alert alert-info" role="alert">
        สมัครสมาชิกเสร็จสิ้น
        <br>
        <a href="<?php echo base_url().'main/login' ?>">เข้าสู่ระบบ</a>
    </div>
</div>