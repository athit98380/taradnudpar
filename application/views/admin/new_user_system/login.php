<div class="col-lg-4 col-lg-offset-4">
    <h2>ยินดีต้อนรับ</h2>
    <h5>กรุณาเข้าสู่ระบบ</h5>
    <?php $fattr = array('class' => 'form-signin');
         echo form_open(site_url().'main/login/', $fattr); ?>
    <div class="form-group">
      <?php echo form_input(array(
          'name'=>'email', 
          'id'=> 'email', 
          'placeholder'=>'อีเมล', 
          'class'=>'form-control', 
          'value'=> set_value('email'))); ?>
      <?php echo form_error('email') ?>
    </div>
    <div class="form-group">
      <?php echo form_password(array(
          'name'=>'password', 
          'id'=> 'password', 
          'placeholder'=>'รหัสผ่าน', 
          'class'=>'form-control', 
          'value'=> set_value('password'))); ?>
      <?php echo form_error('password') ?>
    </div>
    <?php if($recaptcha == 'yes'){ ?>
    <div style="text-align:center;" class="form-group">
        <div style="display: inline-block;"><?php echo $this->recaptcha->render(); ?></div>
    </div>
    <?php
    }
    echo form_submit(array('value'=>'เข้าสู่ระบบ', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
    <?php echo form_close(); ?>
    <br>
    <p>ยังไม่ได้ลงทะเบียน? <a href="<?php echo site_url();?>main/register">
สมัครสมาชิก</a></p>
    <p>ลืมรหัสผ่านหรือไม่? <a href="<?php echo site_url();?>main/forgot">ลืมรหัสผ่าน</a></p>
</div>