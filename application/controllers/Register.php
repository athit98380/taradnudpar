<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Register extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    // Loader
    $this->load->library('form_validation');
    $this->load->model('Users_Model');
    $this->load->model('Shop_Model');
    $this->load->model('Image_Register_Model');
    $this->load->model('Menu_Model');
    $this->load->library('Theme');
    $this->load->model('Theme_Model');

    if (isset($this->session->uid)) {
      //redirect(base_url());
    }

  }

  public function index()
  {
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['title'] = "สมัครสมาชิก";
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
    $this->load->view('layout/main/header',$data);
    $this->load->view('Choice_Register',$data);
    $this->load->view('layout/main/footer',$data);
  }

  public function member($get_shop = null)
  {
    $input = $this->input->post(null,true);
    $data['menu'] = $this->Menu_Model->get_menu();
     $data['get_shop']=$get_shop;
    if (!empty($input)) {
      // ยังไม่ได้ required file image
      $this->form_validation->set_rules('email','Email','required|valid_email');
      $this->form_validation->set_rules('password','Password','required|min_length[8]');
      $this->form_validation->set_rules('ConfirmPassword','Confirm Password','required|min_length[8]|matches[password]');
      $this->form_validation->set_rules('fname','First Name','required|max_length[100]');
      $this->form_validation->set_rules('lname','First Name','required|max_length[100]');
      $this->form_validation->set_rules('tel','Tel','required|min_length[10]|max_length[10]');
      $this->form_validation->set_rules('numberid','Number ID','required|min_length[13]|max_length[13]');

      $this->form_validation->set_rules('checkbox2','Accept','required');
      if ($this->form_validation->run() == true) {
        // เช็คอีเมล์ซ้ำหรือเปล่า
        if ($this->Users_Model->record_count($input['email']) == false) {
        //if (!empty($input)) {
          $salt = $this->Users_Model->create_salt();
          $dataUser = array(
            'first_name' => $input['fname'],
            'last_name' => $input['lname'],
            'email' => $input['email'],
            'password' => md5($input['password']),
            'salt' => $salt,
            'user_tel' => $input['tel'],
            'user_number_id' => $input['numberid'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
          );
          $lastID = $this->Users_Model->create_user($dataUser);
          // Upload Image ID
          $upload_path = FCPATH . 'assets/uploads/numberid/'.$lastID;
          //echo $upload_path;echo "<br/>";
          if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
          $this->load->library('upload',[
            'upload_path' => $upload_path,
            'max_size' => 15000,
            'encrypt_name' => TRUE,
            'allowed_types' => 'jpg|jpeg|png'
          ]);
          // ถ้าอัพรูปบัตรประชาชนสำเร็จ
/*          if (!$this->upload->do_upload('imageid')) {
              $error = array('error' => $this->upload->display_errors());
              print_r($error);exit();
          }*/

          if ($this->upload->do_upload('imageid')) {
            $file = $this->upload->data();
            $this->Users_Model->set_imgageid(array('user_image' => $file['file_name']));

            //resize:
            if (!empty($lastID)) {
              $file_parts = getimagesize('assets/uploads/numberid/' .$lastID .'/'. $file['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/numberid/' .$lastID .'/'. $file['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config); 
                $this->image_lib->resize();
              }
            }

            // Shop
            $this->db->set('shop_create_on',date('Y-m-d H:i:s'));
            $this->db->set('shop_update_on',date('Y-m-d H:i:s'));
            $this->db->set('shop_expired_on', date('Y-m-d H:i:s',strtotime('+ 3 month')));
            //$shopid = $this->Shop_Model->create_shop($dataShop);
            // End Shop
            $this->db->set('image_register_create_on', 'now()', false);
            $this->db->set('image_register_update_on', 'now()', false);

            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","สมัครสมาชิกสำเร็จ","success");</script>'));

            if ($get_shop=="shop") {
              $data = array(
                'uid' => $lastID
              );
              $this->session->set_userdata($data);
              redirect(base_url('register/shop'));
            }
            // End image_register
          }else {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด Error รูปภาพขนาดใหญ่เกิน 5 Mb","warning");</script>'));
            $data2 = array(
                'admin_active' => 2
            );
            $this->Users_Model->admin_active($data2,$lastID);
          }
        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อีเมล์นี้ถูกใช้งานแล้ว กรุณาลองใหม่อีกครั้ง","warning");</script>'));
        }

      }
    }
    $data['title'] = "สมัครสมาชิก";
    if (!empty($data['page_detail']->menu_seo_title)) {
      $data['title'] = $data['page_detail']->menu_seo_title;
      $data['description'] = $data['page_detail']->menu_seo_description;
    }
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
    $this->load->view('layout/main/header',$data);
    $this->load->view('Member_Register',$data);
    $this->load->view('layout/main/footer',$data);
  }

  public function shop()
  {
    $input = $this->input->post(null,true);
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
    
    if (empty($this->session->uid)){
       redirect(base_url('register/member/shop'));
    }
    if (!empty($input)) {
      // ยังไม่ได้ required file image
/*      $this->form_validation->set_rules('email','Email','required|valid_email');
      $this->form_validation->set_rules('password','Password','required|min_length[8]');
      $this->form_validation->set_rules('ConfirmPassword','Confirm Password','required|min_length[8]|matches[password]');
      $this->form_validation->set_rules('fname','First Name','required|max_length[100]');
      $this->form_validation->set_rules('lname','First Name','required|max_length[100]');
      $this->form_validation->set_rules('tel','Tel','required|min_length[10]|max_length[10]');
      $this->form_validation->set_rules('numberid','Number ID','required|min_length[13]|max_length[13]');*/

      $this->form_validation->set_rules('shopname','Shop Name','required|max_length[200]');
      $this->form_validation->set_rules('shop_address','Shop Adress','required');
      $this->form_validation->set_rules('shopdetail','Shop Detail','required');
      $this->form_validation->set_rules('shop_guarantee','Shop Guarantee','required');
      $this->form_validation->set_rules('shop_guarantee_by','Shop Guarantee By','required|max_length[200]');
      $this->form_validation->set_rules('shop_guarantee_by_tel','Shop Guarantee By Tel','required|min_length[10]|max_length[10]');

      $this->form_validation->set_rules('checkbox1','accept','required');
      $this->form_validation->set_rules('shop_bank_name','Shop Bank Name','required|max_length[100]');
      $this->form_validation->set_rules('shop_bank_type','Shop Bank Type','required|max_length[15]');
      $this->form_validation->set_rules('shop_bank_sub','Shop Bank Sub','required|max_length[150]');
      $this->form_validation->set_rules('shop_bank_name_account','Shop Bank Account','required|max_length[200]');
      $this->form_validation->set_rules('shop_bank_number','Shop Bank Number','required|max_length[15]');

      $this->form_validation->set_rules('image_register_detail1','Image Detail','required');

      $this->form_validation->set_rules('checkbox2','Accept','required');
      if ($this->form_validation->run() == true) {
/*          $salt = $this->Users_Model->create_salt();
          $dataUser = array(
            'first_name' => $input['fname'],
            'last_name' => $input['lname'],
            'email' => $input['email'],
            'password' => md5($input['password']),
            'salt' => $salt,
            'user_tel' => $input['tel'],
            'user_number_id' => $input['numberid'],
            'ip_address' => $_SERVER['REMOTE_ADDR']
          );
          $lastID = $this->Users_Model->create_user($dataUser);*/

          // Upload Image ID
          $upload_path = FCPATH . 'assets/uploads/numberid/'.$this->session->uid;
          if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
          $this->load->library('upload',[
            'upload_path' => $upload_path,
            'max_size' => 15000,
            'encrypt_name' => TRUE,
            'allowed_types' => 'jpg|jpeg|png'
          ]);
            // Shop
            $dataShop = array(
              'shop_name' => $input['shopname'],
              'shop_address' => $input['shop_address'],
              'shop_detail' => $input['shopdetail'],
              'shop_guarantee' => $input['shop_guarantee'],
              'shop_guarantee_by' => $input['shop_guarantee_by'],
              'shop_guarantee_by_tel' => $input['shop_guarantee_by_tel'],
              'shop_bank_name' => $input['shop_bank_name'],
              'shop_bank_type' => $input['shop_bank_type'],
              'shop_bank_sub' => $input['shop_bank_sub'],
              'shop_bank_name_account' => $input['shop_bank_name_account'],
              'shop_bank_number' => $input['shop_bank_number'],
              'users_id' => $this->session->uid
            );
            $this->db->set('shop_create_on',date('Y-m-d H:i:s'));
            $this->db->set('shop_update_on',date('Y-m-d H:i:s'));
            $this->db->set('shop_expired_on', date('Y-m-d H:i:s',strtotime('+ 3 month')));
            $shopid = $this->Shop_Model->create_shop($dataShop);
            // End Shop

            // image_register
            $error_text_font_detail="";
            for ($i=1; $i < 2; $i++) { 
              if(empty($input['image_check_front'.$i])){
                if ($this->upload->do_upload('image_register_front'.$i)) {
                  $font[$i] = $this->upload->data();
                }else{
                  $font[$i]['file_name']="";
                  $error_text_font_detail.=$i." ";
                }
              }else{
                $font[$i]['file_name']=$input['image_check_front'.$i];
              }
            }
            if (!empty($error_text_font_detail)) {
              $error_text_font=" รูปด้านหน้า/หลัง ไม่สำเร็จ รูปภาพที่ ".$error_text_font_detail;
            }
            

            $error_text_back_detail="";
            for ($i=1; $i < 2; $i++) { 
              if(empty($input['image_check_back'.$i])){
                if ($this->upload->do_upload('image_register_back'.$i)) {
                  $back[$i] = $this->upload->data();
                }else{
                  $back[$i]['file_name']="";
                  $error_text_back_detail.=$i." ";
                }
              }else{
                $back[$i]['file_name']=$input['image_check_back'.$i];
              }
            }
            if (!empty($error_text_back_detail)) {
              $error_text_back=" รูปด้านข้าง/ก้น ไม่สำเร็จ รูปภาพที่ ".$error_text_back_detail; 
            }           

            for ($i=1; $i < 2; $i++) { 
              if(empty($input['image_check_front'.$i])){
                if (!empty($font[$i]['file_name'])) {
                  $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name']);
                  $file_parts_width = $file_parts[0];

                  if ($file_parts_width > 1920)
                  {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width']     = 1920;
                    $this->load->library('image_lib', $config); 
                    $this->image_lib->resize();
                  }
                }
              }
            }
            for ($i=1; $i < 2; $i++) { 
              if(empty($input['image_check_back'.$i])){
                if (!empty($back[$i]['file_name'])) {
                  $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $back[$i]['file_name']);
                  $file_parts_width = $file_parts[0];

                  if ($file_parts_width > 1920)
                  {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'assets/uploads/numberid/' .$this->session->uid .'/'. $back[$i]['file_name'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width']     = 1920;
                    $this->load->library('image_lib', $config); 
                    $this->image_lib->resize();
                  }
                }
              }
            }

            $data['image_register_shop'] = $this->Image_Register_Model->get_image($shopid); 
                $data['dataImage'] = array(
                  'image_register_front1' => @$font[1]['file_name'],
                  'image_register_back1' => @$back[1]['file_name'],
                  'image_register_detail1' => $input['image_register_detail1'],
                  'users_id' => $this->session->uid,
                  'shop_id' => $shopid
                );
                $this->db->set('image_register_create_on', 'now()', false);
                $this->db->set('image_register_update_on', 'now()', false);
              if (empty($data['image_register_shop'])) {
                $this->Image_Register_Model->create_item($data['dataImage']);
              }else{
                $this->Image_Register_Model->update_item($data['dataImage'],$shopid);
              }

            if ($error_text_font_detail=="" && $error_text_back_detail=="") {
              //$this->Image_Register_Model->create_item($data['dataImage']);
              $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","สมัครสมาชิกสำเร็จ","success");</script>'));
              redirect(base_url('login'));
            }else{
              $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","1อัพโหลดไฟล์'.@$error_text_font.' '.@$error_text_back.'","warning");</script>'));
            }
      }
    }

    $data['title'] = "สมัครสมาชิกร้านค้า";
    $this->load->view('layout/main/header',$data);
    $this->load->view('Register_Page',$data);
    $this->load->view('layout/main/footer',$data);
  }

}

 ?>
