<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Page extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Menu_Model');
    $this->load->model('Product_Model');
    $this->load->model('Product_Type_Model');
    $this->load->model('Blog_Model');
    $this->load->model('Shop_Model');
    $this->load->model('Static_Model');
    $this->load->model('Menu_Model');
    $this->load->model('Users_Model');
    $this->load->model('Theme_Model');
    $this->load->library('Theme');

    // สถิติ
    $this->Static_Model->useronline();
    if (!isset($_SESSION['online_id'])) {
      $this->Static_Model->update_item();
    }
    $_SESSION['online_id'] = SESSION_ID();
  }

  public function index($page='contact')
  {
    redirect(base_url('page/contact'));
  }

  public function contact()
  {
    $data['title']="ติดต่อเรา";
    $data['page_detail'] = $this->Menu_Model->get_menu_by_id(7);
    if (!empty($data['page_detail']->menu_seo_title)) {
      $data['title']=$data['page_detail']->menu_seo_title;
      $data['description']=$data['page_detail']->menu_seo_description;
    }
    $data['menu_seo_description']=$data['page_detail']->menu_seo_description;
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['recommend'] = $this->Shop_Model->get_recommend();
    $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
    $data['news'] = $this->Blog_Model->get_blog(10);
    $data['products'] = $this->Product_Model->get_limit_product(10);
    $data['static'] = $this->Static_Model->get_static();
    $data['countProduct'] = $this->Product_Model->getCountAllProduct();
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();

    $this->load->view('layout/main/header',$data);
    //$this->load->view('layout/main/left',$data);
    $this->load->view('page/view',$data);
    //$this->load->view('layout/main/right',$data);
    $this->load->view('layout/main/footer',$data);
  }

  public function usage()
  {
    $data['title']="ระเบียบการใช้งาน";
    $data['page_detail'] = $this->Menu_Model->get_menu_by_id(6);
    if (!empty($data['page_detail']->menu_seo_title)) {
      $data['title']=$data['page_detail']->menu_seo_title;
      $data['description']=$data['page_detail']->menu_seo_description;
    }
    $data['menu_seo_description']=$data['page_detail']->menu_seo_description;
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['recommend'] = $this->Shop_Model->get_recommend();
    $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
    $data['news'] = $this->Blog_Model->get_blog(10);
    $data['products'] = $this->Product_Model->get_limit_product(10);
    $data['static'] = $this->Static_Model->get_static();
    $data['countProduct'] = $this->Product_Model->getCountAllProduct();
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();

    $this->load->view('layout/main/header',$data);
    //$this->load->view('layout/main/left',$data);
    $this->load->view('page/view',$data);
    //$this->load->view('layout/main/right',$data);
    $this->load->view('layout/main/footer',$data);
  }
}

 ?>
