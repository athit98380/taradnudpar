<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Users extends CI_Controller
{

  function __construct()
  {
    parent:: __construct();
    $this->load->model('Shop_Model');
    $this->load->model('Product_Model');
    $this->load->model('Product_Type_Model');
    $this->load->model('Users_Model');
    $this->load->model('Renew_Model');
    $this->load->model('Menu_Model');
    $this->load->model('Blog_Model');
    $this->load->model('Image_Register_Model');
    $this->load->model('Theme_Model');
    $this->load->library('Datethai');
    $this->load->library('form_validation');
    $this->load->library('pagination');
    $this->load->library('Theme');

    if (empty($this->session->uid)) {
      redirect(base_url());
    }

  }

  // โปรไฟล์
  public function index()
  {
    $data['title'] = "โปรไฟล์";
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
    $data['users'] = $this->Users_Model->get_profile($this->session->uid);
    $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
    $this->load->view('layout/main/header',$data);
    $this->load->view('layout/users/left',$data);
    $this->load->view('users/home',$data);
    $this->load->view('layout/main/footer',$data);
  }

  // แก้ไข โปรไฟล์ Member
  public function edit_member()
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $data = array(
        'user_tel' => $input['user_tel'],
        'user_tel_add' => $input['user_tel_add']
      );
      $this->Shop_Model->update_member_profile($data,$this->session->uid);
      $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
      redirect(base_url('users'));
    }
  }

  // แก้ไข โปรไฟล์ post
  public function edit_profile()
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $data = array(
        'shop_name' => $input['shopname'],
        'shop_address' => $input['shop_address'],
        'shop_detail' => $input['shopdetail'],
        'shop_guarantee' => $input['shop_guarantee'],
        'shop_guarantee_by' => $input['shop_guarantee_by'],
        'shop_guarantee_by_tel' => $input['shop_guarantee_by_tel']
      );
      $this->Shop_Model->update_shop_profile($data,$input['shopid']);
      $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
      redirect(base_url('users'));
    }
  }

  // แก้ไข ข้อมูลธนาคาร post
  public function edit_bank()
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $data = array(
        'shop_bank_name' => $input['sbankname'],
        'shop_bank_type' => $input['sbanktype'],
        'shop_bank_sub' => $input['sbanksub'],
        'shop_bank_name_account' => $input['sbankaccount'],
        'shop_bank_number' => $input['sbanknumber']
      );
      $this->Shop_Model->update_shop_bank($data,$input['shopid']);
      $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
      redirect(base_url('users'));
    }
  }

  // สมัครร้านค้า
  public function register_shop()
  {
    $data['title'] = "สมัครร้านค้า";
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
    $data['users'] = $this->Users_Model->get_profile($this->session->uid);

    $input = $this->input->post(null,true);
    $data['menu'] = $this->Menu_Model->get_menu();
    if (!empty($input)) {
      // ยังไม่ได้ required file image
      $this->form_validation->set_rules('shopname','Shop Name','required|max_length[200]');
      $this->form_validation->set_rules('shop_address','Shop Adress','required');
      $this->form_validation->set_rules('shopdetail','Shop Detail','required');
      $this->form_validation->set_rules('shop_guarantee','Shop Guarantee','required');
      $this->form_validation->set_rules('shop_guarantee_by','Shop Guarantee By','required|max_length[200]');
      $this->form_validation->set_rules('shop_guarantee_by_tel','Shop Guarantee By Tel','required|min_length[10]|max_length[10]');

      $this->form_validation->set_rules('checkbox1','accept','required');
      $this->form_validation->set_rules('shop_bank_name','Shop Bank Name','required|max_length[100]');
      $this->form_validation->set_rules('shop_bank_type','Shop Bank Type','required|max_length[15]');
      $this->form_validation->set_rules('shop_bank_sub','Shop Bank Sub','required|max_length[150]');
      $this->form_validation->set_rules('shop_bank_name_account','Shop Bank Account','required|max_length[200]');
      $this->form_validation->set_rules('shop_bank_number','Shop Bank Number','required|max_length[15]');

      $this->form_validation->set_rules('image_register_detail1','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail2','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail3','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail4','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail5','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail6','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail7','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail8','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail9','Image Detail','required');
      $this->form_validation->set_rules('image_register_detail10','Image Detail','required');

      $this->form_validation->set_rules('checkbox2','Accept','required');
      if ($this->form_validation->run() == true) {
        $lastID = $this->session->uid;

          // Upload Image ID
        $upload_path = FCPATH . 'assets/uploads/numberid/'.$lastID;
        if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 15000000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png|gif'
        ]);

            // Shop
        $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
        if (empty($data['shop']->shop_id)) {
          $dataShop = array(
            'shop_name' => $input['shopname'],
            'shop_address' => $input['shop_address'],
            'shop_detail' => $input['shopdetail'],
            'shop_guarantee' => $input['shop_guarantee'],
            'shop_guarantee_by' => $input['shop_guarantee_by'],
            'shop_guarantee_by_tel' => $input['shop_guarantee_by_tel'],
            'shop_bank_name' => $input['shop_bank_name'],
            'shop_bank_type' => $input['shop_bank_type'],
            'shop_bank_sub' => $input['shop_bank_sub'],
            'shop_bank_name_account' => $input['shop_bank_name_account'],
            'shop_bank_number' => $input['shop_bank_number'],
            'users_id' => $lastID
          );
          $this->db->set('shop_create_on',date('Y-m-d H:i:s'));
          $this->db->set('shop_update_on',date('Y-m-d H:i:s'));
          $this->db->set('shop_expired_on', date('Y-m-d H:i:s',strtotime('+ 3 month')));
          $shopid = $this->Shop_Model->create_shop($dataShop);
          $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
        }else{
          $shopid = $data['shop']->shop_id;
        }

            // End Shop

            // image_register
        $error_text_font_detail="";
        for ($i=1; $i <=10; $i++) {
          if(empty($input['image_check_front'.$i])){
            if ($this->upload->do_upload('image_register_front'.$i)) {
              $font[$i] = $this->upload->data();
            }else{
              $font[$i]['file_name']="";
              $error_text_font_detail.=$i." ";
            }
          }else{
            $font[$i]['file_name']=$input['image_check_front'.$i];
          }
        }
        if (!empty($error_text_font_detail)) {
          $error_text_font=" รูปด้านหน้า/หลัง ไม่สำเร็จ รูปภาพที่ ".$error_text_font_detail;
        }


        $error_text_back_detail="";
        for ($i=1; $i <= 10; $i++) {
          if(empty($input['image_check_back'.$i])){
            if ($this->upload->do_upload('image_register_back'.$i)) {
              $back[$i] = $this->upload->data();
            }else{
              $back[$i]['file_name']="";
              $error_text_back_detail.=$i." ";
            }
          }else{
            $back[$i]['file_name']=$input['image_check_back'.$i];
          }
        }
        if (!empty($error_text_back_detail)) {
          $error_text_back=" รูปด้านข้าง/ก้น ไม่สำเร็จ รูปภาพที่ ".$error_text_back_detail;
        }

        for ($i=1; $i <= 10; $i++) {
          if(empty($input['image_check_front'.$i])){
            if (!empty($font[$i]['file_name'])) {
              $file_parts = getimagesize('assets/uploads/numberid/' .$lastID .'/'. $font[$i]['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/numberid/' .$lastID .'/'. $font[$i]['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
              }
            }
          }
        }
        for ($i=1; $i <= 10; $i++) {
          if(empty($input['image_check_back'.$i])){
            if (!empty($back[$i]['file_name'])) {
              $file_parts = getimagesize('assets/uploads/numberid/' .$lastID .'/'. $back[$i]['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/numberid/' .$lastID .'/'. $back[$i]['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
              }
            }
          }
        }

        $data['image_register_shop'] = $this->Image_Register_Model->get_image($shopid);
        $data['dataImage'] = array(
          'image_register_front1' => @$font[1]['file_name'],
          'image_register_front2' => @$font[2]['file_name'],
          'image_register_front3' => @$font[3]['file_name'],
          'image_register_front4' => @$font[4]['file_name'],
          'image_register_front5' => @$font[5]['file_name'],
          'image_register_front6' => @$font[6]['file_name'],
          'image_register_front7' => @$font[7]['file_name'],
          'image_register_front8' => @$font[8]['file_name'],
          'image_register_front9' => @$font[9]['file_name'],
          'image_register_front10' => @$font[10]['file_name'],
          'image_register_back1' => @$back[1]['file_name'],
          'image_register_back2' => @$back[2]['file_name'],
          'image_register_back3' => @$back[3]['file_name'],
          'image_register_back4' => @$back[4]['file_name'],
          'image_register_back5' => @$back[5]['file_name'],
          'image_register_back6' => @$back[6]['file_name'],
          'image_register_back7' => @$back[7]['file_name'],
          'image_register_back8' => @$back[8]['file_name'],
          'image_register_back9' => @$back[9]['file_name'],
          'image_register_back10' => @$back[10]['file_name'],
          'image_register_detail1' => $input['image_register_detail1'],
          'image_register_detail2' => $input['image_register_detail2'],
          'image_register_detail3' => $input['image_register_detail3'],
          'image_register_detail4' => $input['image_register_detail4'],
          'image_register_detail5' => $input['image_register_detail5'],
          'image_register_detail6' => $input['image_register_detail6'],
          'image_register_detail7' => $input['image_register_detail7'],
          'image_register_detail8' => $input['image_register_detail8'],
          'image_register_detail9' => $input['image_register_detail9'],
          'image_register_detail10' => $input['image_register_detai10'],
          'users_id' => $lastID,
          'shop_id' => $shopid
        );
        $this->db->set('image_register_create_on', 'now()', false);
        $this->db->set('image_register_update_on', 'now()', false);
        if (empty($data['image_register_shop'])) {
          $this->Image_Register_Model->create_item($data['dataImage']);
        }else{
          $this->Image_Register_Model->update_item($data['dataImage'],$shopid);
        }

            // End image_register
        if ($error_text_font_detail=="" && $error_text_back_detail=="") {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","สมัครสมาชิกสำเร็จ","success");</script>'));
          redirect(base_url('users'));
        }else{
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดไฟล์'.@$error_text_font.' '.@$error_text_back.'","warning");</script>'));
        }
      }
    }
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();

    $this->load->view('layout/main/header',$data);
    $this->load->view('users/register_shop',$data);
    $this->load->view('layout/main/footer',$data);
  }

  // รายการสินค้า
  public function productlist($product_online=null)
  {
    $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
    $config['base_url'] = base_url('users/productlist');
    $config['total_rows'] = $this->Product_Model->get_count_product($data['shop']->shop_id);
    $config['per_page'] = 16;
    $config['uri_segment'] = 3;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['attributes'] = array('class' => 'page-link');
    $config['first_link'] = 'First';
    $config['last_link'] = 'Last';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="prev">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
    $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $config['use_page_numbers'] = TRUE;
    $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;

    $this->pagination->initialize($config);
    $data['pagination'] = $this->pagination->create_links();

    $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
    if ($product_online==0) {
      $data['products'] = $this->Product_Model->get_pagination_product_idby_all_0($config["per_page"],$page,$data['shop']->shop_id);
    }else{
      $data['products'] = $this->Product_Model->get_pagination_product_idby_all_0($config["per_page"],$page,$data['shop']->shop_id);
    }
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();

    $data['title'] = "จัดการสินค้า";
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);

    //$data['products'] = $this->Product_Model->get_product(null,$this->session->uid);

    $this->load->view('layout/main/header',$data);
    $this->load->view('layout/users/left',$data);
    $this->load->view('users/productlist',$data);
    $this->load->view('layout/main/footer',$data);
  }

  // เพิ่มสินค้า
  public function add_product()
  {
    function resize_img($image,$max){
      $img_w = imagesx($image);
      $img_h = imagesy($image);

      $ratio = $max / $img_w;
      $new_w = $max;
      $new_h = $img_h * $ratio;
      if($new_h>$max){
        $ratio = $max / $img_h;
        $new_h = $max;
        $new_w = $img_w * $ratio;
      }
      if($image){
        $new_img = imagecreatetruecolor($new_w , $new_h);
        imagecopyresampled($new_img, $image, 0, 0, 0, 0, $new_w , $new_h, $img_w, $img_h);

        imagejpeg($new_img,$image,90);
      }
    }
    
    function crop_img($image,$max){

      $img_w = imagesx($image);
      $img_h = imagesy($image);

      if($img_h>$img_w){
        $ratio = $max / $img_w;
        $new_w = $max;
        $new_h = $img_h * $ratio;
        $diff = $img_h - $img_w;
        $x=round($diff/2)-$diff;
        $y=0;
      }   
      else{
        $ratio = $max / $img_h;
        $new_h = $max;
        $new_w = $img_w * $ratio;
        $diff =  $img_w - $img_h;
        $x=0;
        $y=round($diff/2)-$diff;
      }
      if($image){
        $new_img = imagecreatetruecolor($new_w , $new_h);
        imagecopyresampled($new_img, $image, 0, 0, 0, 0, $new_w , $new_h, $img_w, $img_h);

        $crop_img = imagecreatetruecolor($max , $max);
        imagecopyresampled($crop_img, $image, 0, 0, $x, $y, $max , $max, $max, $max);
        imagejpeg($crop_img,$image,90);
      }
    }
    $shop = $this->Shop_Model->get_shop($this->session->uid);
    $amount_add = $this->Shop_Model->get_amount_add($shop->shop_id);
    $product = $this->Product_Model->get_product_last($this->session->uid);
    // เช็คจำนวนครั้งลงต่อวัน
    if ($this->session->uid!=1) {
      if ($amount_add->shop_amount_added == 1) {
        if ($this->Product_Model->record_product($this->session->uid) == true) {
          if (date('Y-m-d',strtotime($product->product_create_on)) == date('Y-m-d')) {
            $this->session->set_flashdata(array('msg' => '<script>swal("จำกัดจำนวนการเพิ่มสินค้า","กรุณารอวันใหม่","warning");</script>'));
            redirect(base_url('users/productlist'));
          }
        }
      }elseif ($amount_add->shop_amount_added == 0) {
        $this->session->set_flashdata(array('msg' => '<script>swal("ไม่อณุญาติให้ลงสินค้า","กรุณารอแอดมินยืนยันร้าน","warning");</script>'));
        redirect(base_url('users/productlist'));
      }
    }

    $input = $this->input->post(null,false);
    if (!empty($input)) {
      $this->form_validation->set_rules('pname','Product Name','required');
      $this->form_validation->set_rules('pprice','Product Price','required|numeric');
      $this->form_validation->set_rules('ptype','Product Type','required');
      $this->form_validation->set_rules('pdetail','Product Detail','required');
      if ($this->form_validation->run() == true) {
        $upload_path = FCPATH . 'assets/uploads/numberid/'.$this->session->uid;
        if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 200000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png|gif'
        ]);

        $file_thumb="";
        $error_text_font_detail="";

        for ($i=1; $i < 11; $i++) { 
          if ($this->upload->do_upload('pimage'.$i)) {
            $file = $this->upload->data();
            $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name']);
            $file_parts_width = $file_parts[0];
            $file_thumb.=$file['file_name'].",";

            $img_path = 'assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name'];
            $img_thumb = 'assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name'];

            $config['image_library'] = 'gd2';
            $config['source_image'] = $img_path;
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = FALSE;
            $config['width'] = 200;
            $config['height'] = 200;                 

            $this->load->library('image_lib');
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            unset($config);

            $image_type = $file_parts['mime'];
            if ($image_type == 'image/jpeg') {
              $img = imagecreatefromjpeg($img_path);
            } elseif ($image_type == 'image/gif') {
              $img = imagecreatefromgif($img_path);
            } elseif ($image_type == 'image/png') {
              $img = imagecreatefrompng($img_path);
            }

            if(empty($input['image_check_front'.$i])){
              if ($this->upload->do_upload('pimage'.$i)) {               
                $font[$i] = $this->upload->data();
              }else{
                $font[$i]['file_name']="";
                $error_text_font_detail.=$i." ";
              }
            }else{
              $font[$i]['file_name']=$input['image_check_front'.$i];
            }

            if (!empty($error_text_font_detail)) {
              $error_text_font=" ไฟล์ที่ ".$error_text_font_detail;
            }

            if(empty($input['image_check_front'.$i])){
              if (!empty($font[$i]['file_name'])) {
                $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name']);
                $file_parts_width = $file_parts[0];

                if ($file_parts_width > 1920)
                {
                  $config['image_library'] = 'gd2';
                  $config['source_image'] = 'assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name'];
                  $config['maintain_ratio'] = TRUE;
                  $config['width']     = 1920;
                  $this->load->library('image_lib', $config); 
                  $this->image_lib->resize();
                }
              }
            }
          }else{
            $file_thumb.=",";
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ขนาดไฟล์ใหญ่เกิน 30 Mb","warning");</script>'));
          }
        } // for 

        if ($this->session->uid!=1) {
          $data = array(
            'product_no' => 'SP'.time().rand(1,3),
            'product_name' => $input['pname'],
            'product_price' => $input['pprice'],
            'product_type_id' => $input['ptype'],
            'product_detail' => $input['pdetail'],
            'product_status_for_sale' => $input['pstatusfs'],
            'product_image' => @$file_thumb,
            'product_images' => @$font[1]['file_name'].','.@$font[2]['file_name'].','.@$font[3]['file_name'].','.@$font[4]['file_name'].','.@$font[5]['file_name'].','
            .@$font[6]['file_name'].','.@$font[7]['file_name'].','.@$font[8]['file_name'].','.@$font[9]['file_name'].','.@$font[10]['file_name'].',',
            'product_status' => '1',
            'users_id' => $this->session->uid,
            'shop_id' => $shop->shop_id
          );
        }else{
          $data = array(
            'product_no' => 'SP'.time().rand(1,3),
            'product_name' => $input['pname'],
            'product_price' => $input['pprice'],
            'product_type_id' => $input['ptype'],
            'product_detail' => $input['pdetail'],
            'product_status_for_sale' => $input['pstatusfs'],
            'product_image' => @$file_thumb,
            'product_images' => @$font[1]['file_name'].','.@$font[2]['file_name'].','.@$font[3]['file_name'].','.@$font[4]['file_name'].','.@$font[5]['file_name'].','
            .@$font[6]['file_name'].','.@$font[7]['file_name'].','.@$font[8]['file_name'].','.@$font[9]['file_name'].','.@$font[10]['file_name'].',',
            'product_status' => '0',
            'users_id' => $this->session->uid,
            'shop_id' => $shop->shop_id
          );
        }
        $this->Product_Model->create_product($data);
        $last_product=$this->Product_Model->get_last_product($this->session->uid);

        if (empty($error_text_font_detail)) {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","บันทึกข้อมูลสำเร็จ","success");</script>'));
          redirect(base_url('users/productlist'));
        }else{
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดไฟล์ไม่สำเร็จ'.@$error_text_font.' หรือไม่จำเป็นต้องครบ 10 ภาพ","warning");</script>'));
          redirect(base_url('users/edit_product/'.$last_product[0]->product_id));
        }
      }else{
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ข้อมูลที่จำเป็นไม่ครบถ้วน","warning");</script>'));
      }
    }else{
     // $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ข้อมูลไม่ครบถ้วน","warning");</script>'));
    }
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
    $data['title'] = "เพิ่มสินค้า";
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
    $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
    $data['ptype'] = $this->Product_Type_Model->get_all_product_type();
    $this->load->view('layout/main/header',$data);
    $this->load->view('layout/users/left',$data);
    $this->load->view('users/add_product',$data);
    $this->load->view('layout/main/footer',$data);
  }


  // แก้ไข สินค้า
  public function edit_product($id = null)
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $this->form_validation->set_rules('pname','Product Name','required');
      $this->form_validation->set_rules('pprice','Product Price','required');
      $this->form_validation->set_rules('ptype','Product Type','required');
      $this->form_validation->set_rules('pdetail','Product Detail','required');
      // $this->form_validation->set_rules('psize','Product Size','required');

      if ($this->form_validation->run() == true) {
        $upload_path = FCPATH . 'assets/uploads/numberid/'.$this->session->uid;
        if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 15000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png|gif'
        ]);

        $error_text_font_detail="";
        $file_thumb="";
        for ($i=1; $i <= 10; $i++) {
          if ($this->upload->do_upload('pimage'.$i)) {
            $file = $this->upload->data();
            $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name']);
            $file_parts_width = $file_parts[0];
            $file_thumb.=$file['file_name'].",";

            $img_path = 'assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name'];
            $img_thumb = 'assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name'];

            $config['image_library'] = 'gd2';
            $config['source_image'] = $img_path;
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = FALSE;

              //$img = imagecreatefromjpeg($img_path);

            $image_type = $file_parts['mime'];
            if ($image_type == 'image/jpeg') {
              $img = imagecreatefromjpeg($img_path);
            } elseif ($image_type == 'image/gif') {
              $img = imagecreatefromgif($img_path);
            } elseif ($image_type == 'image/png') {
              $img = imagecreatefrompng($img_path);
            }
            $_width = imagesx($img);
            $_height = imagesy($img);

            $img_type = '';
            $thumb_size = 200;

            if ($_width > $_height)
            {
                  // wide image
              $config['width'] = intval(($_width / $_height) * $thumb_size);
              if ($config['width'] % 2 != 0)
              {
                $config['width']++;
              }
              $config['height'] = $thumb_size;
              $img_type = 'wide';
            }
            else if ($_width < $_height)
            {
                  // landscape image
              $config['width'] = $thumb_size;
              $config['height'] = intval(($_height / $_width) * $thumb_size);
              if ($config['height'] % 2 != 0)
              {
                $config['height']++;
              }
              $img_type = 'landscape';
            }
            else
            {
                  // square image
              $config['width'] = $thumb_size;
              $config['height'] = $thumb_size;
              $img_type = 'square';
            }

            $this->load->library('image_lib');
            $this->image_lib->initialize($config);
            $this->image_lib->resize();

              // reconfigure the image lib for cropping
            $conf_new = array(
              'image_library' => 'gd2',
              'source_image' => $img_thumb,
              'create_thumb' => FALSE,
              'maintain_ratio' => FALSE,
              'width' => $thumb_size,
              'height' => $thumb_size
            );

            if ($img_type == 'wide')
            {
              $conf_new['x_axis'] = ($config['width'] - $thumb_size) / 2 ;
              $conf_new['y_axis'] = 0;
            }
            else if($img_type == 'landscape')
            {
              $conf_new['x_axis'] = 0;
              $conf_new['y_axis'] = ($config['height'] - $thumb_size) / 2;
            }
            else
            {
              $conf_new['x_axis'] = 0;
              $conf_new['y_axis'] = 0;
            }

            $this->image_lib->initialize($conf_new);

            $this->image_lib->crop();

            $this->image_lib->clear();
            unset($config);

            if ($this->upload->do_upload('pimage'.$i)) {
              $font[$i] = $this->upload->data();
            }else{
              if(empty($input['image_check_front'.$i])){
                $font[$i]['file_name']="";
                $error_text_font_detail.=$i." ";
              }else{
                $font[$i]['file_name']=$input['image_check_front'.$i];
              }
            }

            if (!empty($error_text_font_detail)) {
              $error_text_font=" ไฟล์ที่ ".$error_text_font_detail;
            }

            if(empty($input['image_check_front'.$i])){
              if (!empty($font[$i]['file_name'])) {
                $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name']);
                $file_parts_width = $file_parts[0];

                if ($file_parts_width > 1920)
                {
                  $config['image_library'] = 'gd2';
                  $config['source_image'] = 'assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name'];
                  $config['maintain_ratio'] = TRUE;
                  $config['width']     = 1920;
                  $this->load->library('image_lib', $config);
                  $this->image_lib->resize();
                }
              }
            }

          }else{
            if(empty($input['file_thumb'.$i])){
              $file_thumb.=",";
            }else{
              $file_thumb.=$input['file_thumb'.$i].",";
            }

            if(empty($input['image_check_front'.$i])){
              $font[$i]['file_name']="";
              $error_text_font_detail.=$i." ";
            }else{
              $font[$i]['file_name']=$input['image_check_front'.$i];
            }
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ขนาดไฟล์ใหญ่เกิน 15 Mb","warning");</script>'));
          }
        } // for

/*            $upload_path = FCPATH . 'assets/uploads/numberid/'.$this->session->uid;
            if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
            $this->load->library('upload',[
              'upload_path' => $upload_path,
              'max_size' => 15000,
              'encrypt_name' => TRUE,
              'allowed_types' => 'jpg|jpeg|png|gif'
            ]);


            for ($i=1; $i < 6; $i++) {
              if ($this->upload->do_upload('pimage'.$i)) {
                  $font[$i] = $this->upload->data();
              }else{
                if(empty($input['image_check_front'.$i])){
                  $font[$i]['file_name']="";
                  $error_text_font_detail.=$i." ";
                }else{
                  $font[$i]['file_name']=$input['image_check_front'.$i];
                }
              }
            }
            if (!empty($error_text_font_detail)) {
              $error_text_font=" ไฟล์ที่ ".$error_text_font_detail;
            }

            for ($i=1; $i < 6; $i++) {
              if(empty($input['image_check_front'.$i])){
                if (!empty($font[$i]['file_name'])) {
                  $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name']);
                  $file_parts_width = $file_parts[0];

                  if ($file_parts_width > 1920)
                  {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'assets/uploads/numberid/' .$this->session->uid .'/'. $font[$i]['file_name'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width']     = 1920;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                  }
                }
              }
            }
          if (empty($file_thumb)) {
            $file_thumb=$input['file_thumb'];
          }*/

          if ($this->session->uid!=1) {
            $data = array(
              'product_name' => $input['pname'],
              'product_price' => $input['pprice'],
              'product_type_id' => $input['ptype'],
              'product_detail' => $input['pdetail'],
              'product_status_for_sale' => $input['pstatusfs'],
              'product_image' => @$file_thumb,
              'product_images' => @$font[1]['file_name'].','.@$font[2]['file_name'].','.@$font[3]['file_name'].','.@$font[4]['file_name'].','.@$font[5]['file_name'].','
              .@$font[6]['file_name'].','.@$font[7]['file_name'].','.@$font[8]['file_name'].','.@$font[9]['file_name'].','.@$font[10]['file_name'].',',
              'product_status' => '1'
            );
          }else{
            $data = array(
              'product_name' => $input['pname'],
              'product_price' => $input['pprice'],
              'product_type_id' => $input['ptype'],
              'product_detail' => $input['pdetail'],
              'product_status_for_sale' => $input['pstatusfs'],
              'product_image' => @$file_thumb,
              'product_images' => @$font[1]['file_name'].','.@$font[2]['file_name'].','.@$font[3]['file_name'].','.@$font[4]['file_name'].','.@$font[5]['file_name'].','
              .@$font[6]['file_name'].','.@$font[7]['file_name'].','.@$font[8]['file_name'].','.@$font[9]['file_name'].','.@$font[10]['file_name'].',',
              'product_status' => '0'
            );
          }

          $this->Product_Model->update_product($data,$id);
          $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","รอแอดมินยืนยัน","success");</script>'));
          redirect(base_url('users/edit_product/'.$id));
        }

      }
      $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
      $data['title'] = "แก้ไขสินค้า";
      $data['menu'] = $this->Menu_Model->get_menu();
      $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
      $data['ptype'] = $this->Product_Type_Model->get_all_product_type();
      $data['product'] = $this->Product_Model->get_product($id,$this->session->uid);
      $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);

      if ($data['product']->users_id != $this->session->uid) {
        redirect(base_url());
      }

      $this->load->view('layout/main/header',$data);
      $this->load->view('layout/users/left',$data);
      $this->load->view('users/edit_product',$data);
      $this->load->view('layout/main/footer',$data);
    }

  // ลบสินค้า (รอแอดยืนยัน)
    public function delete_product($id = null)
    {
      $this->Product_Model->delete_product($id);
      $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","รอแอดมินยืนยัน","success");</script>'));
      redirect(base_url('users/productlist'));
    }

  // ต่ออายุ
    public function renew()
    {
      $input = $this->input->post(null,true);
      if (!empty($input)) {
      // Upload Image ID
        $upload_path = FCPATH . 'assets/uploads/numberid/'.$this->session->uid;
        if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 1000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png'
        ]);
        if ($this->upload->do_upload('file')) {
          $file = $this->upload->data();
          $shop = $this->Shop_Model->get_shop($this->session->uid);
          $data = array(
            'renew_confirm' => $file['file_name'],
            'renew_confirm_by' => $input['year'],
            'shop_id' => $shop->shop_id,
            'users_id' => $this->session->uid
          );
          if ($this->Renew_Model->add($data) != false) {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดสำเร็จ","success");</script>'));
            redirect(base_url('users/renew'));
          }else {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","คุณได้อัพโหลดไปแล้ว","warning");</script>'));
            redirect(base_url('users/renew'));
          }
        }
      }
      $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
      $data['title'] = "ต่ออายุ";
      $data['menu'] = $this->Menu_Model->get_menu();
      $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
      $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
      $this->load->view('layout/main/header',$data);
      $this->load->view('layout/users/left',$data);
      $this->load->view('users/renew',$data);
      $this->load->view('layout/main/footer',$data);
    }

  // ประกาศจากร้าน
    public function notify($sid = null)
    {
      $input = $this->input->post(null,true);
      if (!empty($input)) {
        $this->form_validation->set_rules('blog','Message','required');
        if ($this->form_validation->run() == true) {
          // ถ้าเขียนประกาศแล้วให้อัพเดต
          $data = array(
            'shop_blog' => $input['blog']
          );
          $this->Shop_Model->update_shop_profile($data,$sid);
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
          redirect(base_url('users/notify'));
        }
      }

      $data['title'] = "ประกาศจากร้าน";
      $data['menu'] = $this->Menu_Model->get_menu();
      $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
    //$data['blog'] = $this->Blog_Model->get_data_by_uid($this->session->uid);
      $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
      $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
      $this->load->view('layout/main/header',$data);
      $this->load->view('layout/users/left',$data);
      $this->load->view('users/notify',$data);
      $this->load->view('layout/main/footer',$data);
    }

    public function photoactive()
    {
      $data['title'] = "รูปยืนยันร้าน";
      $data['menu'] = $this->Menu_Model->get_menu();
      $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
      $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
      $data['photo'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
      $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
      $this->load->view('layout/main/header',$data);
      $this->load->view('layout/users/left',$data);
      $this->load->view('users/photoactive',$data);
      $this->load->view('layout/main/footer',$data);
    }

    public function change_image_profile($uid=null)
    {
      $upload_path = FCPATH . 'assets/uploads/numberid/'.$uid;
      if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
      $this->load->library('upload',[
        'upload_path' => $upload_path,
        'max_size' => 15000,
        'encrypt_name' => TRUE,
        'allowed_types' => 'jpg|jpeg|png|gif'
      ]);
      if ($this->upload->do_upload('pimage')) {
        $file = $this->upload->data();
        $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $file['file_name']);
        $file_parts_width = $file_parts[0];
        $file_thumb=$file['file_name'];

        $img_path = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];
        $img_thumb = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];

        $config['image_library'] = 'gd2';
        $config['source_image'] = $img_path;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;

            //$img = imagecreatefromjpeg($img_path);
        $image_type = $file_parts['mime'];
        if ($image_type == 'image/jpeg') {
          $img = imagecreatefromjpeg($img_path);
        } elseif ($image_type == 'image/gif') {
          $img = imagecreatefromgif($img_path);
        } elseif ($image_type == 'image/png') {
          $img = imagecreatefrompng($img_path);
        }
        $_width = imagesx($img);
        $_height = imagesy($img);

        $img_type = '';
        $thumb_size = 200;

        if ($_width > $_height)
        {
                // wide image
          $config['width'] = intval(($_width / $_height) * $thumb_size);
          if ($config['width'] % 2 != 0)
          {
            $config['width']++;
          }
          $config['height'] = $thumb_size;
          $img_type = 'wide';
        }
        else if ($_width < $_height)
        {
                // landscape image
          $config['width'] = $thumb_size;
          $config['height'] = intval(($_height / $_width) * $thumb_size);
          if ($config['height'] % 2 != 0)
          {
            $config['height']++;
          }
          $img_type = 'landscape';
        }
        else
        {
                // square image
          $config['width'] = $thumb_size;
          $config['height'] = $thumb_size;
          $img_type = 'square';
        }

        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        $this->image_lib->resize();

            // reconfigure the image lib for cropping
        $conf_new = array(
          'image_library' => 'gd2',
          'source_image' => $img_thumb,
          'create_thumb' => FALSE,
          'maintain_ratio' => FALSE,
          'width' => $thumb_size,
          'height' => $thumb_size
        );

        if ($img_type == 'wide')
        {
          $conf_new['x_axis'] = ($config['width'] - $thumb_size) / 2 ;
          $conf_new['y_axis'] = 0;
        }
        else if($img_type == 'landscape')
        {
          $conf_new['x_axis'] = 0;
          $conf_new['y_axis'] = ($config['height'] - $thumb_size) / 2;
        }
        else
        {
          $conf_new['x_axis'] = 0;
          $conf_new['y_axis'] = 0;
        }

        $this->image_lib->initialize($conf_new);

        $this->image_lib->crop();

        $this->image_lib->clear();
        unset($config);
      }
      if ($this->upload->do_upload('pimage')) {
        $file = $this->upload->data();
        $file_parts = getimagesize('assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name']);
        $file_parts_width = $file_parts[0];

        if ($file_parts_width > 1920)
        {
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/uploads/numberid/' .$this->session->uid .'/'. $file['file_name'];
          $config['maintain_ratio'] = TRUE;
          $config['width']     = 1920;
          $this->load->library('image_lib', $config);
          $this->image_lib->resize();
        }
        $data = array(
          'user_image' => $file_thumb,
          'user_image_profile' => $file['file_name']
        );

        if ($this->Users_Model->change_image_profile($data,$this->session->uid) != false) {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดสำเร็จ","success");</script>'));
          redirect(base_url('users'));
        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
          redirect(base_url('users'));
        }

      }
      $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ขนาดไฟล์ใหญ่เกิน 15 Mb","warning");</script>'));
      redirect(base_url('users'));
    }

    public function change_shop_banner($uid=null,$sid=null)
    {
      $upload_path = FCPATH . 'assets/uploads/numberid/'.$uid;
      if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
      $this->load->library('upload',[
        'upload_path' => $upload_path,
        'max_size' => 15000,
        'encrypt_name' => TRUE,
        'allowed_types' => 'jpg|jpeg|png'
      ]);
      if ($this->upload->do_upload('shop_banner')) {
        $file = $this->upload->data();
        $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $file['file_name']);
        $file_parts_width = $file_parts[0];

        if ($file_parts_width > 1920)
        {
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];
          $config['maintain_ratio'] = TRUE;
          $config['width']     = 1920;
          $this->load->library('image_lib', $config);
          $this->image_lib->resize();
        }
        $data = array(
          'shop_banner' => $file['file_name']
        );

        if ($this->Shop_Model->change_shop_banner($data,$sid) != false) {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดสำเร็จ","success");</script>'));
          redirect(base_url('users'));
        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
          redirect(base_url('users'));
        }

      }
      $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ขนาดไฟล์ใหญ่เกิน 15 Mb","warning");</script>'));
      redirect(base_url('users'));
    }

    public function change_shop_qrcode($uid=null,$sid=null)
    {
      $input = $this->input->post(null,true);
      $upload_path = FCPATH . 'assets/uploads/numberid/'.$uid;
      if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
      $this->load->library('upload',[
        'upload_path' => $upload_path,
        'max_size' => 5000,
        'encrypt_name' => TRUE,
        'allowed_types' => 'jpg|jpeg|png'
      ]);
      if ($this->upload->do_upload('shop_qrcode')) {
        $file = $this->upload->data();
        $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $file['file_name']);
        $file_parts_width = $file_parts[0];

        if ($file_parts_width > 300)
        {
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];
          $config['maintain_ratio'] = TRUE;
          $config['width']     = 300;
          $this->load->library('image_lib', $config);
          $this->image_lib->resize();
        }
        if (!empty($input['shop_qrcode_link'])) {
          $data = array(
            'shop_qrcode' => $file['file_name'],
            'shop_qrcode_link' => $input['shop_qrcode_link']
          );
        }else{
          $data = array(
            'shop_qrcode' => $file['file_name']
          );
        }
        if ($this->Shop_Model->change_shop_qrcode($data,$sid) != false) {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดสำเร็จ","success");</script>'));
          redirect(base_url('users'));
        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
          redirect(base_url('users'));
        }

      }else{
        if (!empty($input['shop_qrcode_old'])) {
          if (!empty($input['shop_qrcode_link'])) {
            $data = array(
              'shop_qrcode' => $input['shop_qrcode_old'],
              'shop_qrcode_link' => $input['shop_qrcode_link']
            );
          }else{
            $data = array(
              'shop_qrcode' => $input['shop_qrcode_old']
            );
          }
        }
        if ($this->Shop_Model->change_shop_qrcode($data,$sid) != false) {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","บันทึกข้อมูลสำเร็จ","success");</script>'));
          redirect(base_url('users'));
        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
          redirect(base_url('users'));
        }
      }
        //$this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ขนาดไฟล์ใหญ่เกิน 5 Mb","warning");</script>'));
        //redirect(base_url('users'));
    }

  }
