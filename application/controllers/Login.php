<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Login extends CI_Controller{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Menu_Model');
    $this->load->model('Theme_Model');
    $this->load->library('Theme');
  }
  
  // View Login Page
  function index()
  {

    if ($this->session->userdata('uid')) {
      redirect(base_url());
    }
    $data['menu'] = $this->Menu_Model->get_menu();
    $this->load->library('form_validation');
    $this->load->model('Users_Model');
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      // Validation
      $this->form_validation->set_rules('email','Email','trim|required|valid_email');
      $this->form_validation->set_rules('password','Password','trim|required|min_length[8]');
      if ($this->form_validation->run()) {
        // $this->Users_Model->record_count($input['email'],$input['password']);
        //   $result = $this->Users_Model->fetch_user($input['email'],$input['password']);
        if ($this->Users_Model->record_count($input['email'],$input['password']) == true) {
          $result = $this->Users_Model->fetch_user($input['email'],$input['password']);

          $data = array(
            'uid' => $result->id,
            'first_name' => $result->first_name,
            'last_name' => $result->last_name,
            'email' => $result->email,
            'user_type' => $result->user_type
          );
          $this->session->set_userdata($data);

          // Set Last Login
          $this->Users_Model->set_log_login($this->session->userdata('email'));
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เข้าสู่ระบบสำเร็จ","success");</script>'));
          if ($this->session->user_type == 'admin') {
            redirect(base_url('admin'));
          }else {
            redirect(base_url('main'));
          }

        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง!","warning");</script>'));
          redirect(base_url('login'));
        }
      }
    }
    $data['theme_for_design'] = $this->Theme_Model->get_theme_design();

    $data['title'] = "เข้าสู่ระบบ";
    $this->load->view('layout/main/header',$data);
    $this->load->view('Login_Page',$data);
    $this->load->view('layout/main/footer',$data);
  }

  public function logout()
  {
    $data = array('uid','first_name','last_name','email','user_type');
    $this->session->unset_userdata($data);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ออกจากระบบสำเร็จ!","warning");</script>'));
    redirect(base_url('login'));
  }

}

 ?>
