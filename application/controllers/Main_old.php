<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 /*
 * หน้าแรก
 *
 */
class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Product_Model');
		$this->load->model('Product_Type_Model');
		$this->load->model('Blog_Model');
		$this->load->model('Shop_Model');
		$this->load->model('Static_Model');
		$this->load->model('Menu_Model');
		$this->load->model('Users_Model');
		$this->load->model('Theme_Model');
		$this->load->library('Datethai');
		$this->load->library('pagination');
		$this->load->library('Theme');

		// สถิติ
		$this->Static_Model->useronline();
		if (!isset($_SESSION['online_id'])) {
			$this->Static_Model->update_item();
		}
		$_SESSION['online_id'] = SESSION_ID();

	}

	public function index()
	{
		$config['base_url'] = base_url('main/index');
		$config['total_rows'] = $this->Product_Model->get_count_product_amulettop();
		$config['per_page'] = 120;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['attributes'] = array('class' => 'page-link');
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['use_page_numbers'] = TRUE;
		$page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['title'] = "หน้าแรก";
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['page_detail'] = $this->Menu_Model->get_menu_by_id(1);
	    if (!empty($data['page_detail']->menu_seo_title)) {
	      $data['title']=$data['page_detail']->menu_seo_title;
	      $data['description']=$data['page_detail']->menu_seo_description;
	    }
		$data['recommend'] = $this->Shop_Model->get_recommend();
    	$data['product_type'] = $this->Product_Type_Model->get_all_product_type();
		$data['news'] = $this->Blog_Model->get_blog(10);
		//$data['products'] = $this->Product_Model->get_limit_product(12);
		//$data['products_admin'] = $this->Product_Model->get_limit_product_by_admin(12);
		$data['products'] = $this->Product_Model->get_pagination_product_amulet($config["per_page"],$page);
		$data['static'] = $this->Static_Model->get_static();
		$data['countProduct'] = $this->Product_Model->getCountAllProduct();
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('main/search');
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/home',$data);
		$this->load->view('layout/main/right',$data);
		$this->load->view('layout/main/footer',$data);
	}

	public function ajaxmodal()
	{
		$input = $this->input->get(null,true);
		$data = $this->Product_Model->get_product_join($input['id']);
		echo json_encode($data);
	}

	// รายการอัพเดต (พระเครื่อง)
	public function amulet()
	{
		$config['base_url'] = base_url('main/amulet');
		$config['total_rows'] = $this->Product_Model->get_count_product_amulettop();
		$config['per_page'] = 30;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['attributes'] = array('class' => 'page-link');
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['use_page_numbers'] = TRUE;
		$page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['title'] = "รายการอัพเดต";
		$data['page_detail'] = $this->Menu_Model->get_menu_by_id(2);
	    if (!empty($data['page_detail']->menu_seo_title)) {
	      $data['title']=$data['page_detail']->menu_seo_title;
	      $data['description']=$data['page_detail']->menu_seo_description;
	    }
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['recommend'] = $this->Shop_Model->get_recommend();
		$data['product_type'] = $this->Product_Type_Model->get_all_product_type();
		$data['news'] = $this->Blog_Model->get_blog(10);
		$data['products'] = $this->Product_Model->get_pagination_product_amulet($config["per_page"],$page);
		$data['static'] = $this->Static_Model->get_static();
		$data['countProduct'] = $this->Product_Model->getCountAllProduct();
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('main/search');
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/amulet',$data);
		$this->load->view('layout/main/right',$data);
		$this->load->view('layout/main/footer',$data);
	}

	// รายการพระเด่น
	public function amulettop()
	{
		$config['base_url'] = base_url('main/amulettop');
		$config['total_rows'] = $this->Product_Model->get_count_product_amulettop();
		$config['per_page'] = 18;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['attributes'] = array('class' => 'page-link');
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['use_page_numbers'] = TRUE;
		$page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['title'] = "รายการพระเด่น";
		$data['page_detail'] = $this->Menu_Model->get_menu_by_id(3);
	    if (!empty($data['page_detail']->menu_seo_title)) {
	      $data['title']=$data['page_detail']->menu_seo_title;
	      $data['description']=$data['page_detail']->menu_seo_description;
	    }
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['recommend'] = $this->Shop_Model->get_recommend();
		$data['product_type'] = $this->Product_Type_Model->get_all_product_type();
		$data['news'] = $this->Blog_Model->get_blog(10);
		$data['products'] = $this->Product_Model->get_pagination_product_orderby($config["per_page"],$page);
		$data['static'] = $this->Static_Model->get_static();
		$data['countProduct'] = $this->Product_Model->getCountAllProduct();
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('main/search');
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/amulet',$data);
		$this->load->view('layout/main/right',$data);
		$this->load->view('layout/main/footer',$data);
	}

	// หมวดหมู่ พระเครื่อง
	public function category($id = null)
	{
		$config['base_url'] = base_url("main/category/{$id}");
		$config['total_rows'] = $this->Product_Model->get_count_product($id);
		$config['per_page'] = 30;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['attributes'] = array('class' => 'page-link');
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['use_page_numbers'] = TRUE;
		$page = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) * $config["per_page"] : 0;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['title'] = "หมวดหมู่ พระเครื่อง";
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['recommend'] = $this->Shop_Model->get_recommend();
		$data['product_type'] = $this->Product_Type_Model->get_all_product_type();
		$data['news'] = $this->Blog_Model->get_blog(10);
		$data['products'] = $this->Product_Model->get_pagination_product_group($config["per_page"],$page,$id);
		$data['static'] = $this->Static_Model->get_static();
		$data['countProduct'] = $this->Product_Model->getCountAllProduct();
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/amulet',$data);
		$this->load->view('layout/main/right',$data);
		$this->load->view('layout/main/footer',$data);
	}

	// รายการข่าว
	public function news()
	{
	$config['base_url'] = base_url('main/news');
    $config['total_rows'] = $this->Blog_Model->getCountAll();
    $config['per_page'] = 10;
    $config['uri_segment'] = 3;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['attributes'] = array('class' => 'page-link');
    $config['first_link'] = 'First';
    $config['last_link'] = 'Last';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="prev">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
    $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
    $data['recommend'] = $this->Shop_Model->get_recommend();
    $data['static'] = $this->Static_Model->get_static();
	$data['countProduct'] = $this->Product_Model->getCountAllProduct();

    $config['use_page_numbers'] = TRUE;
    $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
    $this->pagination->initialize($config);
    $data['pagination'] = $this->pagination->create_links();

		$data['title'] = "ข่าวประชาสัมพันธ์";
		$data['page_detail'] = $this->Menu_Model->get_menu_by_id(5);
	    if (!empty($data['page_detail']->menu_seo_title)) {
	      $data['title']=$data['page_detail']->menu_seo_title;
	      $data['description']=$data['page_detail']->menu_seo_description;
	    }
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['news'] = $this->Blog_Model->get_pagination($config["per_page"],$page);
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/news',$data);
		$this->load->view('layout/main/footer',$data);
	}

	// รายละเอียดข่าว
	public function news_detail($id = null)
	{
		$data['title'] = "ข่าวประชาสัมพันธ์";
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['newsDetail'] = $this->Blog_Model->get_blog_by_id($id);
		$data['product_type'] = $this->Product_Type_Model->get_all_product_type();
		$data['recommend'] = $this->Shop_Model->get_recommend();
		$data['static'] = $this->Static_Model->get_static();
		$data['countProduct'] = $this->Product_Model->getCountAllProduct();
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/blog',$data);
		$this->load->view('layout/main/footer',$data);

	}

	public function shoplist()
	{
		$data['title'] = 'ร้านพระมาตรฐาน';
		$data['page_detail'] = $this->Menu_Model->get_menu_by_id(4);
	    if (!empty($data['page_detail']->menu_seo_title)) {
	      $data['title']=$data['page_detail']->menu_seo_title;
	      $data['description']=$data['page_detail']->menu_seo_description;
	    }
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['recommend'] = $this->Shop_Model->get_recommend();
		$data['product_type'] = $this->Product_Type_Model->get_all_product_type();
		$data['news'] = $this->Blog_Model->get_blog(10);
		$data['static'] = $this->Static_Model->get_static();
		$data['countProduct'] = $this->Product_Model->getCountAllProduct();
		$data['shoplist'] = $this->Shop_Model->get_all_shop_show();
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('main/search');
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/shoplist',$data);
		$this->load->view('layout/main/right',$data);
		$this->load->view('layout/main/footer',$data);
	}

	public function shop($uid = null)
	{
		if (!isset($_SESSION['view_shop'])) {
			$this->Shop_Model->update_view($uid);
		}
		$_SESSION['view_shop'] = SESSION_ID();
		$this->Shop_Model->update_view($uid);

		$shop = $this->Shop_Model->get_shop_id($uid);
		//print_r($shop);
		$data['title'] = $shop->shop_name;
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['shop'] = $shop;
		$data['blog'] = $this->Blog_Model->get_data_by_uid($uid);
		$data['products'] = $this->Product_Model->get_product_by_shop_id($uid);
		$data['user'] = $this->Users_Model->get_user($shop->users_id);

		if ($uid==1) {
			$data['news'] = $this->Blog_Model->get_blog();
		}

		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('main/search');
		$this->load->view('main/shop',$data);
		$this->load->view('layout/main/footer',$data);
	}

	public function search()
	{
		$input = $this->input->post(null,true);
		$data['search']=$input['search'];
		$data['shoplist'] = $this->Shop_Model->search_shop($input['search']);
		$data['products'] = $this->Product_Model->search_product($input['search']);
		$data['title'] = "ผลการค้นหาบนตลาดนัดพระเครื่องออนไลน์";
		$data['menu'] = $this->Menu_Model->get_menu();
		$data['recommend'] = $this->Shop_Model->get_recommend();
		$data['product_type'] = $this->Product_Type_Model->get_all_product_type();
		$data['news'] = $this->Blog_Model->get_blog(10);
		$data['static'] = $this->Static_Model->get_static();
		$data['countProduct'] = $this->Product_Model->getCountAllProduct();
		$data['theme_for_design'] = $this->Theme_Model->get_theme_design();

		$this->load->view('layout/main/header',$data);
		$this->load->view('main/search',$data);
		$this->load->view('layout/main/left',$data);
		$this->load->view('main/search_detail',$data);
		$this->load->view('layout/main/right',$data);		
		$this->load->view('layout/main/footer',$data);
	}
}

?>
