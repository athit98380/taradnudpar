
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

    public $status;
    public $roles;

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'user_model', TRUE);
        $this->load->library('form_validation');
        //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->status = $this->config->item('status');
        $this->roles = $this->config->item('roles');
        $this->load->library('userlevel');
        $this->load->model('Product_Model');
        $this->load->model('Product_Type_Model');
        $this->load->model('Blog_Model');
        $this->load->model('Shop_Model');
        $this->load->model('Static_Model');
        $this->load->model('Menu_Model');
        $this->load->model('Users_Model');
        $this->load->model('Theme_Model');
        $this->load->model('Image_Register_Model');
        $this->load->library('Datethai');
        $this->load->library('pagination');
        $this->load->library('Theme');
    }

    //index dasboard
    public function index()
    {
        $config['base_url'] = base_url('main/index');
        $config['total_rows'] = $this->Product_Model->get_count_product_amulettop();
        $config['per_page'] = 120;
        $config['uri_segment'] = 3;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = array('class' => 'page-link');
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['use_page_numbers'] = TRUE;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = "หน้าแรก";
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['page_detail'] = $this->Menu_Model->get_menu_by_id(1);
        if (!empty($data['page_detail']->menu_seo_title)) {
            $data['title'] = $data['page_detail']->menu_seo_title;
            $data['description'] = $data['page_detail']->menu_seo_description;
        }
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['news'] = $this->Blog_Model->get_blog(10);
        //$data['products'] = $this->Product_Model->get_limit_product(12);
        //$data['products_admin'] = $this->Product_Model->get_limit_product_by_admin(12);
        $data['products'] = $this->Product_Model->get_pagination_product_amulet($config["per_page"], $page);
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->getCountAllProduct();
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();

        //user data from session
        $this->load->view('layout/main/header', $data);
        $this->load->view('main/search');
        $this->load->view('layout/main/left', $data);
        $this->load->view('main/home', $data);
        $this->load->view('layout/main/right', $data);
        $this->load->view('layout/main/footer', $data);
    }

    public function ajaxmodal()
    {
        $input = $this->input->get(null, true);
        $data = $this->Product_Model->get_product_join($input['id']);
        echo json_encode($data);
    }

    function show_approved()
    {
        //user data from session
        $data = $this->session->userdata;
        if (empty($data)) {
            redirect(site_url() . 'main/login/');
        }
        //print_r($data);
        //check user level
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $dataLevel = $this->userlevel->checkLevel($data['role']);
        //check user level

        $data['title'] = "Dashboard Admin";
        $data['active_menu'] = "main";

        $data['groups'] = $this->user_model->getUserData();
        $data['get_approved'] = $this->user_model->get_approved();

        if (empty($this->session->userdata['email'])) {
            redirect(site_url() . 'main/login/');
        } else {
            $this->load->view('header', $data);
            $this->load->view('navbar', $data);
            $this->load->view('container');
            $this->load->view('show_approved', $data);
            $this->load->view('footer');
        }
    }

    function approved($user_id = null)
    {
        $params = array('status' => 'approved',);
        $this->user_model->approved($user_id, $params);
        redirect(site_url() . 'main/show_approved/');
    }

    public function checkLoginUser()
    {
        //user data from session
        $data = $this->session->userdata;
        if (empty($data)) {
            redirect(site_url() . 'main/login');
        }
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เข้าสู่ระบบสำเร็จ","success");</script>'));

        $this->load->library('user_agent');
        $browser = $this->agent->browser();
        $os = $this->agent->platform();
        $getip = $this->input->ip_address();

        $result = $this->user_model->getAllSettings();
        $stLe = $result->site_title;
        //$tz = $result->timezone;

        $now = new DateTime();
        //$now->setTimezone(new DateTimezone($tz));
        $dTod =  $now->format('Y-m-d');
        $dTim =  $now->format('H:i:s');

        $this->load->helper('cookie');
        $keyid = rand(1, 9000);
        $scSh = sha1($keyid);
        $neMSC = md5($data['email']);
        $setLogin = array(
            'name'   => $neMSC,
            'value'  => $scSh,
            'expire' => strtotime("+2 year"),
        );
        $getAccess = get_cookie($neMSC);

        if (!$getAccess && $setLogin["name"] == $neMSC) {
            $this->load->library('email');
            $this->load->library('sendmail');
            $bUrl = base_url();
            $message = $this->sendmail->secureMail($data['first_name'], $data['last_name'], $data['email'], $dTod, $dTim, $stLe, $browser, $os, $getip, $bUrl);
            $to_email = $data['email'];
            $this->email->from($this->config->item('register'), 'New sign-in! from ' . $browser . '');
            $this->email->to($to_email);
            $this->email->subject('New sign-in! from ' . $browser . '');
            $this->email->message($message);
            $this->email->set_mailtype("html");
            $this->email->send();

            $this->input->set_cookie($setLogin, TRUE);
            redirect(site_url() . 'main/');
        } else {
            $this->input->set_cookie($setLogin, TRUE);
            redirect(site_url() . 'main/');
        }
    }

    public function settings()
    {
        $data = $this->session->userdata;
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $dataLevel = $this->userlevel->checkLevel($data['role']);
        //check user level

        $data['title'] = "Settings";
        $data['active_menu'] = "setting";
        $this->form_validation->set_rules('site_title', 'Site Title', 'required');
        $this->form_validation->set_rules('site_address', 'ชื่อที่อยู่เบอร์โทร', 'required');
        $this->form_validation->set_rules('timezone', 'Timezone', 'required');
        $this->form_validation->set_rules('recaptcha', 'Recaptcha', 'required');
        $this->form_validation->set_rules('theme', 'Theme', 'required');

        $result = $this->user_model->getAllSettings();
        $data['id'] = $result->id;
        $data['site_title'] = $result->site_title;
        $data['site_address'] = $result->site_address;
        $data['timezone'] = $result->timezone;
        if (!empty($data['timezone'])) {
            $data['timezonevalue'] = $result->timezone;
            $data['timezone'] = $result->timezone;
        } else {
            $data['timezonevalue'] = "";
            $data['timezone'] = "Select a time zone";
        }

        if ($dataLevel == "is_admin") {
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', $data);
                $this->load->view('navbar', $data);
                $this->load->view('container');
                $this->load->view('settings', $data);
                $this->load->view('footer');
            } else {
                $post = $this->input->post(NULL, TRUE);
                $cleanPost = $this->security->xss_clean($post);
                $cleanPost['id'] = $this->input->post('id');
                $cleanPost['site_title'] = $this->input->post('site_title');
                $cleanPost['site_address'] = str_replace("\r\n", "<br>", $this->input->post('site_address'));
                $cleanPost['timezone'] = $this->input->post('timezone');
                $cleanPost['recaptcha'] = $this->input->post('recaptcha');
                $cleanPost['theme'] = $this->input->post('theme');

                if (!$this->user_model->settings($cleanPost)) {
                    $this->session->set_flashdata('flash_message', 'There was a problem updating your data!');
                } else {
                    $this->session->set_flashdata('success_message', 'Your data has been updated.');
                }
                redirect(site_url() . 'main/settings/');
            }
        }
    }

    //user list
    public function users()
    {
        $data = $this->session->userdata;
        $data['title'] = "User List";
        $data['active_menu'] = "setting";
        $data['groups'] = $this->user_model->getUserData();

        //check user level
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $dataLevel = $this->userlevel->checkLevel($data['role']);
        //check user level

        //check is admin or not
        if ($dataLevel == "is_admin") {
            $this->load->view('header', $data);
            $this->load->view('navbar', $data);
            $this->load->view('container');
            $this->load->view('user', $data);
            $this->load->view('footer');
        } else {
            redirect(site_url() . 'main/');
        }
    }

    //change level user
    public function changelevel()
    {
        $data = $this->session->userdata;
        //check user level
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $dataLevel = $this->userlevel->checkLevel($data['role']);
        //check user level

        $data['title'] = "Change Level Admin";
        $data['active_menu'] = "setting";
        $data['groups'] = $this->user_model->getUserData();

        //check is admin or not
        if ($dataLevel == "is_admin") {

            $this->form_validation->set_rules('email', 'Your Email', 'required');
            $this->form_validation->set_rules('level', 'User Level', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', $data);
                $this->load->view('navbar', $data);
                $this->load->view('container');
                $this->load->view('changelevel', $data);
                $this->load->view('footer');
            } else {
                $cleanPost['email'] = $this->input->post('email');
                $cleanPost['level'] = $this->input->post('level');
                if (!$this->user_model->updateUserLevel($cleanPost)) {
                    $this->session->set_flashdata('flash_message', 'There was a problem updating the level user');
                } else {
                    $this->session->set_flashdata('success_message', 'The level user has been updated.');
                }
                redirect(site_url() . 'main/changelevel');
            }
        } else {
            redirect(site_url() . 'main/');
        }
    }

    //ban or unban user
    public function banuser()
    {
        $data = $this->session->userdata;
        //check user level
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $dataLevel = $this->userlevel->checkLevel($data['role']);
        //check user level

        $data['title'] = "Ban User";
        $data['active_menu'] = "setting";
        $data['groups'] = $this->user_model->getUserData();

        //check is admin or not
        if ($dataLevel == "is_admin") {

            $this->form_validation->set_rules('email', 'Your Email', 'required');
            $this->form_validation->set_rules('banuser', 'Ban or Unban', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', $data);
                $this->load->view('navbar', $data);
                $this->load->view('container');
                $this->load->view('banuser', $data);
                $this->load->view('footer');
            } else {
                $post = $this->input->post(NULL, TRUE);
                $cleanPost = $this->security->xss_clean($post);
                $cleanPost['email'] = $this->input->post('email');
                $cleanPost['banuser'] = $this->input->post('banuser');
                if (!$this->user_model->updateUserban($cleanPost)) {
                    $this->session->set_flashdata('flash_message', 'There was a problem updating');
                } else {
                    $this->session->set_flashdata('success_message', 'The status user has been updated.');
                }
                redirect(site_url() . 'main/banuser');
            }
        } else {
            redirect(site_url() . 'main/');
        }
    }

    //edit user
    public function changeuser()
    {
        $data = $this->session->userdata;
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }

        $dataInfo = array(
            'firstName' => $data['first_name'],
            'id' => $data['id'],
        );

        $data['title'] = "Change Password";
        $data['active_menu'] = "profile";
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('mini_name', 'Mini name', 'required|max_length[2]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        $data['groups'] = $this->user_model->getUserInfo($dataInfo['id']);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', $data);
            $this->load->view('navbar', $data);
            $this->load->view('container');
            $this->load->view('changeuser', $data);
            $this->load->view('footer');
        } else {
            $this->load->library('password');
            $post = $this->input->post(NULL, TRUE);
            $cleanPost = $this->security->xss_clean($post);
            $hashed = $this->password->create_hash($cleanPost['password']);
            $cleanPost['password'] = $hashed;
            $cleanPost['user_id'] = $dataInfo['id'];
            $cleanPost['email'] = $this->input->post('email');
            $cleanPost['firstname'] = $this->input->post('firstname');
            $cleanPost['lastname'] = $this->input->post('lastname');
            $cleanPost['mini_name'] = $this->input->post('mini_name');
            unset($cleanPost['passconf']);
            if (!$this->user_model->updateProfile($cleanPost)) {
                $this->session->set_flashdata('flash_message', 'There was a problem updating your profile');
            } else {
                $this->session->set_flashdata('success_message', 'Your profile has been updated.');
            }
            redirect(site_url() . 'main/');
        }
    }

    //open profile and gravatar user
    public function profile()
    {
        $data = $this->session->userdata;
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }

        $data['title'] = "Profile";
        $data['active_menu'] = "profile";
        $this->load->view('header', $data);
        $this->load->view('navbar', $data);
        $this->load->view('container');
        $this->load->view('profile', $data);
        $this->load->view('footer');
    }

    //delete user
    public function deleteuser($id)
    {
        $data = $this->session->userdata;
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $dataLevel = $this->userlevel->checkLevel($data['role']);
        //check user level

        //check is admin or not
        if ($dataLevel == "is_admin") {
            $this->user_model->deleteUser($id);
            if ($this->user_model->deleteUser($id) == FALSE) {
                $this->session->set_flashdata('flash_message', 'Error, cant delete the user!');
            } else {
                $this->session->set_flashdata('success_message', 'Delete user was successful.');
            }
            redirect(site_url() . 'main/users/');
        } else {
            redirect(site_url() . 'main/');
        }
    }

    //add new user from backend
    public function adduser()
    {
        $data = $this->session->userdata;
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $data['active_menu'] = "setting";
        //check user level
        if (empty($data['role'])) {
            redirect(site_url() . 'main/login/');
        }
        $dataLevel = $this->userlevel->checkLevel($data['role']);
        //check user level

        //check is admin or not
        if ($dataLevel == "is_admin") {
            $this->form_validation->set_rules('firstname', 'First Name', 'required');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('role', 'role', 'required');
            $this->form_validation->set_rules('mini_name', 'Mini name', 'required|max_length[2]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

            $data['title'] = "Add User";
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', $data);
                $this->load->view('navbar');
                $this->load->view('container');
                $this->load->view('adduser', $data);
                $this->load->view('footer');
            } else {
                if ($this->user_model->isDuplicate($this->input->post('email'))) {
                    $this->session->set_flashdata('flash_message', 'User email already exists');
                    redirect(site_url() . 'main/adduser');
                } else {
                    $this->load->library('password');
                    $post = $this->input->post(NULL, TRUE);
                    $cleanPost = $this->security->xss_clean($post);
                    $hashed = $this->password->create_hash($cleanPost['password']);
                    $cleanPost['email'] = $this->input->post('email');
                    $cleanPost['role'] = $this->input->post('role');
                    $cleanPost['firstname'] = $this->input->post('firstname');
                    $cleanPost['lastname'] = $this->input->post('lastname');
                    $cleanPost['mini_name'] = $this->input->post('mini_name');
                    $cleanPost['banned_users'] = 'unban';
                    $cleanPost['password'] = $hashed;
                    unset($cleanPost['passconf']);

                    //insert to database
                    if (!$this->user_model->addUser($cleanPost)) {
                        $this->session->set_flashdata('flash_message', 'There was a problem add new user');
                    } else {
                        $this->session->set_flashdata('success_message', 'New user has been added.');
                    }
                    redirect(site_url() . 'main/users/');
                }
            }
        } else {
            redirect(site_url() . 'main/');
        }
    }

    //register new user from frontend
    public function register()
    {
        $data['title'] = "Register to Admin";
        $this->load->library('curl');
        $this->load->library('recaptcha');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('mini_name', 'Mini name', 'required|max_length[2]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        $result = $this->user_model->getAllSettings();
        $sTl = $result->site_title;
        $data['recaptcha'] = $result->recaptcha;
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', $data);
            $this->load->view('container');
            $this->load->view('register');
            $this->load->view('footer');
        } else {
            if ($this->user_model->isDuplicate($this->input->post('email'))) {
                $this->session->set_flashdata('flash_message', 'User email already exists');
                redirect(site_url() . 'main/register');
            } else {
                $post = $this->input->post(NULL, TRUE);
                $clean = $this->security->xss_clean($post);

                if ($data['recaptcha'] == 'yes') {
                    //recaptcha
                    $recaptchaResponse = $this->input->post('g-recaptcha-response');
                    $userIp = $_SERVER['REMOTE_ADDR'];
                    $key = $this->recaptcha->secret;
                    $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $key . "&response=" . $recaptchaResponse . "&remoteip=" . $userIp; //link
                    $response = $this->curl->simple_get($url);
                    $status = json_decode($response, true);

                    //recaptcha check
                    if ($status['success']) {
                        //insert to database
                        $this->load->library('password');
                        $post = $this->input->post(NULL, TRUE);
                        $cleanPost = $this->security->xss_clean($post);
                        $hashed = $this->password->create_hash($cleanPost['password']);
                        $cleanPost['email'] = $this->input->post('email');
                        $cleanPost['role'] = 2;
                        $cleanPost['firstname'] = $this->input->post('firstname');
                        $cleanPost['lastname'] = $this->input->post('lastname');
                        $cleanPost['banned_users'] = 'unban';
                        $cleanPost['password'] = $hashed;
                        unset($cleanPost['passconf']);

                        //insert to database
                        //if(!$this->user_model->addUser($cleanPost)){
                        if (!$this->user_model->insertUser($cleanPost)) {
                            $this->session->set_flashdata('flash_message', 'There was a problem add new user');
                        } else {
                            $this->session->set_flashdata('success_message', 'New user has been added.');
                        }

                        //$id = $this->user_model->insertUser($clean);
                        //$token = $this->user_model->insertToken($id);

                        //generate token
                        //$qstring = $this->base64url_encode($token);
                        //$url = site_url() . 'main/complete/token/' . $qstring;
                        //$link = '<a href="' . $url . '">' . $url . '</a>';

                        redirect(site_url() . 'main/successregister/');

                        /*                        $this->load->library('email');
                        $this->load->library('sendmail');

                        $message = $this->sendmail->sendRegister($this->input->post('lastname'),$this->input->post('email'),$link, $sTl);
                        $to_email = $this->input->post('email');
                        $this->email->from($this->config->item('register'), 'Set Password ' . $this->input->post('firstname') .' '. $this->input->post('lastname'));
                        //from sender, title email
                        $this->email->to($to_email);
                        $this->email->subject('Set Password Login');
                        $this->email->message($message);
                        $this->email->set_mailtype("html");

                        //Sending mail
                        if($this->email->send()){
                        redirect(site_url().'main/successregister/');
                        }else{
                        $this->session->set_flashdata('flash_message', 'There was a problem sending an email.');
                        exit;
                    }*/
                    } else {
                        //recaptcha failed
                        $this->session->set_flashdata('flash_message', 'Error...! Google Recaptcha UnSuccessful!');
                        redirect(site_url() . 'main/register/');
                        exit;
                    }
                } else {
                    //insert to database
                    $this->load->library('password');
                    $post = $this->input->post(NULL, TRUE);
                    $cleanPost = $this->security->xss_clean($post);
                    $hashed = $this->password->create_hash($cleanPost['password']);
                    $cleanPost['email'] = $this->input->post('email');
                    $cleanPost['role'] = 2;
                    $cleanPost['firstname'] = $this->input->post('firstname');
                    $cleanPost['lastname'] = $this->input->post('lastname');
                    $cleanPost['banned_users'] = 'unban';
                    $cleanPost['password'] = $hashed;
                    unset($cleanPost['passconf']);

                    //insert to database
                    //if(!$this->user_model->addUser($cleanPost)){
                    if (!$this->user_model->insertUser($cleanPost)) {
                        $this->session->set_flashdata('flash_message', 'There was a problem add new user');
                    } else {
                        $this->session->set_flashdata('success_message', 'New user has been added.');
                    }
                    //$id = $this->user_model->insertUser($clean);
                    //$token = $this->user_model->insertToken($id);

                    //generate token
                    //$qstring = $this->base64url_encode($token);
                    //$url = site_url() . 'main/complete/token/' . $qstring;
                    //$link = '<a href="' . $url . '">' . $url . '</a>';

                    redirect(site_url() . 'main/successregister/');
                    /*                    $this->load->library('email');
                    $this->load->library('sendmail');

                    $message = $this->sendmail->sendRegister($this->input->post('lastname'),$this->input->post('email'),$link,$sTl);
                    $to_email = $this->input->post('email');
                    $this->email->from($this->config->item('register'), 'Set Password ' . $this->input->post('firstname') .' '. $this->input->post('lastname')); //from sender, title email
                    $this->email->to($to_email);
                    $this->email->subject('Set Password Login');
                    $this->email->message($message);
                    $this->email->set_mailtype("html");

                    //Sending mail
                    if($this->email->send()){
                    redirect(site_url().'main/successregister/');
                    }else{
                    $this->session->set_flashdata('flash_message', 'There was a problem sending an email.');
                    exit;
                }*/
                }
            }
        }
    }

    //if success new user register
    public function successregister()
    {
        $data['title'] = "Success Register";
        $this->load->view('header', $data);
        $this->load->view('container');
        $this->load->view('register-info');
        $this->load->view('footer');
    }

    //if success after set password
    public function successresetpassword()
    {
        $data['title'] = "Success Reset Password";
        $this->load->view('header', $data);
        $this->load->view('container');
        $this->load->view('reset-pass-info');
        $this->load->view('footer');
    }

    protected function _islocal()
    {
        return strpos($_SERVER['HTTP_HOST'], 'local');
    }

    //check if complate after add new user
    public function complete()
    {
        $token = base64_decode($this->uri->segment(4));
        $cleanToken = $this->security->xss_clean($token);

        $user_info = $this->user_model->isTokenValid($cleanToken); //either false or array();

        if (!$user_info) {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","Token ผิดพลาดหรือหมดอายุ","warning");</script>'));
            redirect(site_url() . 'main/login');
        }
        $data = array(
            'firstName' => $user_info->first_name,
            'email' => $user_info->email,
            'user_id' => $user_info->id,
            'token' => $this->base64url_encode($token)
        );

        $data['title'] = "Set the Password";

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', $data);
            $this->load->view('container');
            $this->load->view('complete', $data);
            $this->load->view('footer');
        } else {
            $this->load->library('password');
            $post = $this->input->post(NULL, TRUE);

            $cleanPost = $this->security->xss_clean($post);

            $hashed = $this->password->create_hash($cleanPost['password']);
            $cleanPost['password'] = $hashed;
            unset($cleanPost['passconf']);
            $userInfo = $this->user_model->updateUserInfo($cleanPost);

            if (!$userInfo) {
                //$this->session->set_flashdata('flash_message', 'There was a problem updating your record');
                $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","มีการอัพเดทข้อมูลกรุณาเข้าสู่ระบบใหม่อีกครั้ง","warning");</script>'));
                redirect(site_url() . 'main/login');
            }

            unset($userInfo->password);

            foreach ($userInfo as $key => $val) {
                $this->session->set_userdata($key, $val);
            }
            redirect(site_url() . 'main/');
        }
    }

    //check login failed or success
    public function login()
    {
        $data = $this->session->userdata;
        if (!empty($data['email'])) {
            redirect(site_url() . 'main/');
        } else {
            $this->load->library('curl');
            $this->load->library('recaptcha');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            $data['title'] = "Welcome";

            $result = $this->user_model->getAllSettings();
            $data['recaptcha'] = $result->recaptcha;

            if ($this->form_validation->run() == FALSE) {
                $data['menu'] = $this->Menu_Model->get_menu();
                $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
                $this->load->view('layout/main/header', $data);
                $this->load->view('Login_Page', $data);
                $this->load->view('layout/main/footer', $data);
            } else {
                $post = $this->input->post();
                $clean = $this->security->xss_clean($post);
                $userInfo = $this->user_model->checkLogin($clean);

                $data = array(
                    'uid' => $userInfo->id,
                    'first_name' => $userInfo->first_name,
                    'last_name' => $userInfo->last_name,
                    'email' => $userInfo->email,
                    'user_type' => $userInfo->user_type
                );
                $this->session->set_userdata($data);

                if ($data['recaptcha'] == 'yes') {
                    //recaptcha
                    $recaptchaResponse = $this->input->post('g-recaptcha-response');
                    $userIp = $_SERVER['REMOTE_ADDR'];
                    $key = $this->recaptcha->secret;
                    $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $key . "&response=" . $recaptchaResponse . "&remoteip=" . $userIp; //link
                    $response = $this->curl->simple_get($url);
                    $status = json_decode($response, true);

                    if (!$userInfo) {
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อีเมล์ หรือ รหัสผ่านผิด","warning");</script>'));
                        redirect(site_url() . 'main/login');
                    } elseif ($userInfo->banned_users == "ban") {
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ท่านไม่ได้รับอนุญาติเข้าสู่ระบบของเรา","warning");</script>'));
                        redirect(site_url() . 'main/login');
                    } else if (!$status['success']) {
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","Google Recaptcha ผิดพลาด","warning");</script>'));
                        redirect(site_url() . 'main/login/');
                        exit;
                    } elseif ($status['success'] && $userInfo && $userInfo->banned_users == "unban") //recaptcha check, success login, ban or unban
                    {
                        foreach ($userInfo as $key => $val) {
                            $this->session->set_userdata($key, $val);
                        }
                        redirect(site_url() . 'main/checkLoginUser/');
                    } else {
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","พลบางอย่างผิดพลาด","warning");</script>'));
                        redirect(site_url() . 'main/login/');
                        exit;
                    }
                } else {
                    if (!$userInfo) {
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อีเมล์ หรือ รหัสไม่ถูกต้อง","warning");</script>'));
                        redirect(site_url() . 'main/login');
                    } elseif ($userInfo->banned_users == "ban") {
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ท่านไม่ได้รับอนุญาติเข้าสู่ระบบของเรา","warning");</script>'));
                        redirect(site_url() . 'main/login');
                    } elseif ($userInfo && $userInfo->banned_users == "unban") //recaptcha check, success login, ban or unban
                    {
                        foreach ($userInfo as $key => $val) {
                            $this->session->set_userdata($key, $val);
                        }
                        redirect(site_url() . 'main/checkLoginUser/');
                    } else {
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","บางอย่างผิดพลาด","warning");</script>'));
                        redirect(site_url() . 'main/login/');
                        exit;
                    }
                }
            }
        }
    }

    //Logout
    public function logout()
    {
        $this->session->sess_destroy();
        redirect(site_url() . 'main/login/');
    }

    //forgot password
    public function forgot()
    {
        $data['title'] = "Forgot Password";
        $this->load->library('curl');
        $this->load->library('recaptcha');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        $result = $this->user_model->getAllSettings();
        $sTl = $result->site_title;
        $data['recaptcha'] = $result->recaptcha;

        if ($this->form_validation->run() == FALSE) {
            $data['menu'] = $this->Menu_Model->get_menu();
            $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
            $this->load->view('layout/main/header', $data);
            $this->load->view('admin/new_user_system/forgot', $data);
            $this->load->view('layout/main/footer', $data);
        } else {
            $email = $this->input->post('email');
            $clean = $this->security->xss_clean($email);
            $userInfo = $this->user_model->getUserInfoByEmail($clean);

            if (!$userInfo) {
                //$this->session->set_flashdata('flash_message', 'We cant find your email address');
                $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ไม่พลอีเมล์","warning");</script>'));
                redirect(site_url() . 'main/login');
            }

            if ($userInfo->status != $this->status[1]) { //if status is not approved
                //$this->session->set_flashdata('flash_message', 'Your account is not in approved status');
                $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","บัญชีของคุณยังไม่ได้รับการอนุมัติ","warning");</script>'));
                redirect(site_url() . 'main/login');
            }

            if ($data['recaptcha'] == 'yes') {
                //recaptcha
                $recaptchaResponse = $this->input->post('g-recaptcha-response');
                $userIp = $_SERVER['REMOTE_ADDR'];
                $key = $this->recaptcha->secret;
                $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $key . "&response=" . $recaptchaResponse . "&remoteip=" . $userIp; //link
                $response = $this->curl->simple_get($url);
                $status = json_decode($response, true);

                //recaptcha check
                if ($status['success']) {

                    //generate token
                    $token = $this->user_model->insertToken($userInfo->id);
                    $qstring = $this->base64url_encode($token);
                    $url = site_url() . 'main/reset_password/token/' . $qstring;
                    $link = '<a href="' . $url . '">' . $url . '</a>';

                    $this->load->library('email');
                    $this->load->library('sendmail');

                    $message = $this->sendmail->sendForgot($this->input->post('lastname'), $this->input->post('email'), $link, $sTl);
                    $to_email = $this->input->post('email');
                    $this->email->from($this->config->item('forgot'), 'Reset Password! ' . $this->input->post('firstname') . ' ' . $this->input->post('lastname')); //from sender, title email
                    $this->email->to($to_email);
                    $this->email->subject('Reset Password');
                    $this->email->message($message);
                    $this->email->set_mailtype("html");

                    if ($this->email->send()) {
                        redirect(site_url() . 'main/successresetpassword/');
                    } else {
                        //$this->session->set_flashdata('flash_message', 'There was a problem sending an email.');
                        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","จัดส่งอีเมล์ให้ท่านเรียบร้อย","warning");</script>'));
                        exit;
                    }
                } else {
                    //recaptcha failed
                    //$this->session->set_flashdata('flash_message', 'Error...! Google Recaptcha UnSuccessful!');
                    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","Google Recaptcha Error","warning");</script>'));
                    redirect(site_url() . 'main/register/');
                    exit;
                }
            } else {
                //generate token
                $token = $this->user_model->insertToken($userInfo->id);
                $qstring = $this->base64url_encode($token);
                $url = site_url() . 'main/reset_password/token/' . $qstring;
                $link = '<a href="' . $url . '">' . $url . '</a>';

                $this->load->library('email');
                $this->load->library('sendmail');

                $message = $this->sendmail->sendForgot($this->input->post('lastname'), $this->input->post('email'), $link, $sTl);
                $to_email = $this->input->post('email');
                $this->email->from($this->config->item('forgot'), 'Reset Password! ' . $this->input->post('firstname') . ' ' . $this->input->post('lastname')); //from sender, title email
                $this->email->to($to_email);
                $this->email->subject('Reset Password');
                $this->email->message($message);
                $this->email->set_mailtype("html");

                if ($this->email->send()) {
                    redirect(site_url() . 'main/successresetpassword/');
                } else {
                    //$this->session->set_flashdata('flash_message', 'There was a problem sending an email.');
                    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ระบบไม่สามารถส่งอีเมล์ถึงคุณได้","warning");</script>'));
                    exit;
                }
            }
        }
    }

    //reset password
    public function reset_password()
    {
        /*        $token = $this->base64url_decode($this->uri->segment(4));
        $cleanToken = $this->security->xss_clean($token);
        $user_info = $this->user_model->isTokenValid($cleanToken); //either false or array();

        if(!$user_info){
        $this->session->set_flashdata('flash_message', 'Token is invalid or expired');
        redirect(site_url().'main/login');
        }
        $data = array(
        'firstName'=> $user_info->first_name,
        'email'=>$user_info->email,
        //'user_id'=>$user_info->id,
        'token'=>$this->base64url_encode($token)
    );*/

        $data['title'] = "Reset Password";
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        $data['users'] = $this->Users_Model->get_profile($this->session->uid);
        if ($this->form_validation->run() == FALSE) {
            $data['menu'] = $this->Menu_Model->get_menu();
            $data['left'] = $this->Image_Register_Model->get_all_by_uid($this->session->uid);
            $data['shop'] = $this->Shop_Model->get_shop($this->session->uid);
            $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
            $this->load->view('layout/main/header', $data);
            $this->load->view('layout/users/left', $data);
            $this->load->view('admin/new_user_system/reset_password', $data);
            $this->load->view('layout/main/footer', $data);
        } else {
            $this->load->library('password');
            $post = $this->input->post(NULL, TRUE);
            $cleanPost = $this->security->xss_clean($post);
            $hashed = $this->password->create_hash($cleanPost['password']);
            $cleanPost['password'] = $hashed;
            $cleanPost['user_id'] = $data['users']->id;
            unset($cleanPost['passconf']);
            if (!$this->user_model->updatePassword($cleanPost)) {
                //$this->session->set_flashdata('flash_message', 'There was a problem updating your password');
                $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลไม่สำเร็จ","","warning");</script>'));
            } else {
                //$this->session->set_flashdata('success_message', 'Your password has been updated. You may now login');
                $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
            }
            redirect(site_url() . 'users');
        }
    }

    public function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function base64url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    // รายการอัพเดต (พระเครื่อง)
    public function amulet()
    {
        $config['base_url'] = base_url('main/amulet');
        $config['total_rows'] = $this->Product_Model->get_count_product_amulettop();
        $config['per_page'] = 30;
        $config['uri_segment'] = 3;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = array('class' => 'page-link');
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['use_page_numbers'] = TRUE;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = "รายการอัพเดต";
        $data['page_detail'] = $this->Menu_Model->get_menu_by_id(2);
        if (!empty($data['page_detail']->menu_seo_title)) {
            $data['title'] = $data['page_detail']->menu_seo_title;
            $data['description'] = $data['page_detail']->menu_seo_description;
        }
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['news'] = $this->Blog_Model->get_blog(10);
        $data['products'] = $this->Product_Model->get_pagination_product_amulet($config["per_page"], $page);
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->getCountAllProduct();
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();

        $this->load->view('layout/main/header', $data);
        $this->load->view('main/search');
        $this->load->view('layout/main/left', $data);
        $this->load->view('main/amulet', $data);
        $this->load->view('layout/main/right', $data);
        $this->load->view('layout/main/footer', $data);
    }

    // รายการพระเด่น
    public function amulettop()
    {
        $config['base_url'] = base_url('main/amulettop');
        $config['total_rows'] = $this->Product_Model->getCountAllProduct();
        $config['per_page'] = 18;
        $config['uri_segment'] = 3;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = array('class' => 'page-link');
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['use_page_numbers'] = TRUE;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = "รายการพระเด่น";
        $data['page_detail'] = $this->Menu_Model->get_menu_by_id(3);
        if (!empty($data['page_detail']->menu_seo_title)) {
            $data['title'] = $data['page_detail']->menu_seo_title;
            $data['description'] = $data['page_detail']->menu_seo_description;
        }
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['news'] = $this->Blog_Model->get_blog(10);
        $data['products'] = $this->Product_Model->get_pagination_product_orderby($config["per_page"], $page);
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->getCountAllProduct();
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();

        $this->load->view('layout/main/header', $data);
        $this->load->view('main/search');
        $this->load->view('layout/main/left', $data);
        $this->load->view('main/amulet', $data);
        $this->load->view('layout/main/right', $data);
        $this->load->view('layout/main/footer', $data);
    }

    // หมวดหมู่ พระเครื่อง
    public function category($id = null)
    {
        $config['base_url'] = base_url("main/category/{$id}");
        $config['total_rows'] = $this->Product_Model->get_count_product($id);
        $config['per_page'] = 30;
        $config['uri_segment'] = 3;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = array('class' => 'page-link');
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['use_page_numbers'] = TRUE;
        $page = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) * $config["per_page"] : 0;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = "หมวดหมู่ พระเครื่อง";
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['news'] = $this->Blog_Model->get_blog(10);
        $data['products'] = $this->Product_Model->get_pagination_product_group($config["per_page"], $page, $id);
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->getCountAllProduct();
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();
        // $data['search'] = $id;

        $this->load->view('layout/main/header', $data);
        $this->load->view('main/search', $data);
        $this->load->view('layout/main/left', $data);
        // $this->load->view('main/search_pra', $data);
        $this->load->view('main/amulet', $data);
        $this->load->view('layout/main/right', $data);
        $this->load->view('layout/main/footer', $data);
    }

    // รายการข่าว
    public function news()
    {
        $config['base_url'] = base_url('main/news');
        $config['total_rows'] = $this->Blog_Model->getCountAll();
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = array('class' => 'page-link');
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->getCountAllProduct();

        $config['use_page_numbers'] = TRUE;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $config["per_page"] : 0;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = "ข่าวประชาสัมพันธ์";
        $data['page_detail'] = $this->Menu_Model->get_menu_by_id(5);
        if (!empty($data['page_detail']->menu_seo_title)) {
            $data['title'] = $data['page_detail']->menu_seo_title;
            $data['description'] = $data['page_detail']->menu_seo_description;
        }
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['news'] = $this->Blog_Model->get_pagination($config["per_page"], $page);
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();

        $this->load->view('layout/main/header', $data);
        $this->load->view('layout/main/left', $data);
        $this->load->view('main/news', $data);
        $this->load->view('layout/main/footer', $data);
    }

    // รายละเอียดข่าว
    public function news_detail($id = null)
    {
        $data['title'] = "ข่าวประชาสัมพันธ์";
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['newsDetail'] = $this->Blog_Model->get_blog_by_id($id);
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->getCountAllProduct();
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();

        $this->load->view('layout/main/header', $data);
        $this->load->view('layout/main/left', $data);
        $this->load->view('main/blog', $data);
        $this->load->view('layout/main/footer', $data);
    }

    public function shoplist()
    {
        $data['title'] = 'ร้านพระมาตรฐาน';
        $data['page_detail'] = $this->Menu_Model->get_menu_by_id(4);
        if (!empty($data['page_detail']->menu_seo_title)) {
            $data['title'] = $data['page_detail']->menu_seo_title;
            $data['description'] = $data['page_detail']->menu_seo_description;
        }
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['news'] = $this->Blog_Model->get_blog(10);
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->getCountAllProduct();
        $data['shoplist'] = $this->Shop_Model->get_all_shop_show();
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();

        $this->load->view('layout/main/header', $data);
        $this->load->view('main/search');
        $this->load->view('layout/main/left', $data);
        $this->load->view('main/shoplist', $data);
        $this->load->view('layout/main/right', $data);
        $this->load->view('layout/main/footer', $data);
    }

    public function shop($uid = null)
    {
        if (!isset($_SESSION['view_shop'])) {
            $this->Shop_Model->update_view($uid);
        }
        $_SESSION['view_shop'] = SESSION_ID();
        $this->Shop_Model->update_view($uid);

        $shop = $this->Shop_Model->get_shop_id($uid);
        //print_r($shop);
        $data['title'] = $shop->shop_name;
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['shop'] = $shop;
        $data['blog'] = $this->Blog_Model->get_data_by_uid($uid);
        $data['products'] = $this->Product_Model->get_product_by_shop_id($uid);
        $data['user'] = $this->Users_Model->get_user($shop->users_id);

        if ($uid == 1) {
            $data['news'] = $this->Blog_Model->get_blog();
        }

        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();

        $this->load->view('layout/main/header', $data);
        // $this->load->view('main/search');
        $this->load->view('main/shop', $data);
        $this->load->view('layout/main/footer', $data);
    }
    public function search($input=null)
    {
        $input = $this->input->post('search');
                
        $data['search']=$input;
        $data['shoplist'] = $this->Shop_Model->search_shop($input);
        $data['products'] = $this->Product_Model->search_product($input);
        $data['title'] = "ผลการค้นหาบนตลาดนัดพระเครื่องออนไลน์";
        $data['menu'] = $this->Menu_Model->get_menu();
        $data['recommend'] = $this->Shop_Model->get_recommend();
        $data['product_type'] = $this->Product_Type_Model->get_all_product_type();
        $data['news'] = $this->Blog_Model->get_blog(10);
        $data['static'] = $this->Static_Model->get_static();
        $data['countProduct'] = $this->Product_Model->get_count_product_search($input);
        $data['theme_for_design'] = $this->Theme_Model->get_theme_design();
        $data['visitorOnline'] = $this->Static_Model->visitorOnline();

        $this->load->view('layout/main/header',$data);
        $this->load->view('main/search',$data);
        $this->load->view('layout/main/left',$data);
        $this->load->view('main/search_detail',$data);
        $this->load->view('layout/main/right',$data);       
        $this->load->view('layout/main/footer',$data);
    }
}
