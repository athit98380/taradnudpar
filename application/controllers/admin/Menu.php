<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Menu extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Menu_Model');
    $this->load->library('form_validation');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index()
  {
    $data['title'] = "จัดการชื่อเมนู";
    $data['menu'] = $this->Menu_Model->get_menu();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/menu/home',$data);
    $this->load->view('admin/layout/footer',$data);
  }
  /*
  public function add()
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $this->form_validation->set_rules('level','Level','required');
      $this->form_validation->set_rules('name','Name','required');
      $this->form_validation->set_rules('link','Link','required');
      if ($this->form_validation->run() == true) {
        $data = array(
          'menu_level' => $input['level'],
          'menu_name' => $input['name'],
          'menu_link' => $input['link']
        );
        $this->Menu_Model->create_menu($data);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เพิ่มข้อมูลสำเร็จ","success");</script>'));
        redirect(base_url('admin/menu'));
      }
    }
    $data['title'] = "เพิ่มเมนู";

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/menu/add_menu',$data);
    $this->load->view('admin/layout/footer',$data);
  }
  */

  public function edit($id = null)
  {
    $input = $this->input->post(null,false);
    if (!empty($input)) {
      $this->form_validation->set_rules('level','Level','required');
      $this->form_validation->set_rules('name','Name','required');
      // $this->form_validation->set_rules('link','Link','required');
      if ($this->form_validation->run() == true) {
        $data = array(
          'menu_level' => $input['level'],
          'menu_name' => $input['name'],
          'menu_detail' => $input['detail'],
          'menu_seo_title' => $input['title_seo'],
          'menu_seo_description' => $input['descript_seo']
          // 'menu_link' => $input['link']
        );
        $this->Menu_Model->update_menu($data,$id);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url('admin/menu'));
      }
    }

    $data['title'] = "จัดการชื่อเมนู";
    $data['menu'] = $this->Menu_Model->get_menu_by_id($id);

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/menu/edit_menu',$data);
    $this->load->view('admin/layout/footer',$data);
  }
}

 ?>
