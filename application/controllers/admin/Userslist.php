<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Userslist extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_Model');
        $this->load->model('Shop_Model');
        $this->load->model('Image_Register_Model');
        $this->load->library('Datethai');
        $this->load->library('Count_admin');
        $this->load->library('form_validation');
        $this->load->model('User_model', 'user_model', TRUE);

        if ($this->session->userdata('user_type') != 'admin') {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['title'] = "จัดการผู้ใช้สมัครใหม่";
        $data['users'] = $this->Users_Model->get_new_all();

        $this->load->view('admin/layout/header',$data);
        $this->load->view('admin/users/userslist',$data);
        $this->load->view('admin/layout/footer',$data);
    }

    public function all()
    {
        $data['title'] = "สมาชิกทั้งหมด";
        $data['users'] = $this->Users_Model->get_all();

        $this->load->view('admin/layout/header',$data);
        $this->load->view('admin/users/usersall',$data);
        $this->load->view('admin/layout/footer',$data);
    }

    public function view_member($uid = null)
    {
        $data['title'] = "สมาชิก";
        $data['users'] = $this->Users_Model->get_user($uid);
        $data['shop'] = $this->Shop_Model->get_shop($uid);
        $data['photo'] = $this->Image_Register_Model->get_all_by_uid($uid);

        $this->load->view('admin/layout/header',$data);
        $this->load->view('admin/users/view_user',$data);
        $this->load->view('admin/layout/footer',$data);
    }

      // แก้ไข โปรไฟล์ Member
      public function edit_member($uid = null)
      {
        $input = $this->input->post(null,true);
        if (!empty($input)) {
          $data = array(
            'user_tel' => $input['user_tel'],
            'user_tel_add' => $input['user_tel_add']
          );
          $this->Shop_Model->update_member_profile($data,$uid);
          $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
          redirect(base_url('admin/userslist/view_member/'.$uid));
        }
      }

      // แก้ไข โปรไฟล์ post
      public function edit_profile($uid = null,$sid = null)
      {
        $input = $this->input->post(null,true);
        if (!empty($input)) {
          $data = array(
            'shop_name' => $input['shopname'],
            'shop_address' => $input['shop_address'],
            'shop_detail' => $input['shopdetail'],
            'shop_guarantee' => $input['shop_guarantee'],
            'shop_guarantee_by' => $input['shop_guarantee_by'],
            'shop_guarantee_by_tel' => $input['shop_guarantee_by_tel']
          );
          $this->Shop_Model->update_shop_profile($data,$sid);
          $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
          redirect(base_url('admin/userslist/view_member/'.$uid));
        }
      }

      // แก้ไข โปรไฟล์ blog
      public function edit_blog($uid = null,$sid = null)
      {
        $input = $this->input->post(null,true);
        if (!empty($input)) {
          $data = array(
            'shop_blog' => $input['shop_blog']
          );
          $this->Shop_Model->update_shop_profile($data,$sid);
          $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
          redirect(base_url('admin/userslist/view_member/'.$uid));
        }
      }


      // แก้ไข ข้อมูลธนาคาร post
      public function edit_bank($uid = null,$sid = null)
      {
        $input = $this->input->post(null,true);
        if (!empty($input)) {
          $data = array(
            'shop_bank_name' => $input['sbankname'],
            'shop_bank_type' => $input['sbanktype'],
            'shop_bank_sub' => $input['sbanksub'],
            'shop_bank_name_account' => $input['sbankaccount'],
            'shop_bank_number' => $input['sbanknumber']
          );
          $this->Shop_Model->update_shop_bank($data,$sid);
          $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
          redirect(base_url('admin/userslist/view_member/'.$uid));
        }
      }

    public function reset_password($id=null)
    {

      $input = $this->input->post();
      if (!empty($input)) {
        $data['title'] = "Reset Password";
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == true) {
            $this->load->library('password');
            $post = $this->input->post(NULL, TRUE);
            $cleanPost = $this->security->xss_clean($post);
            $hashed = $this->password->create_hash($cleanPost['password']);
            $cleanPost['password'] = $hashed;
            $cleanPost['user_id'] = $id;
            if(!$this->user_model->updatePassword($cleanPost)){
                //$this->session->set_flashdata('flash_message', 'There was a problem updating your password');
                $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลไม่สำเร็จ","","warning");</script>'));
            }else{
                //$this->session->set_flashdata('success_message', 'Your password has been updated. You may now login');
                $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","","success");</script>'));
            }
            redirect(site_url().'admin/userslist/view_member/'.$id);
        }
      }
    }

      public function change_image_profile($uid = null)
      {
            $upload_path = FCPATH . 'assets/uploads/numberid/'.$uid;
            if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
            $this->load->library('upload',[
              'upload_path' => $upload_path,
              'max_size' => 15000,
              'encrypt_name' => TRUE,
              'allowed_types' => 'jpg|jpeg|png'
            ]);
        if ($this->upload->do_upload('pimage')) {
            $file = $this->upload->data();
            $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $file['file_name']);
            $file_parts_width = $file_parts[0];
            $file_thumb=$file['file_name'];

            $img_path = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];
            $img_thumb = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];

            $config['image_library'] = 'gd2';
            $config['source_image'] = $img_path;
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = FALSE;

            //$img = imagecreatefromjpeg($img_path);
              $image_type = $file_parts['mime'];
              if ($image_type == 'image/jpeg') {
                  $img = imagecreatefromjpeg($img_path);
              } elseif ($image_type == 'image/gif') {
                  $img = imagecreatefromgif($img_path);
              } elseif ($image_type == 'image/png') {
                  $img = imagecreatefrompng($img_path);
              }
            $_width = imagesx($img);
            $_height = imagesy($img);

            $img_type = '';
            $thumb_size = 200;

            if ($_width > $_height)
            {
                // wide image
                $config['width'] = intval(($_width / $_height) * $thumb_size);
                if ($config['width'] % 2 != 0)
                {
                    $config['width']++;
                }
                $config['height'] = $thumb_size;
                $img_type = 'wide';
            }
            else if ($_width < $_height)
            {
                // landscape image
                $config['width'] = $thumb_size;
                $config['height'] = intval(($_height / $_width) * $thumb_size);
                if ($config['height'] % 2 != 0)
                {
                    $config['height']++;
                }
                $img_type = 'landscape';
            }
            else
            {
                // square image
                $config['width'] = $thumb_size;
                $config['height'] = $thumb_size;
                $img_type = 'square';
            }

            $this->load->library('image_lib');
            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            // reconfigure the image lib for cropping
            $conf_new = array(
                'image_library' => 'gd2',
                'source_image' => $img_thumb,
                'create_thumb' => FALSE,
                'maintain_ratio' => FALSE,
                'width' => $thumb_size,
                'height' => $thumb_size
            );

            if ($img_type == 'wide')
            {
                $conf_new['x_axis'] = ($config['width'] - $thumb_size) / 2 ;
                $conf_new['y_axis'] = 0;
            }
            else if($img_type == 'landscape')
            {
                $conf_new['x_axis'] = 0;
                $conf_new['y_axis'] = ($config['height'] - $thumb_size) / 2;
            }
            else
            {
                $conf_new['x_axis'] = 0;
                $conf_new['y_axis'] = 0;
            }

            $this->image_lib->initialize($conf_new);

            $this->image_lib->crop();

            $this->image_lib->clear();
            unset($config);
        }

            if ($this->upload->do_upload('pimage')) {
              $file = $this->upload->data();
                  $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $file['file_name']);
                  $file_parts_width = $file_parts[0];

                  if ($file_parts_width > 1920)
                  {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width']     = 1920;
                    $this->load->library('image_lib', $config); 
                    $this->image_lib->resize();
                  }
              $data = array(
              'user_image' => $file_thumb,
              'user_image_profile' => $file['file_name']
              );

              if ($this->Users_Model->change_image_profile($data,$uid) != false) {
                $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดสำเร็จ","success");</script>'));
                redirect(base_url('admin/userslist/view_member/'.$uid));
              }else {
                $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
               redirect(base_url('admin/userslist/view_member/'.$uid));
              }

            }
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
            redirect(base_url('admin/userslist/view_member/'.$uid));
      }

    public function photo($uid = null)
    {
        $data['title'] = "ดูรูปพระเครื่อง";
        $data['photo'] = $this->Image_Register_Model->get_all_by_uid($uid);

        $this->load->view('admin/layout/header',$data);
        $this->load->view('admin/users/photoregister',$data);
        $this->load->view('admin/layout/footer',$data);
    }

    public function confirm_member($uid = null,$comeback = null)
    {
        $data2 = array(
            'admin_active' => 1
        );
        $this->Users_Model->admin_active($data2,$uid);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        if (!empty($comeback)) {
        redirect(base_url('admin/userslist/view_member/'.$uid));
        }else{
        redirect(base_url('admin/userslist'));
        }
    }

    public function confirm_shop($uid = null,$comeback = null)
    {
        $data1 = array(
            'shop_status' => '0',
            'shop_amount_added' => 1
        );
        $this->Shop_Model->admin_active($data1,$uid);
        $data3 = array(
            'image_register_comment' => '',
            'sub_order_status' => 1
        );
        $this->Image_Register_Model->admin_active($data3,$uid);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        if (!empty($comeback)) {
        redirect(base_url('admin/userslist/view_member/'.$uid));
        }else{
        redirect(base_url('admin/userslist'));
        }
    }
    public function unconfirm_member($uid = null,$comeback = null)
    {
/*        $input = $this->input->post(null,true);
        $uid = $input['uid'];
        $comment = $input['comment'];*/
        $data2 = array(
            'admin_active' => 0
        );
        $this->Users_Model->admin_active($data2,$uid);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        if (!empty($comeback)) {
        redirect(base_url('admin/userslist/view_member/'.$uid));
        }else{
        redirect(base_url('admin/userslist'));
        }
    }

    public function unconfirm_shop($uid = null,$comeback = null)
    {
/*        $input = $this->input->post(null,true);
        $uid = $input['uid'];
        $comment = $input['comment'];*/
        $data1 = array(
            'shop_status' => '1',
            'shop_amount_added' => 0
        );
        $this->Shop_Model->admin_active($data1,$uid);
        $data3 = array(
            'image_register_comment' => $comment,
            'sub_order_status' => 0
        );
        $this->Image_Register_Model->admin_active($data3,$uid);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        if (!empty($comeback)) {
        redirect(base_url('admin/userslist/view_member/'.$uid));
        }else{
        redirect(base_url('admin/userslist'));
        }
    }

    public function remove($uid = null)
    {
/*        $input = $this->input->post(null,true);
        $uid = $input['uid'];
        $comment = $input['comment'];*/
        $data1 = array(
            'shop_status' => '2',
            'shop_amount_added' => 0
        );
        $this->Shop_Model->admin_active($data1,$uid);
        $data2 = array(
            'admin_active' => 2
        );
        $this->Users_Model->admin_active($data2,$uid);
        $data3 = array(
            'image_register_comment' => $comment,
            'sub_order_status' => 2
        );
        $this->Image_Register_Model->admin_active($data3,$uid);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url('admin/userslist'));
    }

    public function change_shop_banner($uid=null,$sid=null)
    {
        $upload_path = FCPATH . 'assets/uploads/numberid/'.$uid;
        if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 15000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png'
        ]);
        if ($this->upload->do_upload('shop_banner')) {
          $file = $this->upload->data();
              $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $file['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config); 
                $this->image_lib->resize();
              }
          if (!empty($input['shop_qrcode_link'])) {
            $data = array(
            'shop_qrcode' => $file['file_name'],
            'shop_qrcode_link' => $input['shop_qrcode_link']
            );
          }else{
            $data = array(
            'shop_qrcode' => $file['file_name']
            );
          }

          if ($this->Shop_Model->change_shop_banner($data,$sid) != false) {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","อัพโหลดสำเร็จ","success");</script>'));
            redirect(base_url('admin/userslist/view_member/'.$uid));
          }else {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
            redirect(base_url('admin/userslist/view_member/'.$uid));
          }

        }else{
          if (!empty($input['shop_qrcode_old'])) {
            if (!empty($input['shop_qrcode_link'])) {
              $data = array(
              'shop_qrcode' => $input['shop_qrcode_old'],
              'shop_qrcode_link' => $input['shop_qrcode_link']
              );
            }else{
              $data = array(
              'shop_qrcode' => $input['shop_qrcode_old']
            );
            }
          }
          if ($this->Shop_Model->change_shop_qrcode($data,$sid) != false) {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","บันทึกข้อมูลสำเร็จ","success");</script>'));
            redirect(base_url('admin/userslist/view_member/'.$uid));
          }else {
            $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด","warning");</script>'));
            redirect(base_url('admin/userslist/view_member/'.$uid));
          }
        }
        //$this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เกิดข้อผิดพลาด ขนาดไฟล์ใหญ่เกิน 15 Mb","warning");</script>'));
        //redirect(base_url('admin/userslist/view_member/'.$uid));
    }
}


?>