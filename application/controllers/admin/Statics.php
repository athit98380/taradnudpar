<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Statics extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Static_Model');
    $this->load->library('form_validation');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index()
  {
    $data['title'] = "จัดการสถิติเว็บไซต์";
    $data['static'] = $this->Static_Model->get_static();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/static/home',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function edit()
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $this->form_validation->set_rules('view_all','View All','required');
      $this->form_validation->set_rules('view_to_day','View To Day','required');
      $this->form_validation->set_rules('view_online','View Online','required');
      if ($this->form_validation->run() == true) {
        $data = array(
          'static_view_all' => $input['view_all'],
          'static_view_today' => $input['view_to_day'],
          'static_view_online' => $input['view_online']
        );
        $this->Static_Model->update_static_admin($data);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url('admin/statics'));
      }
    }
    $data['title'] = "แก้ไขสถิติเว็บไซต์";
    $data['static'] = $this->Static_Model->get_static();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/static/edit_static',$data);
    $this->load->view('admin/layout/footer',$data);
  }
}
 ?>
