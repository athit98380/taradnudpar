<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Main extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Count_admin');
    // if ($this->session->userdata('user_type') != 'admin') {
    //   redirect(base_url());
    // }
  }

  public function index()
  {
    $user_type = $this->session->user_type;
    if (isset($user_type)) {
      if ($user_type == 'admin') {
        if (!file_exists('application/views/admin/dashboard.php')) {
          show_404();
        }
        //echo $this->Count_admin->Count_data();
        $data['title'] = ucfirst('dashboard');

        $this->load->view('admin/layout/header',$data);
        $this->load->view('admin/dashboard',$data);
        $this->load->view('admin/layout/footer',$data);
      }else {
        redirect(base_url());
      }
    }else {
      redirect(base_url('login'));
    }
  }
}

 ?>
