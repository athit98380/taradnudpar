<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Theme extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Menu_Model');
    $this->load->model('Theme_Model');
    $this->load->library('form_validation');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index()
  {
    $data['title'] = "จัดการตกแต่งเว็บไซต์";
    $data['menu'] = $this->Menu_Model->get_menu();
    $data['theme'] = $this->Theme_Model->get_theme();
    //print_r($data['theme']);
    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/theme/theme',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function edit($id = '1')
  {
    $input = $this->input->post(null,false);
    if (!empty($input)) {
      $this->form_validation->set_rules('theme_h_height','Header Height','required');
      $this->form_validation->set_rules('theme_f_height','Footer Height','required');
      if ($this->form_validation->run() == true) {
        $upload_path = FCPATH . 'assets/images/theme/';
        if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 15000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png'
        ]);
        if ($this->upload->do_upload('theme_bg_image')) {
            $bg_image = $this->upload->data();
            $file_parts = getimagesize('assets/images/theme/'. $bg_image['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/images/theme/'. $bg_image['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config); 
                $this->image_lib->resize();
              }
        }else{
            $bg_image['file_name']=$input['theme_bg_image_old'];
        }
        if ($this->upload->do_upload('image_header')) {
            $image_header = $this->upload->data();
            $file_parts = getimagesize('assets/images/theme/'. $image_header['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/images/theme/'. $image_header['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config); 
                $this->image_lib->resize();
              }
        }else{
            $image_header['file_name']=$input['image_header_old'];
        }
        if ($this->upload->do_upload('image_footer')) {
            $image_footer = $this->upload->data();
            $file_parts = getimagesize('assets/images/theme/'. $image_footer['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/images/theme/'. $image_footer['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config); 
                $this->image_lib->resize();
              }
        }else{
            $image_footer['file_name']=$input['image_footer_old'];
        }
        $data = array(
          'theme_h_image' => @$image_header['file_name'],
          'theme_h_height' => $input['theme_h_height'],
          'theme_f_image' => @$image_footer['file_name'],
          'theme_f_height' => $input['theme_f_height'],
          'theme_f_detail' => $input['detail'],
          'theme_f_all_right' => $input['detail_footer'],
          'theme_bg_image' => @$bg_image['file_name']
        );
        $this->Theme_Model->update_theme($data,$id);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url('admin/theme'));
      }
    }

    $data['title'] = "จัดการตกแต่งเว็บไซต์";
    $data['menu'] = $this->Menu_Model->get_menu_by_id($id);
    $data['theme'] = $this->Theme_Model->get_theme();
    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/theme/theme',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function color($id = '1')
  {
    $input = $this->input->post(null,false);
    if (!empty($input)) {
      $this->form_validation->set_rules('theme_d_category_bg','theme_d_category_bg','required');
      if ($this->form_validation->run() == true) {
        $data = array(
          'theme_color_bg_all' => $input['theme_color_bg_all'],
          'theme_color_font_all' => $input['theme_color_font_all'],
          'theme_d_category_bg' => $input['theme_d_category_bg'],
          'theme_d_category_font' => $input['theme_d_category_font'],
          'theme_p_modal_bg' => $input['theme_p_modal_bg'],
          'theme_p_modal_font' => $input['theme_p_modal_font'],
          'theme_p_list_bg' => $input['theme_p_list_bg'],
          'theme_p_list_font' => $input['theme_p_list_font'],
          'theme_d_news_bg' => $input['theme_d_news_bg'],
          'theme_d_news_font' => $input['theme_d_news_font'],
          'theme_d_menu_bg' => $input['theme_d_menu_bg'],
          'theme_d_menu_font' => $input['theme_d_menu_font'],
          'theme_page_bg' => $input['theme_page_bg'],
          'theme_page_font' => $input['theme_page_font']
        );
        $this->Theme_Model->update_theme($data,$id);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url('admin/theme/color'));
      }else{
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการไม่สำเร็จ","warning");</script>'));
        redirect(base_url('admin/theme/color'));
      }
    }

    $data['title'] = "จัดการตกแต่งเว็บไซต์";
    $data['menu'] = $this->Menu_Model->get_menu_by_id($id);
    $data['theme'] = $this->Theme_Model->get_theme();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/theme/color',$data);
    $this->load->view('admin/layout/footer',$data);
  }
}

 ?>
