<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class News extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Blog_Model');
    $this->load->library('form_validation');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index()
  {
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    $data['title'] = 'ข่าวประชาสัมพันธ์';
    $data['items'] = $this->Blog_Model->get_all_blog();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/blog/news',$data);
    $this->load->view('admin/layout/footer',$data);

  }

  public function add()
  {
    $input = $this->input->post();
    if (!empty($input)) {
      $this->form_validation->set_rules('name','Title','required');
      // $this->form_validation->set_rules('file','File','required');
      $this->form_validation->set_rules('detail','Detail','required');
      if ($this->form_validation->run() == true) {
        // Upload Image
        $upload_path = "assets/uploads";
        if(!file_exists($upload_path)) mkdir($upload_path);
        if(!$_FILES) redirect(base_url('admin/news'));
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'allowed_types' => 'jpg|jpeg|png|gif'
        ]);

        if ($this->upload->do_upload('file')) {
          $file = $this->upload->data();
          $data = array(
            'blog_name' => $input['name'],
            'blog_image' => $file['file_name'],
            'blog_detail' => $input['detail'],
            'users_id' => $this->session->uid
          );
          $this->db->set('blog_create_on', 'NOW()', FALSE);
          $this->db->set('blog_update_on', 'NOW()', FALSE);
          $this->Blog_Model->create_news($data);
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เพิ่มข้อมูลสำเร็จ","success");</script>'));
          redirect(base_url('admin/news'));
        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","You did not select a file to upload.","warning");</script>'));
          // $data['error'] = $this->upload->display_errors();
        }
      }
    }

    $data['title'] = "เพิ่มข่าวประชาสัมพันธ์";

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/blog/add_news',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function edit($id = null)
  {
    $input = $this->input->post();
    if (!empty($input)) {
      $this->form_validation->set_rules('name','Name','required');
      $this->form_validation->set_rules('detail','Detail','required');
      if ($this->form_validation->run() == true) {
        // Upload Image
        $upload_path = "assets/uploads";
        if(!file_exists($upload_path)) mkdir($upload_path);
        if(!$_FILES) redirect(base_url('admin/news'));
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 15000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png'
        ]);

        if ($this->upload->do_upload('file')) {
          $file = $this->upload->data();
              $file_parts = getimagesize('assets/uploads/'. $file['file_name']);
              $file_parts_width = $file_parts[0];

              if ($file_parts_width > 1920)
              {
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/uploads/'. $file['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 1920;
                $this->load->library('image_lib', $config); 
                $this->image_lib->resize();
              }
          $this->db->set('blog_image', $file['file_name']);
        }

        $data = array(
          'blog_name' => $input['name'],
          'blog_detail' => $input['detail'],
          'users_id' => $this->session->uid
        );
        $this->db->set('blog_update_on', 'NOW()', FALSE);
        $this->Blog_Model->update_item($id,$data);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url("admin/news"));
      }
    }

    $data['title'] = ucfirst('group');
    $data['items'] = $this->Blog_Model->get_by_id($id);

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/blog/edit_news',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function delete($id = null)
  {
    $this->Blog_Model->delete_item($id);
    $this->session->set_flashdata(array('msg' => "<script>swal('ข้อความจากระบบ','ลบข้อมูลสำเร็จ','success');</script>"));
    redirect(base_url('admin/news'));
  }
}

 ?>
