<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Renew extends CI_Controller
{
    function __construct()
  {
    parent::__construct();
    $this->load->model('Renew_Model');
    $this->load->model('Shop_Model');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index()
  {
    $data['title'] = "ยืนยันการชำระเงิน";
    $data['renew'] = $this->Renew_Model->get();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/renew/home',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function confirm($rid)
  {
    $input = $this->input->get(null,true);
    // set status
    $this->Renew_Model->confirm(1,$rid);
    $shopid = $this->Renew_Model->get_shopid($rid);
    $shop = $this->Shop_Model->get_shop_id($shopid);
    $dataNext = date('Y-m-d H:i:s',strtotime($shop->shop_expired_on." + ".$input['year']." year"));

    $data = array(
        'shop_expired_on' => $dataNext
    );
    $this->Shop_Model->update_shop($data,$shopid);

    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    redirect(base_url('admin/renew'));

  }

  public function unconfirm($rid)
  {
    // set status
    $this->Renew_Model->confirm(2,$rid);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    redirect(base_url('admin/renew'));
  }
}

?>