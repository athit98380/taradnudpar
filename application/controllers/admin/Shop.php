<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Shop extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Image_Register_Model');
    $this->load->model('Product_Model');
    $this->load->model('Product_Type_Model');
    $this->load->model('Blog_Model');
    $this->load->model('Shop_Model');
    $this->load->model('Users_Model');
    $this->load->library('Datethai');
    $this->load->library('form_validation');
    $this->load->helper('file');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index()
  {
    $data['title'] = "สมัครร้านค้า";
    $data['shop'] = $this->Shop_Model->get_all_shop();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/shop/home',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function all()
  {
    $data['title'] = "ร้านค้าทั้งหมด";
    $data['shop'] = $this->Shop_Model->get_all_shop_show();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/shop/home',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function search($uid = null)
  {
    $data['title'] = "ร้านค้า";
    $data['shop'] = $this->Shop_Model->get_shop($uid);

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/shop/search',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  // แก้ไขร้านค้า
  public function edit_shop($sid)
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $this->form_validation->set_rules('sid','ID','required');
      $this->form_validation->set_rules('sname','Name','required');
      $this->form_validation->set_rules('saddress','Adress','required');
      $this->form_validation->set_rules('sdetail','Detail','required');
      $this->form_validation->set_rules('sguarantee','Guarantee','required');
      $this->form_validation->set_rules('sguarantee_by','Guarantee By','required');
      $this->form_validation->set_rules('sguarantee_by_tel','Guarantee By Tel','required');
      $this->form_validation->set_rules('sbankname','Bank Name','required');
      $this->form_validation->set_rules('sbanktype','Bank Type','required');
      $this->form_validation->set_rules('sbanksub','Bank Sub','required');
      $this->form_validation->set_rules('sbanknameaccount','Bank Name Account','required');
      $this->form_validation->set_rules('sbanknumber','Bank Number','required');
      $this->form_validation->set_rules('sexpired','Expired','required');
      $this->form_validation->set_rules('sstatus','Status','required');
      $this->form_validation->set_rules('sview','View','required');
      if ($this->form_validation->run() == true) {
        $data = array(
          'shop_name' => $input['sname'],
          'shop_address' => $input['saddress'],
          'shop_detail' => $input['sdetail'],
          'shop_guarantee' => $input['sguarantee'],
          'shop_guarantee_by' => $input['sguarantee_by'],
          'shop_guarantee_by_tel' => $input['sguarantee_by_tel'],
          'shop_bank_name' => $input['sbankname'],
          'shop_bank_type' => $input['sbanktype'],
          'shop_bank_sub' => $input['sbanksub'],
          'shop_bank_name_account' => $input['sbanknameaccount'],
          'shop_bank_number' => $input['sbanknumber'],
          'shop_expired_on' => $input['sexpired'],
          'shop_status' => $input['sstatus'],
          'shop_view' => $input['sview']
        );
        $this->Shop_Model->update_shop($data,$input['sid']);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url("admin/shop"));
      }
    }
    $shop = $this->Shop_Model->get_shop_id($sid);
    $data['title'] = "แก้ไขร้าน ".$shop->shop_name;
    $data['shop'] = $shop;

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/shop/edit_shop.php',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  // รายการสินค้าแต่ละร้าน
  public function product($sid = null)
  {
    $shop = $this->Shop_Model->get_shop_id($sid);
    $data['title'] = "รายการสินค้าของ ".@$shop->shop_name;
    $data['products'] = $this->Product_Model->get_product_by_shop($sid);

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/shop/product',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  // แก้ไขสินค้า
  public function edit_product($uid = null,$sid = null,$pid = null)
  {
    $input = $this->input->post(null,true);
    if (!empty($input)) {
      $this->form_validation->set_rules('pname','Product Name','required');
      $this->form_validation->set_rules('pprice','Product Price','required');
      $this->form_validation->set_rules('ptype','Product Type','required');
      $this->form_validation->set_rules('pdetail','Product Detail','required');

      if ($this->form_validation->run() == true) {
        $upload_path = FCPATH . 'assets/uploads/numberid/'.$uid;
        if(!file_exists($upload_path)) mkdir($upload_path, 0777, true);
        $this->load->library('upload',[
          'upload_path' => $upload_path,
          'max_size' => 15000,
          'encrypt_name' => TRUE,
          'allowed_types' => 'jpg|jpeg|png|gif'
        ]);

        $error_text_font_detail="";
        $file_thumb="";
        for ($i=1; $i < 6; $i++) { 
          if ($this->upload->do_upload('pimage'.$i)) {
              $file = $this->upload->data();
              $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $file['file_name']);
              $file_parts_width = $file_parts[0];
              $file_thumb.=$file['file_name'].",";

              $img_path = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];
              $img_thumb = 'assets/uploads/numberid/' .$uid .'/'. $file['file_name'];

              $config['image_library'] = 'gd2';
              $config['source_image'] = $img_path;
              $config['create_thumb'] = FALSE;
              $config['maintain_ratio'] = FALSE;

              //$img = imagecreatefromjpeg($img_path);
              $image_type = $file_parts['mime'];
              if ($image_type == 'image/jpeg') {
                  $img = imagecreatefromjpeg($img_path);
              } elseif ($image_type == 'image/gif') {
                  $img = imagecreatefromgif($img_path);
              } elseif ($image_type == 'image/png') {
                  $img = imagecreatefrompng($img_path);
              }
              $_width = imagesx($img);
              $_height = imagesy($img);

              $img_type = '';
              $thumb_size = 200;

              if ($_width > $_height)
              {
                  // wide image
                  $config['width'] = intval(($_width / $_height) * $thumb_size);
                  if ($config['width'] % 2 != 0)
                  {
                      $config['width']++;
                  }
                  $config['height'] = $thumb_size;
                  $img_type = 'wide';
              }
              else if ($_width < $_height)
              {
                  // landscape image
                  $config['width'] = $thumb_size;
                  $config['height'] = intval(($_height / $_width) * $thumb_size);
                  if ($config['height'] % 2 != 0)
                  {
                      $config['height']++;
                  }
                  $img_type = 'landscape';
              }
              else
              {
                  // square image
                  $config['width'] = $thumb_size;
                  $config['height'] = $thumb_size;
                  $img_type = 'square';
              }

              $this->load->library('image_lib');
              $this->image_lib->initialize($config);
              $this->image_lib->resize();

              // reconfigure the image lib for cropping
              $conf_new = array(
                  'image_library' => 'gd2',
                  'source_image' => $img_thumb,
                  'create_thumb' => FALSE,
                  'maintain_ratio' => FALSE,
                  'width' => $thumb_size,
                  'height' => $thumb_size
              );

              if ($img_type == 'wide')
              {
                  $conf_new['x_axis'] = ($config['width'] - $thumb_size) / 2 ;
                  $conf_new['y_axis'] = 0;
              }
              else if($img_type == 'landscape')
              {
                  $conf_new['x_axis'] = 0;
                  $conf_new['y_axis'] = ($config['height'] - $thumb_size) / 2;
              }
              else
              {
                  $conf_new['x_axis'] = 0;
                  $conf_new['y_axis'] = 0;
              }

              $this->image_lib->initialize($conf_new);

              $this->image_lib->crop();

              $this->image_lib->clear();
              unset($config);

              if ($this->upload->do_upload('pimage'.$i)) {                
                  $font[$i] = $this->upload->data();
              }else{
                if(empty($input['image_check_front'.$i])){
                  $font[$i]['file_name']="";
                  $error_text_font_detail.=$i." ";
                }else{
                  $font[$i]['file_name']=$input['image_check_front'.$i];
                }
              }

              if (!empty($error_text_font_detail)) {
                $error_text_font=" ไฟล์ที่ ".$error_text_font_detail;
              }

              if(empty($input['image_check_front'.$i])){
                if (!empty($font[$i]['file_name'])) {
                  $file_parts = getimagesize('assets/uploads/numberid/' .$uid .'/'. $font[$i]['file_name']);
                  $file_parts_width = $file_parts[0];

                  if ($file_parts_width > 1920)
                  {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'assets/uploads/numberid/' .$uid .'/'. $font[$i]['file_name'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width']     = 1920;
                    $this->load->library('image_lib', $config); 
                    $this->image_lib->resize();
                  }
                }
              }
          }else{
                if(empty($input['file_thumb'.$i])){
                  $file_thumb.=",";
                }else{
                  $file_thumb.=$input['file_thumb'.$i].",";
                }

                if(empty($input['image_check_front'.$i])){
                  $font[$i]['file_name']="";
                  $error_text_font_detail.=$i." ";
                }else{
                  $font[$i]['file_name']=$input['image_check_front'.$i];
                }
          }
        }   
            
/*          if (empty($file_thumb)) {
            $file_thumb=$input['file_thumb'];
          } */ 

          if ($this->session->uid!=1) {
            $data = array(
              'product_no' => 'SP'.time().rand(1,3),
              'product_name' => $input['pname'],
              'product_price' => $input['pprice'],
              'product_type_id' => $input['ptype'],
              'product_detail' => $input['pdetail'],
              'product_status_for_sale' => $input['pstatusfs'],
              'product_image' => @$file_thumb,
              'product_images' => @$font[1]['file_name'].','.@$font[2]['file_name'].','.@$font[3]['file_name'].','.@$font[4]['file_name'].','.@$font[5]['file_name'],
              'product_status' => '1'
            );
          }else{
            $data = array(
              'product_no' => 'SP'.time().rand(1,3),
              'product_name' => $input['pname'],
              'product_price' => $input['pprice'],
              'product_type_id' => $input['ptype'],
              'product_detail' => $input['pdetail'],
              'product_status_for_sale' => $input['pstatusfs'],
              'product_image' => @$file_thumb,
              'product_images' => @$font[1]['file_name'].','.@$font[2]['file_name'].','.@$font[3]['file_name'].','.@$font[4]['file_name'].','.@$font[5]['file_name'],
              'product_status' => '0'
            );
          }

        $this->Product_Model->update_product($data,$pid);
        $this->session->set_flashdata(array('msg' => '<script>swal("แก้ไขข้อมูลสำเร็จ","รอแอดมินยืนยัน","success");</script>'));
        redirect(base_url("admin/shop/product/{$sid}"));
      }

    }
    $data['title'] = "แก้ไขสินค้า ";
    $data['ptype'] = $this->Product_Type_Model->get_all_product_type();
    $data['product'] = $this->Product_Model->get_product($pid,$uid);
    //print_r($data['product']);

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/shop/edit_product',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  // ลบสินค้า
  public function delete_product($sid =null,$pid = null)
  {
    // ค้นหาเลข users_id
    $userid = $this->Product_Model->get_userid($pid,FALSE);
    $this->Product_Model->delete_product_confirm($pid);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    redirect(base_url("admin/shop/product/".$sid));
  }

  // function ลบโฟเดอร์
  private function rrmdir($dir)
  {
     if (is_dir($dir)) {
       $objects = scandir($dir);
       foreach ($objects as $object) {
         if ($object != "." && $object != "..") {
           if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
         }
       }
       reset($objects);
       rmdir($dir);
     }
  }

  // ลบข้อมูลร้านจากตาราง shop,product,image_register,blog,user
  public function delete($uid = null)
  {
    if ($uid != null) {
      $this->Image_Register_Model->del_image($uid);
      $this->Product_Model->del_product($uid);
      $this->Blog_Model->del_blog($uid);
      $this->Shop_Model->del_shop($uid);
      $this->Users_Model->del_user($uid);
      $path = FCPATH . "assets/uploads/numberid/".$uid."/";
      $this->rrmdir($path);
      $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
      redirect(base_url('admin/shop'));
    }
  }

  public function recommend($sid = null,$uid = null)
  {
    $this->Shop_Model->recommend(1,$sid);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    if (!empty($uid)) {
    redirect(base_url('admin/userslist/view_member/'.$uid));
    }else{
    redirect(base_url("admin/shop"));
    }
  }

  public function unrecommend($sid = null,$uid = null)
  {
    $this->Shop_Model->recommend(0,$sid);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    if (!empty($uid)) {
    redirect(base_url('admin/userslist/view_member/'.$uid));
    }else{
    redirect(base_url("admin/shop"));
    }
  }
}

 ?>
