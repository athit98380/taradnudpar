<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Group extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Product_Type_Model');
    $this->load->library('form_validation');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index($page = 1)
  {
    $data['title'] = 'กลุ่มสินค้า';
    $data['items'] = $this->Product_Type_Model->get_all_product_type();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/product_type/group',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function add()
  {
    $input = $this->input->post();
    if (!empty($input)) {
      $this->form_validation->set_rules('name','Name','required');
      if ($this->form_validation->run() == true) {
        if ($this->Product_Type_Model->record_count($input['name']) <= 0) {
          $data = array(
            'product_type_name' => $input['name'],
            'users_id' => $this->session->uid
          );
          $this->Product_Type_Model->set_product_type($data);
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","เพิ่มข้อมูลสำเร็จ","success");</script>'));
          redirect(base_url('admin/group'));
        }else {
          $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","พบข้อมูลนี้ในระบบ","warning");</script>'));
          redirect(base_url('admin/group'));
        }

      }
    }
    $data['title'] = 'เพิ่มกลุ่มสินค้า';

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/product_type/add_group',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function edit($id = null)
  {
    $input = $this->input->post();
    if (!empty($input)) {
      $this->form_validation->set_rules('id','Id','required');
      $this->form_validation->set_rules('name','Name','required');
      if ($this->form_validation->run() == true) {
        $data = array(
          'product_type_name' => $input['name']
        );
        $this->Product_Type_Model->update_item($input['id'],$data);
        $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
        redirect(base_url('admin/group'));
      }
    }

    $data['title'] = ucfirst('edit');
    $data['id'] = $id;
    $data['item'] = $this->Product_Type_Model->get_by_id($id);

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/product_type/edit_group',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function delete($id = null)
  {
    $this->Product_Type_Model->delete_item($id);
    $this->session->set_flashdata(array('msg' => "<script>swal('ข้อความจากระบบ','ลบข้อมูลสำเร็จ','success');</script>"));
    redirect(base_url('admin/group'));
  }
}

 ?>
