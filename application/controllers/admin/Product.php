<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Product extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Product_Model');
    $this->load->library('pagination');
    $this->load->library('Count_admin');

    if ($this->session->userdata('user_type') != 'admin') {
      redirect(base_url());
    }
  }

  public function index()
  {
    $data['title'] = "จัดการสินค้า (ยืนยัน)";
    $data['products'] = $this->Product_Model->get_confirm_product();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/product/product',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function get($product_status=null)
  {

    if (!empty($product_status) || $product_status==0) {
      $data['title'] = "จัดการสินค้า (ยืนยัน)";
      $data['products'] = $this->Product_Model->get_product_by_status($product_status);

      $this->load->view('admin/layout/header',$data);
      $this->load->view('admin/product/product',$data);
      $this->load->view('admin/layout/footer',$data);
    }else{
      redirect(base_url('admin/product/get/0'));
    }
  }

  public function all()
  {
    $data['title'] = "จัดการสินค้าทั้งหมด";
    $data['products'] = $this->Product_Model->get_all_product();

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/product/product',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function detail($id = null)
  {
    $data['title'] = "รายละเอียด";
    $data['products'] = $this->Product_Model->get_product($id);

    $this->load->view('admin/layout/header',$data);
    $this->load->view('admin/product/product_detail',$data);
    $this->load->view('admin/layout/footer',$data);
  }

  public function confirm($id = null)
  {
    $product = $this->Product_Model->get_product($id);
    $link_back = $product->product_status;
    $this->Product_Model->update_product(array('product_status' => '0'),$id);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    redirect(base_url('admin/product/get/'.$link_back));
  }

  public function confirm_del($sid = null,$pid = null)
  {
    $product = $this->Product_Model->get_product($pid);
    $link_back = $product->product_status;
    $this->Product_Model->update_product(array('product_status' => '3'),$pid);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ลบการสำเร็จ","success");</script>'));
    redirect(base_url('admin/product/get/'.$link_back));
  }  

  public function del($id = null)
  {
    $this->Product_Model->delete_product_confirm($id);
    $this->session->set_flashdata(array('msg' => '<script>swal("ข้อความจากระบบ","ทำรายการสำเร็จ","success");</script>'));
    redirect(base_url('admin/product'));
  }
}

 ?>
