<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Seo extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('Shop_Model');
    $this->load->model('Blog_Model');
  }

    function sitemap()
    {
		$data['data_url'] = array(
			'main/amulet',
			'main/amulettop',
			'main/shoplist',
			'main/news',
			'main/usage',
			'main/contact',
		);

    	$data['shoplist'] = $this->Shop_Model->get_all_shop_show();
    	foreach ($data['shoplist'] as $key => $value) {
    		$data['shoplist_seo'][$key]='main/shop/'.$value->shop_id;
    	}
    	//print_r($data['shoplist_seo']);

    	$data['bloglist'] = $this->Blog_Model->get_all_blog();
    	foreach ($data['bloglist'] as $key => $value) {
    		$data['bloglist_seo'][$key]='main/news_detail/'.$value->blog_id;
    	}
    	//print_r($data['bloglist_seo']);


        //$data = "";//select urls from DB to Array
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->load->view("sitemap",$data);
    }
}
