-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2019 at 08:39 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taradnudpar`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `banner_position` enum('left','right') DEFAULT NULL,
  `banner_image` varchar(45) DEFAULT NULL,
  `banner_status` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(11) NOT NULL,
  `blog_name` varchar(255) DEFAULT NULL,
  `blog_detail` longtext,
  `blog_image` varchar(200) DEFAULT NULL,
  `blog_create_on` datetime DEFAULT NULL,
  `blog_update_on` datetime DEFAULT NULL,
  `blog_status` varchar(1) DEFAULT '1' COMMENT '1 = admin\n2 = user',
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `blog_name`, `blog_detail`, `blog_image`, `blog_create_on`, `blog_update_on`, `blog_status`, `users_id`) VALUES
(2, 'adsssss111', '<p>effefwefwefe123 [removed]alert&#40;&#39;asdasd&#39;&#41;;[removed] <strong>TEST</strong></p>', 'hs-CPatrick.jpg', '2019-03-19 11:17:06', '2019-03-21 12:13:07', '1', 7),
(3, 'werrwer', '<p>wrwrrew<strong>ewrwewe</strong>erwererw</p>', 'd1.jpg', '2019-03-20 13:41:00', '2019-03-20 13:41:00', '1', 7),
(4, NULL, '<p>qwqwqe<strong>qweweewq</strong>qeqw</p>', NULL, '2019-03-20 16:31:03', '2019-03-20 16:31:03', '2', 7),
(5, 'dsdsdffds', '<p>sdfsdfdfssfd<strong>efewfeefw</strong>fewfweewf</p>', 'hs-CPatrick1.jpg', '2019-03-21 12:13:47', '2019-03-21 12:13:47', '1', 7),
(6, NULL, '<p>ไม่มีประกาศ</p>', NULL, '2019-03-21 14:05:27', '2019-03-21 14:05:27', '2', 8);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL DEFAULT 'Master admin/admin',
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `image_register`
--

CREATE TABLE `image_register` (
  `image_register_id` int(11) NOT NULL,
  `image_register_front1` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_back1` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_detail1` mediumtext,
  `image_register_status1` varchar(1) DEFAULT NULL,
  `image_register_front2` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_back2` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_detail2` mediumtext,
  `image_register_status2` varchar(1) DEFAULT NULL,
  `image_register_front3` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_back3` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_detail3` mediumtext,
  `image_register_status3` varchar(1) DEFAULT NULL,
  `image_register_front4` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_back4` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_detail4` mediumtext,
  `image_register_status4` varchar(1) DEFAULT NULL,
  `image_register_front5` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_back5` varchar(100) DEFAULT NULL COMMENT 'ทวง,ติดตาม,อัพเดท,เปลี่ยนสี,หมายเหตุ',
  `image_register_detail5` mediumtext,
  `image_register_status5` varchar(1) DEFAULT NULL,
  `image_register_create_on` datetime DEFAULT NULL,
  `image_register_update_on` datetime DEFAULT NULL,
  `sub_order_status` int(1) DEFAULT '0',
  `users_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image_register`
--

INSERT INTO `image_register` (`image_register_id`, `image_register_front1`, `image_register_back1`, `image_register_detail1`, `image_register_status1`, `image_register_front2`, `image_register_back2`, `image_register_detail2`, `image_register_status2`, `image_register_front3`, `image_register_back3`, `image_register_detail3`, `image_register_status3`, `image_register_front4`, `image_register_back4`, `image_register_detail4`, `image_register_status4`, `image_register_front5`, `image_register_back5`, `image_register_detail5`, `image_register_status5`, `image_register_create_on`, `image_register_update_on`, `sub_order_status`, `users_id`, `shop_id`) VALUES
(1, 'd.jpg', 'd5.jpg', 'qwe', NULL, 'd1.jpg', 'd6.jpg', 'qweqwe', NULL, 'd2.jpg', 'd7.jpg', 'qwesdfd', NULL, 'd3.jpg', 'd8.jpg', 'qeffeeq', NULL, 'd4.jpg', 'd9.jpg', 'qfeefwfewfeewf', NULL, NULL, NULL, 0, 1, 1),
(2, '393f43b1c77b0b78bce6c850b0f2b8aa.jpg', '3a6a16afcede22658bca0e4808d56fdf.jpg', '1', NULL, 'af2b7f4638f29e374cbb92d376a57f57.jpg', '1054ecad6e240a02eba6dae69da83a5d.jpg', '1', NULL, 'bb87363472d68050f5c35533576bd668.jpg', '7c488e2514779c72a58c3d2ce6824d9b.jpg', '1', NULL, '98a38161072311f7d14d9485a18d1291.jpg', 'f8878344467c9bbcbf58337fa7f76be4.jpg', '1', NULL, 'cd9ced5e9ebd209e5c9d904ecddeff46.jpg', 'b14cddc690f9c1e9bdb90e28b62f0db1.jpg', '1', NULL, '2019-03-13 10:27:33', '2019-03-13 10:27:33', 0, 2, 2),
(4, '88593e8b582c8e9a99de33f50e6ab4d7.jpg', '49ad50ddacbc7953e63c837841edf2dd.jpg', '1111', NULL, '7158627c185b98523b0fc8902d006c0e.jpg', '68874b6df5841f943031ca70b974b5d9.jpg', '1', NULL, '521f082a3dece739becca3312f7f355c.jpg', '27f43452ac1bb1cf16154dd760eedf2b.jpg', '1', NULL, '1ba6b6af84925fc8e3cde923f408e755.jpg', 'd25a2e6406ea5735e3f994506fa6dc40.jpg', '1', NULL, '467d850817603886a239635b1bf6b4eb.jpg', '999c5cf1dd5c56e0a292caec34873679.jpg', '1', NULL, '2019-03-13 10:40:29', '2019-03-13 10:40:29', 0, 4, 4),
(5, '7caeb9304f6cc14f42608074a2254ddc.jpg', '09d53c6bc7d453e34d4eb49a9803b411.jpg', '1111', NULL, '3ac0618bafd6e768ed8560da0e8b363c.jpg', 'e0ab85e7b20e72813ddab4d57086178d.jpg', '1', NULL, '3525d6a0e148d7478e7c5859b0840e67.jpg', 'e5c43405f110a7cc520b4b474f963e8b.jpg', '1', NULL, '486c0c390aae0a9ed93b75b1d1a7c3a9.jpg', '94749e21e8088b3af4987377a39b9380.jpg', '1', NULL, 'dccf6b0b43e8e0afe3456af9f2d14e0a.jpg', '77d51e734eee34fd2e5867a5328808ac.jpg', '1', NULL, '2019-03-13 10:45:25', '2019-03-13 10:45:25', 0, 5, 5),
(7, 'c6cf94d6058af971fd55a92b98b02506.jpg', '862d87586b0e5e46735122b7f16d8a73.jpg', '1', NULL, '3586ccbd07e73a62d9d666d51f2b8583.jpg', '6e41bd46bc20cdcaafb02f66e0296cd2.jpg', '1', NULL, '6faa171abbcfa5df64735242794df32e.jpg', '8a4a6a2595f7cee1df83af446b39d1a3.jpg', '1', NULL, 'e2efb4c049103ec39535ddb93ecddedf.jpg', '40b48b8d0b7dcbdccaedbfccee56b6cf.jpg', '1', NULL, 'a3406fd8ab5f139361469dbff374816f.jpg', '0f4e86a9f6c4b7ba9847fdcab7c82619.jpg', '1', NULL, '2019-03-13 11:01:42', '2019-03-13 11:01:42', 0, 7, 7),
(8, '2a60e6d50f75616d9e668b75e1cb4019.jpg', '40501737da132be656e00fa586a5ea8a.jpg', '1', NULL, 'dd39dcd11dfee36e690386248900aef2.jpg', '0b813314398031967b9dbf7da67dd53a.jpg', '1', NULL, 'e0fe6c7c2ac72c810a8f2ed9a1baba11.jpg', 'f06da57916641451ef90f2ab31db9967.jpg', '1', NULL, '4537a35e79c868d234aad6204acd3709.jpg', '1ca3415103cc1306844fa4449eb3e212.jpg', '1', NULL, 'a426eb20d99457ba1bcab237b8db5d9c.jpg', 'd6aa642bc4df18d28b938a3e4a58212b.jpg', '1', NULL, '2019-03-13 14:35:21', '2019-03-13 14:35:21', 0, 8, 8);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_link` varchar(45) DEFAULT NULL,
  `menu_level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_level`) VALUES
(1, 'หน้าแรก', 'http://localhost/taradnudpar/', 1),
(2, 'รายการอัพเดต', 'http://localhost/taradnudpar/main/amulet', 2),
(3, 'รายการพระเด่น', 'http://localhost/taradnudpar/main/amulettop', 3),
(4, 'ร้านพระมาตราฐาน', 'http://localhost/taradnudpar/', 4),
(5, 'ข่าวประชาสัมพันธ์', 'http://localhost/taradnudpar/main/news', 5),
(6, 'ระเบียบการใช้งาน', 'http://localhost/taradnudpar/', 6),
(7, 'ติดต่อ', 'http://localhost/taradnudpar/', 7);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_no` varchar(15) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_detail` longtext,
  `product_view` int(11) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `product_images` mediumtext,
  `product_price` int(11) DEFAULT NULL,
  `product_status_for_sale` varchar(45) DEFAULT NULL,
  `product_confirm` varchar(255) DEFAULT NULL,
  `product_confirm_by` int(11) DEFAULT NULL,
  `product_create_on` datetime DEFAULT NULL,
  `product_update_on` datetime DEFAULT NULL,
  `product_status` varchar(1) DEFAULT '0' COMMENT '0 = แอดมินยืนยันแล้ว\n1 = แอดมินยังไม่ได้ยืนยัน\n2 = รอแอดมินยืนยันลบ',
  `users_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 = รอแอดมินยืนยัน\n1 = ยืนยันแล้ว',
  `product_type_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_no`, `product_name`, `product_detail`, `product_view`, `product_image`, `product_images`, `product_price`, `product_status_for_sale`, `product_confirm`, `product_confirm_by`, `product_create_on`, `product_update_on`, `product_status`, `users_id`, `product_type_id`, `shop_id`) VALUES
(5, 'SP1552901972', 'พระดี', 'testing', NULL, 'fd0bc145ee120880172c70794f87b0e3.jpg', NULL, 9999, NULL, NULL, NULL, '2019-03-18 10:39:32', '2019-03-20 04:10:59', '0', 7, 1, 7),
(7, 'SP15531399871', 'พรดีมาก', 'aaaaa', NULL, '3cd99ee62b741b8d8e48ea3685b6ae80.jpg', NULL, 90, 'โชว์พระ', NULL, NULL, '2019-03-21 04:46:27', '2019-03-22 04:55:14', '1', 8, 2, 8),
(8, 'SP15531511731', '123', '123123', NULL, '850fa059e655ced996d393f15f8b562c.jpg', NULL, 123, 'ขายแล้ว', NULL, NULL, '2019-03-21 07:52:53', '2019-03-22 04:57:16', '1', 8, 1, 8),
(9, 'SP15531514391', 'we', '1221', NULL, '95531cc6ea065cdf92247e357cc77034.jpg', NULL, 12, NULL, NULL, NULL, '2019-03-21 07:57:19', '2019-03-21 07:57:30', '0', 8, 2, 8),
(10, 'SP15531515171', 'dwq', 'wdqdwq', NULL, 'b1db811ecaf6e6ea969182fc705bfe89.jpg', NULL, 21, NULL, NULL, NULL, '2019-03-21 07:58:37', '2019-03-21 07:58:50', '0', 8, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `product_type_id` int(11) NOT NULL,
  `product_type_name` varchar(255) DEFAULT NULL,
  `product_type_status` varchar(45) DEFAULT '0',
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`product_type_id`, `product_type_name`, `product_type_status`, `users_id`) VALUES
(1, 'พระกรุ', '0', 7),
(2, 'พระเครื่อง', '0', 7),
(4, 'as', '0', 7);

-- --------------------------------------------------------

--
-- Table structure for table `renew`
--

CREATE TABLE `renew` (
  `renew_id` int(11) NOT NULL,
  `renew_confirm` varchar(255) DEFAULT NULL,
  `renew_confirm_by` int(11) DEFAULT NULL,
  `renew_create_on` varchar(45) DEFAULT NULL,
  `renew_update_on` varchar(45) DEFAULT NULL,
  `renew_status` varchar(1) DEFAULT '0' COMMENT '0 = ไม่ได้ยืนยัน\n1 = ยืนยันแล้ว',
  `shop_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `renew`
--

INSERT INTO `renew` (`renew_id`, `renew_confirm`, `renew_confirm_by`, `renew_create_on`, `renew_update_on`, `renew_status`, `shop_id`, `users_id`) VALUES
(1, '9bb8cb6c20043ef6363504a7bac86faf.jpg', NULL, '2019-03-18 10:16:34', '2019-03-18 10:16:34', '0', 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `shop_id` int(11) NOT NULL,
  `shop_no` varchar(10) DEFAULT NULL,
  `shop_name` varchar(200) NOT NULL,
  `shop_address` mediumtext NOT NULL,
  `shop_detail` longtext NOT NULL,
  `shop_guarantee` mediumtext,
  `shop_guarantee_by` varchar(200) DEFAULT NULL,
  `shop_guarantee_by_tel` varchar(10) DEFAULT NULL,
  `shop_bank_name` varchar(100) DEFAULT NULL,
  `shop_bank_type` enum('ออมทรัพย์','กระแสรายวัน') DEFAULT NULL,
  `shop_bank_sub` varchar(150) NOT NULL,
  `shop_bank_name_account` varchar(200) DEFAULT NULL,
  `shop_bank_number` varchar(15) DEFAULT NULL,
  `shop_create_on` datetime DEFAULT NULL,
  `shop_update_on` datetime DEFAULT NULL,
  `shop_expired_on` datetime DEFAULT NULL,
  `shop_amout_product` int(11) DEFAULT NULL,
  `shop_amount_added` int(11) DEFAULT '1' COMMENT 'จำนวนที่เพิ่มได้ต่อวัน',
  `shop_status` varchar(45) DEFAULT '0',
  `shop_view` int(11) DEFAULT '0',
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`shop_id`, `shop_no`, `shop_name`, `shop_address`, `shop_detail`, `shop_guarantee`, `shop_guarantee_by`, `shop_guarantee_by_tel`, `shop_bank_name`, `shop_bank_type`, `shop_bank_sub`, `shop_bank_name_account`, `shop_bank_number`, `shop_create_on`, `shop_update_on`, `shop_expired_on`, `shop_amout_product`, `shop_amount_added`, `shop_status`, `shop_view`, `users_id`) VALUES
(1, NULL, 'KTOINDY', 'testing', 'testing', 'testing', 'testing testing', '0987742160', 'ธนาคารกรุงเทพ', 'ออมทรัพย์', 'กำแพงเพชร', 'คติวิชฌ์  ทานาค', '192083746273809', '2019-03-13 03:34:54', '2019-03-13 03:34:54', '2019-06-13 03:34:54', 1, 1, '0', NULL, 1),
(2, NULL, '11111', '1111111111', '1111111', '11111111', '11', '1111111111', 'ธนาคารไทยพาณิชย์', 'ออมทรัพย์', '1', '1', '111111111111111', '2019-03-13 04:27:33', '2019-03-13 04:27:33', '2019-06-13 04:27:33', 1, 1, '0', NULL, 2),
(4, NULL, '1', '1', '1', '1', '1', '1111111111', 'ธนาคารเกียรตินาคิน', 'ออมทรัพย์', '1', '1', '111111111111111', '2019-03-13 04:40:29', '2019-03-13 04:40:29', '2019-06-13 04:40:29', 1, 1, '0', NULL, 4),
(5, NULL, '1', '1', '1', '1', '1', '1111111111', 'ธนาคารเกียรตินาคิน', 'ออมทรัพย์', '1', '1', '111111111111111', '2019-03-13 04:45:25', '2019-03-13 04:45:25', '2019-06-13 04:45:25', 1, 1, '0', NULL, 5),
(7, NULL, '123123123', '1', '1', '1', '1', '1111111111', 'ธนาคารไทยเครดิตเพื่อรายย่อย', 'ออมทรัพย์', '1123123', '1WQEQWEQ', '111111111111111', '2019-03-13 05:01:42', '2019-03-13 05:01:42', '2019-03-13 00:00:00', NULL, 1, '0', NULL, 7),
(8, NULL, 'พระสมปอง', '53 หมู่ 13 ต.หินดาต อ.ปางศิลาทอง จ.กำแพงเพชร', 'ร้านดีมีคุณภาพ', 'ประกันทุกอย่าง', 'นายทองดี', '0881212122', 'ธนาคารกรุงเทพ', 'ออมทรัพย์', 'กำแพงเพชร', 'คติวิชฌ์ ทานาค', '92839098271632', '2019-03-13 08:35:21', '2019-03-13 08:35:21', '2019-06-13 08:35:21', NULL, 1, '0', 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `shop_recommand`
--

CREATE TABLE `shop_recommand` (
  `shop_recommand_id` int(11) NOT NULL,
  `shop_recommand_create_on` datetime DEFAULT NULL,
  `shop_recommand_update_on` datetime DEFAULT NULL,
  `shop_recommand_status` varchar(1) DEFAULT NULL,
  `shop_ids` mediumtext,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `static`
--

CREATE TABLE `static` (
  `static_view_all` int(11) DEFAULT NULL,
  `static_view_today` int(11) DEFAULT NULL,
  `static_view_online` int(11) DEFAULT NULL,
  `static_view_update_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static`
--

INSERT INTO `static` (`static_view_all`, `static_view_today`, `static_view_online`, `static_view_update_on`) VALUES
(500, 11, 10, '2019-03-26 09:30:38');

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE `system` (
  `system_free_month` int(11) DEFAULT NULL,
  `system_amout_product_of_month` int(11) DEFAULT NULL,
  `system_first_year_price` int(11) DEFAULT NULL,
  `system_one_year_price` int(11) DEFAULT NULL,
  `system_header_bgcolor` varchar(6) DEFAULT NULL,
  `system_header_image` varchar(45) DEFAULT NULL,
  `system_header_position` varchar(45) DEFAULT NULL,
  `system_footer_bgcolor` varchar(6) DEFAULT NULL,
  `system_footer_image` varchar(45) DEFAULT NULL,
  `system_footer_position` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อ',
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'นามสกุล',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'password',
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เลขที่',
  `user_tel_add` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_number_id` int(15) NOT NULL,
  `user_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `created_on` datetime NOT NULL,
  `user_facebook_id` int(6) DEFAULT NULL,
  `user_facebook_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_google_id` int(22) DEFAULT NULL,
  `user_google_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `admin_active` int(1) NOT NULL DEFAULT '0',
  `user_gendar` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_locale` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `salt`, `user_tel`, `user_tel_add`, `user_number_id`, `user_image`, `user_type`, `created_on`, `user_facebook_id`, `user_facebook_link`, `user_google_id`, `user_google_link`, `active`, `admin_active`, `user_gendar`, `user_locale`, `ip_address`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `activation_code`, `last_login`) VALUES
(1, 'Khatiwit', 'Thanark', 'ktoindy@gmail.com', '8a450654834b05f03cde4ba4bb10f7db9fd28a09c6056a7de5ccff8fbebf51b4', 'XRDD?', '0643532160', NULL, 2147483647, '313e573c49941400852768e41061d6af.jpg', 'user', '2019-03-13 09:34:54', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL),
(2, 'Khatiwit', 'Thanark', 'ktoindy1@gmail.com', 'fb648502d6861227c44699ba44a9e357c53fcbb20fd542b3f07f341f56f06f49', '?RDvS', '0643532160', NULL, 2147483647, '313e573c49941400852768e41061d6af.jpg', 'user', '2019-03-13 10:27:33', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL),
(4, 'Khatiwit', 'Thanark', 'ktoindy3@gmail.com', '16bf75615b808129f02b56df22d4b974ca19e79ec42376356392c6c3dcb9adf9', '2b86aa03ebcee530e8d8d874ee126ac5', '0643532160', NULL, 2147483647, '313e573c49941400852768e41061d6af.jpg', 'user', '2019-03-13 10:40:29', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL),
(5, 'Khatiwit', 'Thanark', 'ktoindy9@gmail.com', '8b8d8cb22a2cebd74db5516057dfb37d', '?RD?%', '0643532160', NULL, 2147483647, '313e573c49941400852768e41061d6af.jpg', 'user', '2019-03-13 10:45:25', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL),
(6, 'Khatiwit', 'Thanark', 'ktoindy10@gmail.com', 'cf2b5592d3ef5a45d0469018a64d9f9a', 'RD?!', '0643532160', NULL, 2147483647, '313e573c49941400852768e41061d6af.jpg', 'user', '2019-03-13 10:52:01', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, '::1', NULL, NULL, NULL, NULL, NULL),
(7, 'Khatiwit', 'Thanark', 'ktoindy11@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'rRD?', '0643532160', NULL, 2147483647, '313e573c49941400852768e41061d6af.jpg', 'admin', '2019-03-13 11:01:42', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, '::1', NULL, NULL, NULL, NULL, '2019-03-22 10:18:50'),
(8, '1', '1', 'ktoindy12@gmail.com', '25d55ad283aa400af464c76d713c07ad', '?RF%!', '1111111111', NULL, 2147483647, '313e573c49941400852768e41061d6af.jpg', 'user', '2019-03-13 14:35:21', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, '::1', NULL, NULL, NULL, NULL, '2019-03-22 09:54:28');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `groups_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `fk_blog_users1_idx` (`users_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_register`
--
ALTER TABLE `image_register`
  ADD PRIMARY KEY (`image_register_id`),
  ADD KEY `fk_sub_order_users1_idx` (`users_id`),
  ADD KEY `fk_image_register_shop1_idx` (`shop_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `fk_sub_order_product_users1_idx` (`users_id`),
  ADD KEY `fk_product_product_type1_idx` (`product_type_id`),
  ADD KEY `fk_product_shop1_idx` (`shop_id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`product_type_id`),
  ADD KEY `fk_product_type_users1_idx` (`users_id`);

--
-- Indexes for table `renew`
--
ALTER TABLE `renew`
  ADD PRIMARY KEY (`renew_id`),
  ADD KEY `fk_renew_shop1_idx` (`shop_id`),
  ADD KEY `fk_renew_users1_idx` (`users_id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`shop_id`),
  ADD KEY `fk_order_users1_idx` (`users_id`);

--
-- Indexes for table `shop_recommand`
--
ALTER TABLE `shop_recommand`
  ADD PRIMARY KEY (`shop_recommand_id`),
  ADD KEY `fk_shop_recommand_users1_idx` (`users_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_groups_groups1_idx` (`groups_id`),
  ADD KEY `fk_users_groups_users1_idx` (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `image_register`
--
ALTER TABLE `image_register`
  MODIFY `image_register_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `product_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `renew`
--
ALTER TABLE `renew`
  MODIFY `renew_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `shop_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `fk_blog_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `image_register`
--
ALTER TABLE `image_register`
  ADD CONSTRAINT `fk_image_register_shop1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_order_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_product_type1` FOREIGN KEY (`product_type_id`) REFERENCES `product_type` (`product_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_shop1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_order_product_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_type`
--
ALTER TABLE `product_type`
  ADD CONSTRAINT `fk_product_type_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `renew`
--
ALTER TABLE `renew`
  ADD CONSTRAINT `fk_renew_shop1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_renew_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `shop`
--
ALTER TABLE `shop`
  ADD CONSTRAINT `fk_order_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `shop_recommand`
--
ALTER TABLE `shop_recommand`
  ADD CONSTRAINT `fk_shop_recommand_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
